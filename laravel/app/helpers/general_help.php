<?php 
/*
|--------------------------------------------------------------------------
| Detect Active Route
|--------------------------------------------------------------------------
|
| Compare given route with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/

function isActiveRoute($route, $output = "active")
{
  if (Route::currentRouteName() == $route) 
  	return $output;
  else
  	return '';
}

/*
|--------------------------------------------------------------------------
| Detect Active Routes
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/

function areActiveRoutes(Array $routes, $output = "active")
{
  foreach ($routes as $route)
  {

  	if(isActiveRoute($route, $output) == $output )
  		return $output;
      //if (Route::currentRouteName() == $route) return $output;
  }

}