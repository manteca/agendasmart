<?php
/*******************************************************************************
* config                                                                      	*
* Página de configuración del comercio                                                                      	*
* Version: 1.0                                                                 	*
* Date:    2014-02-20                                                          	*
* Author:  flow.cl                                                     			*
********************************************************************************/

return array(
/**
 * Ingrese aquí la URL de su página de éxito
 * Ejemplo: http://www.comercio.cl/kpf/exito.php
 * 
 * @var string
 */
'flow_url_exito' => 'http://www.agendasmart.cl/adm/php/kpf/exito.php',
/**
 * Ingrese aquí la URL de su página de fracaso
 * Ejemplo: http://www.comercio.cl/kpf/fracaso.php
 * 
 * @var string
 */
'flow_url_fracaso' => 'http://www.agendasmart.cl/adm/php/kpf/fracaso.php',
/**
 * Ingrese aquí la URL de su página de confirmación
 * Ejemplo: http://www.comercio.cl/kpf/confirmacion.php
 * 
 * @var string
 */
'flow_url_confirmacion' => 'http://www.agendasmart.cl/adm/php/kpf/confirma.php',
/**
 * Ingrese aquí la tasa de comisión de Flow que usará
 * Valores posibles:
 * Pago siguiente día hábil = 1 (Expreso)
 * Pago a tres días hábiles = 2 (Veloz)
 * Pago a cinco días hábiles = 3 (Normal)
 * 
 * @var int
 */
'flow_tasa_default' => 3,

/**
 * Ingrese aquí la página de pago de Flow
 * Ejemplo:
 * Sitio de pruebas = http://flow.tuxpan.com/app/kpf/pago.php
 * Sitio de produccion = https://www.flow.cl/app/kpf/pago.php
 * 
 * @var string
 */
'flow_url_pago' => 'http://flow.tuxpan.com/app/kpf/pago.php',

# Commerce specific config

/**
 * Ingrese aquí la ruta (path) en su sitio donde están las llaves
 * 
 * @var string
 */
'flow_keys' => dirname(__FILE__).'/keys',
'flow_keys_name' => "comercio_dev.pem",

/**
 * Ingrese aquí la ruta (path) en su sitio donde estarán los archivos de logs
 * 
 * @var string
 */
'flow_logPath' => dirname(__FILE__).'/logs',

/**
 * Ingrese aquí el email con el que está registrado en Flow
 * 
 * @var string
 */
'flow_comercio' => 'f.villalobos@agendasmart.cl',
'flow_err' => array(
  'ERR_AUTH' => 'Error asociado a la autenticación del cliente.',   
  'ERR_AUTH_U' => 'Error asociado a la autenticación del usuario.',   
  'ERR_MAIL' => 'Formato de email incorrecto.',   
  'ERR_USER' => 'Ya existe un usuario registrado para ese mail.',
  'ERR_VTA' => 'El usuario no se encuentra habilitado para recibir pagos.',   
  'ERR_PSW' => 'Las claves ingresadas no coinciden.',   
  'ERR_LMAX' => 'La clave debe codificarse en SHA1 y el largo debe ser 40 caracteres.',   
  'ERR_RUT' => 'No se envió RUT.',   
  'ERR_RUT_INV' => 'El RUT no es válido.',   
  'ERR_NOM' => 'No se enviaron nombres.',   
  'ERR_AP' => 'No se enviaron apellidos.',   
  'ERR_BAN' => 'No se envió código de banco o el código del banco no es válido.',   
  'ERR_CTA_T' => 'No se envió tipo de cuenta o el tipo de cuenta es incorrecto.',   
  'ERR_CTA' => 'No se envió número de cuenta.',   
  'ERR_RUTF' => 'No se envió RUT de facturación.',   
  'ERR_RUTF_INV' => 'El RUT de facturación no es válido.',   
  'ERR_RZ' => 'No se envió razón social.',   
  'ERR_DIR' => 'No se envió dirección.',   
  'ERR_COM' => 'No se envió comuna.',   
  'ERR_CIUD' => 'No se envió ciudad.',   
  'ERR_TEL' => 'No se envió teléfono.',   
  'ERR_GIRO' => 'No se envió giro.',   
  'ERR_BD' => 'No se pudo guardar registro en la base de datos.',   
  ),
);
?>
