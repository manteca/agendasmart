<?php
/*
	Variales y estados que se utilizan globalmente
*/
return array(
	// Estados de Reserva
	'estado_reserva' => array(
												1 => 'ok', 
												2 => 'en_proceso',
												3 => 'error', 
												4 => 'cancelada',
												5 => 'canc_man'),

	'estado_pago' => array(
												1 => 'exito', 
												2 => 'pendiente', 
												3 => 'fracaso',
												4 => 'cancelada',
												5 => 'canc_man'),

	'tipo_agenda' => array(
												60 => 'Hora',
												1440 => 'Día'),

	'tipo_agenda_vista_union' => array(
												60 => 'agendaWeek',
												1440 => 'month'),

	'tipo_agenda_vista' => array(
												'month' => 'Mes',
												'agendaWeek' => 'Semana'),
	'tipo_grupo_defecto' => array(
												60 => 'Agenda x Horas',
												1440 => 'Agenda x Dia')

);