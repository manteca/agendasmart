<?php 
require_once('../../include/php/config.php');
include "flowAPI.php";
global $db_type, $db_server, $db_database, $db_user, $db_password;

$db = new DB($db_type, $db_server, $db_database, $db_user, $db_password);
if(!$db->Conectar()) {
  die($db->EntregaError());
}

$sql = "SELECT id,valor, detalle FROM pagos WHERE id = ".$_GET['orden'];
if(!$db->Ejecuta($sql)) {
      die($db->EntregaError());
    }

if(!$this->db->EntregaLinea()) die('Vacio');
die('tada');
$orden_id = $db->EntregaCampo(0);
$orden_valor = $db->EntregaCampo(1);
$orden_descripcion = $db->EntregaCampo(2);

$date = new DateTime();
$timestamp = $date->format('YmdHis');

$orden_compra = $orden_id;//$_POST['orden_compra    '];
$monto = $orden_valor;//$_POST['monto'];
$concepto = $orden_descripcion;//$_POST['concepto'];
$tipo_comision = $flow_tasa_default;

$flowAPI = new flowAPI();
try {
	$flow_pack = $flowAPI->new_order($orden_compra, $monto, $concepto);
	
} catch (Exception $e) {
	header('location: error.php?e='.$e);
}

?>

<?php include "template_c/head.php" ?>

<p>
Confirme su orden antes de proceder al pago via Flow<br /><br />
Orden N°: <?php echo $orden_compra?><br />
Monto: <?php echo $monto?><br />
Descripción: <?php echo $concepto?><br />
</p> 
<form method="post" action="<?php echo $flow_url_pago?>">
<input type="hidden" name="parameters" value="<?php echo $flow_pack ?>" />
<button type="submit">Pagar en Flow</button>
</form>

<?php include "template_c/foot.php" ?>