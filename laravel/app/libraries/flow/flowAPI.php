<?php
namespace Flow;
include 'config.php';	
use Config;
use Fconfig\FlowConfig as Fc;
/*******************************************************************************
* flowAPI                                                                      	*
*                                                                              	*
* Version: 1.0                                                                 	*
* Date:    2014-02-20                                                          	*
* Author:  flow.cl                                                     			*
********************************************************************************/

class flowAPI {
	
	protected $order = array();
	//Método constructor de compatibilidad
	/*
	function flowAPI() {;
		$this->__construct();
	}
	*/
	//Constructor de la clase
	function __construct($flow_comercio = '') {

		$this->order["OrdenNumero"] = "";
		$this->order["Concepto"] = "";
		$this->order["Monto"] = "";
		$this->order["Comision"] = Config::get('flow.flow_tasa_default');
		$this->order["FlowNumero"] = "";
		$this->order["Pagador"] = "";
		$this->order["Status"] = "";
		$this->order["Error"] = "";

		$this->soap = false;
		$this->soap_url = 'http://flow.tuxpan.com/app/ws/services.wsdl';
		$this->soap_trace = 1;
		$this->soap_login = "wsFlow1";
		$this->soap_password = "ra4i9k54og33ic2d1296";

		$this->fc = new Fc;
		if($flow_comercio != ''){
				$this->fc->flow_comercio = strtolower($flow_comercio);
				$this->fc->flow_keys_name = str_replace("@", "_", strtolower($flow_comercio)).'.pem';
				$this->fc->flow_tasa_default = '3'; // TODO: Se debe agregar la tasa selccionadoa por el cliente
			}
	}


	public function soap_c()
	{
		$params = array(
	    'trace' => $this->soap_trace,
	    'login' => $this->soap_login,
	    'password' => $this->soap_password
		);

		$this->soap = new \SoapClient($this->soap_url, $params);
	}

	public function getBancos()
	{
		$bancos = $this->soap->getBancos()->data;		

		foreach ($bancos as $value) {
			$temp[$value->id] = $value->nombre;
		}
		return $temp;
	}

	public function saveCertificate($mail, $hash)
	{
		$mail = strtolower($mail);
		$response = $this->soap->getCertificate($mail,$hash);
		if($response->code != 'NO_ERR')
		{
			$this->flow_log("ERROR: ".$mail." - ".$response->code, "saveCertificate");
			return false; // Problemas de conexion con Flow
		}
		$data = $response->data;
		$flow_keys = $this->fc->flow_keys;
		$file = fopen("$flow_keys/".str_replace("@", "_", $mail).'.pem', "w+");
		if($file == false){
			$this->flow_log("ERROR: ".$mail." - No se pudo crear el archivo", "saveCertificate");
			return false; //No se logro crear el archivo.
		}
		if(!fwrite($file, $data)){
			$this->flow_log("ERROR: ".$mail." - No se pudo escribir en el archivo", "saveCertificate");
			return false; //No se logro escribir en el archivo.
		}
		fclose($file);
		$this->flow_log($mail. " - Se guerda Con exito el certificado", "saveCertificate");
		// TODO: Controlar la descarga del archivo si es exitosa o no

		return true;
	}

	public  function hasCertificate($mail = ''){
		if($mail != ''){
			$this->fc->flow_keys_name = str_replace("@", "_", strtolower($flow_comercio)).'.pem';
		}
		if(fopen($this->fc->flow_keys."/".$this->fc->flow_keys_name, "r")){
			return true;
		}

		return false;

	}
	
	public function checkPrivateKey()
	{
		return $this->flow_get_private_key_id();
	}
	// Metodos SET
	
	/**
	* Set el número de Orden del comercio
	* 
	* @param string $orderNumer El número de la Orden del Comercio
	*
	* @return bool (true/false)
	*/
	public function setOrderNumber($orderNumber) {
		if(!empty($orderNumber)) {
			$this->order["OrdenNumero"] = $orderNumber;
		}
		$this->flow_log("Asigna Orden N°: ". $this->order["OrdenNumero"], '');
		return !empty($orderNumber);	
	} 
	
	/**
	* Set el concepto de pago
	* 
	* @param string $concepto El concepto del pago
	*
	* @return bool (true/false)
	*/
	public function setConcept($concepto) {
		if(!empty($concepto)) {
			$this->order["Concepto"] = $concepto;
		}
		return !empty($concepto);	
	}
	
	/**
	* Set el monto del pago
	* 
	* @param string $monto El monto del pago
	*
	* @return bool (true/false)
	*/
	public function setAmount($monto) {
		if(!empty($monto)) {
			$this->order["Monto"] = $monto;
		}
		return !empty($monto);	
	}
	
	/**
	* Set la tasa de comisión, por default la tasa será la configurada en config.php
	* 
	* @param int $comision La comisión Flow del pago
	*
	* @return bool (true/false)
	*/
	public function setRate($comision) {
		if(!empty($comision) && ($comision == 1 || $comision == 2 || $comision == 3)) {
			$this->order["Comision"] = $comision;
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	// Metodos GET
	
	/**
	* Get el número de Orden del Comercio
	* 
	* @return string el número de Orden del comercio
	*/
	public function getOrderNumber() {
		return $this->order["OrdenNumero"];
	}
	
	/**
	* Get el concepto de Orden del Comercio
	* 
	* @return string el concepto de Orden del comercio
	*/
	public function getConcept() {
		return $this->order["Concepto"];
	}
	
	/**
	* Get el monto de Orden del Comercio
	* 
	* @return string el monto de la Orden del comercio
	*/
	public function getAmount() {
		return $this->order["Monto"];
	}
	
	/**
	* Get la comisión Flow de Orden del Comercio
	* 
	* @return string la tasa de la Orden del comercio
	*/
	public function getRate() {
		return $this->order["Comision"];
	}
	
	/**
	* Get el estado de la Orden del Comercio
	* 
	* @return string el estado de la Orden del comercio
	*/
	public function getStatus() {
		return $this->order["Status"];
	}
	
	/**
	* Get el número de Orden de Flow
	* 
	* @return string el número de la Orden de Flow
	*/
	public function getFlowNumber() {
		return $this->order["FlowNumero"];
	}
	
	/**
	* Get el email del pagador de la Orden
	* 
	* @return string el email del pagador de la Orden de Flow
	*/
	public function getPayer() {
		return $this->order["Pagador"];
	}
	
	
	/**
	* Crea una nueva Orden para ser enviada a Flow
	* 
	* @param string $orden_compra El número de Orden de Compra del Comercio
	* @param string $monto El monto de Orden de Compra del Comercio
	* @param string $concepto El concepto de Orden de Compra del Comercio
	* @param mixed $tipo_comision La comision de Flow (1,2,3)
	*
	* @return string flow_pack Paquete de datos firmados listos para ser enviados a Flow
	*/
	public function new_order($orden_compra, $monto,  $concepto, $tipo_comision = "Non") {
		// global $flow_tasa_default;
		$this->flow_log("Iniciando nueva Orden", "new_order");
		$this->flow_log('ID comercial:'.$this->fc->flow_comercio,'new_order');
		if(!isset($orden_compra,$monto,$concepto)) {
			$this->flow_log("Error: No se pasaron todos los parámetros obligatorios","new_order");
		}
		if($tipo_comision == "Non") {
			$tipo_comision = Config::get('flow.flow_tasa_default');
		}
		if(!is_numeric($monto)) {
			$this->flow_log("Error: El parámetro monto de la orden debe ser numérico","new_order");
			throw new \Exception("El monto de la orden debe ser numérico");
		}
		$this->order["OrdenNumero"] = $orden_compra;
		$this->order["Concepto"] = $concepto;
		$this->order["Monto"] = $monto;
		$this->order["Comision"] = $tipo_comision;
		return $this->flow_pack();
	}
	
	/**
	* Lee los datos enviados desde Flow a la página de confirmación del comercio
	*
	*/
	public function read_confirm() {
		if(!isset($_POST['response'])) {
			$this->flow_log("Respuesta Inválida", "read_confirm");
			throw new \Exception('Invalid response');
		}
		$data = $_POST['response'];
		$params = array();
		parse_str($data, $params);
		if(!isset($params['status'])) {
			$this->flow_log("Respuesta sin status", "read_confirm");
			throw new \Exception('Invalid response status');
		}
		$this->order['Status'] = $params['status'];
		$this->flow_log("Lee Status: " . $params['status'], "read_confirm");
		if (!isset($params['s'])) {
			$this->flow_log("Mensaje no tiene firma", "read_confirm");
			throw new \Exception('Invalid response (no signature)');
		}
		if(!$this->flow_sign_validate($params['s'], $data)) {
			$this->flow_log("firma invalida", "read_confirm");
			throw new \Exception('Invalid signature from Flow');
		}
		$this->flow_log("Firma verificada", "read_confirm");
		if($params['status'] == "ERROR") {
			$this->flow_log("Error: " .$params['kpf_error'], "read_confirm");
			$this->order["Error"] = $params['kpf_error'];
			return;
		}
		if(!isset($params['kpf_orden'])) {
			$this->flow_log("Error: kpf_orden" , "read_confirm");
			throw new \Exception('Invalid response Orden number');
		}
		$this->order['OrdenNumero'] = $params['kpf_orden'];
		$this->flow_log("Lee Numero Orden: " . $params['kpf_orden'], "read_confirm");
		if(!isset($params['kpf_monto'])) {
			throw new \Exception('Invalid response Amount');
		}
		$this->order['Monto'] = $params['kpf_monto'];
		$this->flow_log("Lee Monto: " . $params['kpf_monto'], "read_confirm");
		if(isset($params['kpf_flow_order'])) {
			$this->order['FlowNumero'] = $params['kpf_flow_order'];
			$this->flow_log("Lee Orden Flow: " . $params['kpf_flow_order'], "read_confirm");
		}
		
	}
	
	/**
	* Método para responder a Flow el resultado de la confirmación del comercio
	*
	* @param bool $result (true: Acepta el pago, false rechaza el pago)
	* 
	* @return string paquete firmado para enviar la respuesta del comercio
	*/
	public function build_response($result){
		$flow_comercio = $this->fc->flow_comercio;
		$r = ($result) ? "ACEPTADO" : "RECHAZADO";
		$data = array();
		$data["status"] = $r;
		$data["c"] = $flow_comercio;
		$q = http_build_query($data);
		$s = $this->flow_sign($q);
		$this->flow_log("Orden N°: ".$this->order["OrdenNumero"]. " - Status: $r","flow_build_response");
		return $q."&s=".$s;
	}

	/**
	* Método para recuperar los datos  en la página de Exito o Fracaso del Comercio
	*
	*/
	public function read_result() {
		if(!isset($_POST['response'])) {
			$this->flow_log("Respuesta Inválida", "read_result");
			throw new \Exception('Invalid response');
		}
		$data = $_POST['response'];
		$params = array();
		parse_str($data, $params);
		if (!isset($params['s'])) {
			$this->flow_log("Mensaje no tiene firma", "read_result");
			throw new \Exception('Invalid response (no signature)');
		}
		if(!$this->flow_sign_validate($params['s'], $data)) {
			$this->flow_log("firma invalida", "read_result");
			throw new \Exception('Invalid signature from Flow');
		}
		//Esta valor de Comision, debe ser el mismo que se envio....
		$this->order["Comision"] = Config::get('flow.flow_tasa_default');;
		$this->order["Status"] = "";
		$this->order["Error"] = "";
		$this->order['OrdenNumero'] = $params['kpf_orden'];
		$this->order['Concepto'] = $params['kpf_concepto'];
		$this->order['Monto'] = $params['kpf_monto'];
		$this->order["FlowNumero"] = $params["kpf_flow_order"];
		$this->order["Pagador"] = $params["kpf_pagador"];
		$this->flow_log("Datos recuperados Orden de Compra N°: " .$params['kpf_orden'], "read_result");
	}
	
	/**
	* Registra en el Log de Flow
	* 
	* @param string $message El mensaje a ser escrito en el log
	* @param string $type Identificador del mensaje
	*
	*/
	public function flow_log($message, $type) {
		$flow_logPath = $this->fc->flow_logPath;
		$file = fopen($flow_logPath . "/flowLog_" . date("Y-m-d") .".txt" , "a+");
		//echo $_SERVER['HTTP_X_FORWARDED_FOR'];
		//die();
		fwrite($file, "[".date("Y-m-d H:i:s.u")." ".getenv('REMOTE_ADDR')." ".getenv('HTTP_X_FORWARDED_FOR')." - $type ] ".$message . PHP_EOL);
		fclose($file);		   
	}
	
	
	// Funciones Privadas
	private function flow_get_public_key_id() {
		$flow_keys = $this->fc->flow_keys;
		try {
			$fp = fopen("$flow_keys/flow.pubkey", "r");
			$pub_key = fread($fp, 8192);
			fclose($fp);
			return openssl_get_publickey($pub_key);
		} catch (\Exception $e) {
			$this->flow_log("Error al intentar obtener la llave pública - Error-> " .$e->getMessage(), "flow_get_public_key_id");
			throw new \Exception($e->getMessage());
		}
	}
	
	private function flow_get_private_key_id() {
		$flow_keys = $this->fc->flow_keys;
		$flow_keys_name = $this->fc->flow_keys_name;
		\Debugbar::info('Nombre de la llave: '.$flow_keys_name);
		\Debugbar::info('Url de la llave: '.$flow_keys);
		try {
			$fp = fopen("$flow_keys/".$flow_keys_name, "r");
			$priv_key = fread($fp, 8192);
			fclose($fp);
			return openssl_get_privatekey($priv_key); 	
		} catch (\Exception $e) {
			$this->flow_log("Error al intentar obtener la llave privada - Error-> " .$e->getMessage(), "flow_get_private_key_id");
			\Debugbar::error("Error al intentar obtener la llave privada - Error-> " .$e->getMessage(), "flow_get_private_key_id");
			throw new \Exception($e->getMessage());
		}
	}
	
	private function flow_sign($data) {
		$priv_key_id = $this->flow_get_private_key_id();
		if(! openssl_sign($data, $signature, $priv_key_id)) {
			$this->flow_log("No se pudo firmar", "flow_sign");
			throw new \Exception('It can not sign');
		};
		return base64_encode($signature);
	}
	
	private function flow_sign_validate($signature, $data) {
		
		$signature = base64_decode($signature);
		$response = explode("&s=", $data, 2);
		$response = $response[0];

		$pub_key_id = $this->flow_get_public_key_id();
		return (openssl_verify($response, $signature, $pub_key_id) == 1);
	}
	
	private function flow_pack() {
		$flow_comercio = $this->fc->flow_comercio;
		$flow_url_exito = $this->fc->flow_url_exito;
		$flow_url_fracaso = $this->fc->flow_url_fracaso;
		$flow_url_confirmacion = $this->fc->flow_url_confirmacion;

		$comercio = urlencode($flow_comercio);
		$orden_compra = urlencode($this->order["OrdenNumero"]);
		$monto = urlencode($this->order["Monto"]);
		$tipo_comision = urlencode($this->order["Comision"]);
		$concepto = urlencode(htmlentities(utf8_decode($this->order["Concepto"])));
	
		$url_exito = urlencode($flow_url_exito);
		$url_fracaso = urlencode($flow_url_fracaso);
		$url_confirmacion = urlencode($flow_url_confirmacion);
	
		$p = "c=$comercio&oc=$orden_compra&tc=$tipo_comision&m=$monto&o=$concepto&ue=$url_exito&uf=$url_fracaso&uc=$url_confirmacion";
		
		$signature = $this->flow_sign($p);
		$this->flow_log("Orden N°: ".$this->order["OrdenNumero"]. " -empaquetado correcto","flow_pack");
		$this->flow_log('exito: '.$flow_url_exito,'flow_pack');
		$this->flow_log('fracaso: '.$flow_url_fracaso,'flow_pack');
		$this->flow_log('confirmacion: '.$flow_url_confirmacion,'flow_pack');
		$this->flow_log('comercio: '.$flow_comercio,'flow_pack');
		return $p."&s=$signature";
	}
	
	
}

?>