<?php
namespace Fconfig;
/*******************************************************************************
* config                                                                      	*
* Página de configuración del comercio                                                                      	*
* Version: 1.0                                                                 	*
* Date:    2014-02-20                                                          	*
* Author:  flow.cl                                                     			*
********************************************************************************/

class FlowConfig{
/**
 * Ingrese aquí la URL de su página de éxito
 * Ejemplo: http://www.comercio.cl/kpf/exito.php
 * 
 * @var string
 */
// public $flow_url_exito = 'http://pag.agendasmart.cl/pagos/exito';
public $flow_url_exito = 'http://agendasmart.ngrok.io/pagos/exito';
/**
 * Ingrese aquí la URL de su página de fracaso
 * Ejemplo: http://www.comercio.cl/kpf/fracaso.php
 * 
 * @var string
 */
// public $flow_url_fracaso = 'http://pag.agendasmart.cl/pagos/fracaso';
public $flow_url_fracaso = 'http://agendasmart.ngrok.io/pagos/fracaso';
/**
 * Ingrese aquí la URL de su página de confirmación
 * Ejemplo: http://www.comercio.cl/kpf/confirmacion.php
 * 
 * @var string
 */
// public $flow_url_confirmacion = 'http://pag.agendasmart.cl/flows/confirmar';
public $flow_url_confirmacion = 'http://agendasmart.ngrok.io/flows/confirmar';
/**
 * Ingrese aquí la tasa de comisión de Flow que usará
 * Valores posibles:
 * Pago siguiente día hábil = 1 (Expreso)
 * Pago a tres días hábiles = 2 (Veloz)
 * Pago a cinco días hábiles = 3 (Normal)
 * 
 * @var int
 */
public $flow_tasa_default = 3;

/**
 * Ingrese aquí la página de pago de Flow
 * Ejemplo:
 * Sitio de pruebas = http://flow.tuxpan.com/app/kpf/pago.php
 * Sitio de produccion = https://www.flow.cl/app/kpf/pago.php
 * 
 * @var string
 */
public $flow_url_pago = 'http://flow.tuxpan.com/app/kpf/pago.php';

# Commerce specific config

/**
 * Ingrese aquí la ruta (path) en su sitio donde están las llaves
 * 
 * @var string
 */
public $flow_keys = "/keys";
public $flow_keys_name = "comercio_dev.pem";

/**
 * Ingrese aquí la ruta (path) en su sitio donde estarán los archivos de logs
 * 
 * @var string
 */
public $flow_logPath = '/logs';

/**
 * Ingrese aquí el email con el que está registrado en Flow
 * 
 * @var string
 */
public $flow_comercio = 'f.villalobos@agendasmart.cl';

/**
 * Ingrese aquí el modo de acceso a Webpay
 * Valores posibles:
 * Mostrar pasarela Flow = f 
 * Ingresar directamente a Webpay = d
 * 
 * @var string
 */
public $flow_tipo_integracion = 'd';

  public function __construct()
    {
        $this->flow_keys = dirname( __FILE__ ).$this->flow_keys;
        $this->flow_logPath = dirname( __FILE__ ).$this->flow_logPath;

    }
}
?>
