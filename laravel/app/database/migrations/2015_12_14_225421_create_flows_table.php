<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFlowsTable extends Migration {

	public function up()
	{
		Schema::create('flows', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('orden_id');
			$table->integer('valor');
			$table->string('descripcion', 200);
			$table->integer('tipo_comision');
			$table->integer('flow_orden_id');
			$table->string('flow_status', 45);
			$table->string('flow_error', 40);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('flows');
	}
}