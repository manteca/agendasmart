<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanTable extends Migration {

	public function up()
	{
		Schema::create('plan', function(Blueprint $table) {
			$table->increments('ID_PLAN');
			$table->string('NOMBRE', 255);
			$table->integer('VALOR');
			$table->integer('USUARIOS');
			$table->integer('RESERVAS');
			$table->tinyInteger('ESTADO');
			$table->integer('dias');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('plan');
	}
}