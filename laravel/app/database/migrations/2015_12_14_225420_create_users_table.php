<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->timestamps();
			$table->string('email', 255);
			$table->string('password', 255);
			$table->string('confirmation_code', 255);
			$table->string('remember_token', 255)->nullable()->default('NULL');
			$table->boolean('confirmed')->default(0);
			$table->integer('id_cliente');
			$table->string('username', 255);
		});

		// Creates password reminders table
        Schema::create('password_reminders', function ($table) {
            $table->string('email');
            $table->string('token');
            $table->timestamp('created_at');
        });
	}

	public function down()
	{
		Schema::drop('password_reminders');
		Schema::drop('users');
	}
}