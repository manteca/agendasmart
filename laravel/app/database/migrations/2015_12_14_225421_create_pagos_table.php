<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagosTable extends Migration {

	public function up()
	{
		Schema::create('pagos', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('cliente_id');
			$table->integer('tipo_id');
			$table->string('tipo', 7);
			$table->integer('app_pago_id');
			$table->string('app_pago', 15);
			$table->integer('valor');
			$table->integer('detalle_id');
			$table->string('detalle_table', 15);
			$table->text('detalle');
			$table->string('estado', 10);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('pagos');
	}
}