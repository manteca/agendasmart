<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetallePrecioEspecialTable extends Migration {

	public function up()
	{
		Schema::create('detalle_precio_especial', function(Blueprint $table) {
			$table->increments('ID_DETALLE');
			$table->integer('ID_ESPECIAL');
			$table->integer('ID_PLAZA');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('detalle_precio_especial');
	}
}