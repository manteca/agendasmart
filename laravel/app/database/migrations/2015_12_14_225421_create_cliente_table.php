<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClienteTable extends Migration {

	public function up()
	{
		Schema::create('cliente', function(Blueprint $table) {
			$table->increments('ID_CLIENTE');
			$table->integer('ID_PLAN');
			$table->integer('ID_RUBRO');
			$table->string('RUT', 15);
			$table->string('NOMBRE', 255);
			$table->string('CONTACTO', 255);
			$table->string('FONO', 255);
			$table->string('CELULAR', 255);
			$table->string('DIRECCION', 255);
			$table->string('SHORTNAME', 255);
			$table->date('INI_PLAN');
			$table->date('FIN_PLAN');
			$table->text('RESENA');
			$table->string('URL', 255);
			$table->string('LOGO', 50);
			$table->integer('usuario');
			$table->integer('clave');
			$table->string('CONFIRMACION', 2)->default('si');
			$table->text('flow_conf');
			$table->integer('user_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cliente');
	}
}