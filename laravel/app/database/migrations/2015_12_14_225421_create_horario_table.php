<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHorarioTable extends Migration {

	public function up()
	{
		Schema::create('horario', function(Blueprint $table) {
			$table->increments('id_horario');
			$table->integer('id_cliente');
			$table->integer('dia');
			$table->integer('desde');
			$table->integer('hasta');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('horario');
	}
}