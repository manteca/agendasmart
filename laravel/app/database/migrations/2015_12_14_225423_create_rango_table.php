<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRangoTable extends Migration {

	public function up()
	{
		Schema::create('rango', function(Blueprint $table) {
			$table->increments('ID_RANGO');
			$table->integer('ID_PLAZA');
			$table->integer('DIA');
			$table->time('DESDE');
			$table->time('HASTA');
			$table->integer('VALOR');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('rango');
	}
}