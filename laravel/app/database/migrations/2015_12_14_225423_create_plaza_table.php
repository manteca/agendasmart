<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlazaTable extends Migration {

	public function up()
	{
		Schema::create('plaza', function(Blueprint $table) {
			$table->increments('ID_PLAZA');
			$table->integer('ID_CLIENTE')->unsigned();
			$table->string('NOMBRE', 255);
			$table->time('HORARIO_INI');
			$table->time('HORARIO_FIN');
			$table->integer('tipo')->default('60');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('plaza');
	}
}