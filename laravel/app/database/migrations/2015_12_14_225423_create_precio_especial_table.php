<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrecioEspecialTable extends Migration {

	public function up()
	{
		Schema::create('precio_especial', function(Blueprint $table) {
			$table->increments('ID_ESPECIAL');
			$table->integer('ID_CLIENTE');
			$table->integer('DIA');
			$table->integer('MES');
			$table->integer('ANHO');
			$table->time('HORA_INICIO');
			$table->time('HORA_FIN');
			$table->integer('VALOR');
			$table->smallInteger('TIPO');
			$table->smallInteger('DIA_TIPO');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('precio_especial');
	}
}