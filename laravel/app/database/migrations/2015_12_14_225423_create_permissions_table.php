<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePermissionsTable extends Migration {

	public function up()
	{
		Schema::create('permissions', function(Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->string('name')->unique();;
			$table->string('display_name');
			$table->timestamps();
		});

		// Creates the permission_role (Many-to-Many relation) table
    Schema::create('permission_role', function ($table) {
        $table->increments('id')->unsigned();
        $table->integer('permission_id')->unsigned();
        $table->integer('role_id')->unsigned();
        $table->foreign('permission_id')->references('id')->on('permissions'); // assumes a users table
        $table->foreign('role_id')->references('id')->on('roles');
    });
	}	

	public function down()
	{
		Schema::drop('permission_role');
		Schema::drop('permissions');
	}
}