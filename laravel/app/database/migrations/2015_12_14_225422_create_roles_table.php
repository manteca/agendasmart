<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolesTable extends Migration {

	public function up()
	{
		Schema::create('roles', function(Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->string('name')->unique();
			$table->timestamps();
		});

		// Creates the assigned_roles (Many-to-Many relation) table
    Schema::create('assigned_roles', function ($table) {
        $table->increments('id')->unsigned();
        $table->integer('user_id')->unsigned();
        $table->integer('role_id')->unsigned();
        $table->foreign('user_id')->references('id')->on('users')
            ->onUpdate('cascade')->onDelete('cascade');
        $table->foreign('role_id')->references('id')->on('roles');
    });

	}

	public function down()
	{
		Schema::drop('assigned_roles');
		Schema::drop('roles');
	}
}