<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservaTable extends Migration {

	public function up()
	{
		Schema::create('reserva', function(Blueprint $table) {
			$table->increments('ID_RESERVA');
			$table->integer('ID_CLIENTE');
			$table->integer('ID_PLAZA');
			$table->time('HORARIO_INI');
			$table->time('HORARIO_FIN');
			$table->integer('DIA');
			$table->integer('MES');
			$table->integer('ANHO');
			$table->string('NOMBRE', 255);
			$table->string('RUT', 15);
			$table->string('TELEFONO', 50);
			$table->string('CELULAR', 50);
			$table->string('EMAIL', 100);
			$table->integer('TOTAL_RESERVA');
			$table->date('fecha');
			$table->string('estado', 10);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('reserva');
	}
}