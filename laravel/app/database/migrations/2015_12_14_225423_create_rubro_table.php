<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRubroTable extends Migration {

	public function up()
	{
		Schema::create('rubro', function(Blueprint $table) {
			$table->increments('ID_RUBRO');
			$table->string('NOMBRE', 255);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('rubro');
	}
}