<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgendaGruposTable extends Migration {

	public function up()
	{

		Schema::create('agenda_grupos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 255);
			$table->string('agendas', 255);
			$table->tinyInteger('default');
			$table->bigInteger('cliente_id')->unsigned();
			$table->boolean('estado')->default(1);
			$table->boolean('block')->default(0);
			$table->string('vista', 20);
			$table->timestamps();
		});

	}

	public function down()
	{
		Schema::drop('agenda_grupos');
	}
}