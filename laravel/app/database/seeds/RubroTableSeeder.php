<?php

class RubroTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('rubro')->delete();

		// actividades
		Rubro::create(array(
				'NOMBRE' => 'Actividades'
			));

		// alojamientos
		Rubro::create(array(
				'NOMBRE' => 'Alojamientos'
			));

		// arriendos
		Rubro::create(array(
				'NOMBRE' => 'Arriendos'
			));
	}
}