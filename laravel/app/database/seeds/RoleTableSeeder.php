<?php

class RoleTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('roles')->delete();

		// admin
		Role::create(array(
				'name' => 'Admin'
			));

		// cliente
		Role::create(array(
				'name' => 'Cliente'
			));

		// user
		Role::create(array(
				'name' => 'User'
			));
	}
}