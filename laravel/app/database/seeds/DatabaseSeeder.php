<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('PlanTableSeeder');
		$this->command->info('Plan table seeded!');

		$this->call('RoleTableSeeder');
		$this->command->info('Role table seeded!');

		$this->call('RubroTableSeeder');
		$this->command->info('Rubro table seeded!');

	}

}
