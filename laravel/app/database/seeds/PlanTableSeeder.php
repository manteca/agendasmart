<?php

class PlanTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('plan')->delete();

		// plan_gratis
		Plan::create(array(
				'NOMBRE' => 'Prueba Gratis',
				'VALOR' => 1,
				'USUARIOS' => 3,
				'RESERVAS' => 2000,
				'ESTADO' => 1
			));

		// plan_mensual
		Plan::create(array(
				'NOMBRE' => 'Plan Mensual',
				'VALOR' => 10000,
				'USUARIOS' => 5,
				'RESERVAS' => 5000,
				'ESTADO' => 1,
				'dias' => 31
			));

		// plan_anul
		Plan::create(array(
				'NOMBRE' => 'Plan Anual',
				'VALOR' => 90000,
				'USUARIOS' => 10,
				'RESERVAS' => 10000,
				'ESTADO' => 1,
				'dias' => 365
			));
	}
}