<?php
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

Event::listen('auth.login', function($event)
{
	// $d = ($event->cliente->flow_conf != NULL || $event->cliente->flow_conf != '')?true:false;
  Session::put('id_user', $event->id);
  if( Auth::user()->hasRole('Cliente') ){
  	Session::put('flow_available', ($event->cliente->flow_conf != NULL || $event->cliente->flow_conf != '')?true:false);
  }
  Session::put('id_cliente', $event->id_cliente);

  return false;
});

Event::listen('auth.logout', function($event)
{

  Session::flush();
  return false;
});


DB::listen(function($sql, $bindings, $time)
{
	// $logFile = 'DBlaravel.log';
	// $view_log = new Logger('View Logs');
	// $view_log->pushHandler(new StreamHandler(storage_path().'/logs/'.$logFile, Logger::INFO));

	// $temp = '';
	// foreach ($bindings as $key => $value) {
	// 	$temp .= '( '.$value.' )';
	// }
	// $view_log->addInfo('BD: '.$sql.' --- '.$temp);

});