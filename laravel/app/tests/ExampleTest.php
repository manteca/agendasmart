<?php

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicUrlNotLogin()
	{
		// True
		$this->call('GET', '/');
  	$this->assertResponseOk();

		// $crawler = $this->client->request('GET', '/users/login');
		// $this->assertTrue($this->client->getResponse()->isOk());
		$this->call('GET', '/users/login');
  	$this->assertResponseOk();

		// $crawler = $this->client->request('GET', '/users/create');
		// $this->assertTrue($this->client->getResponse()->isOk());
		$this->call('GET', '/users/create');
  	$this->assertResponseOk();

		// // False
		// $crawler = $this->client->request('GET', '/users/logout');
		// $this->assertRedirectedTo('/');
		$this->call('GET', '/users/logout');
		$this->assertRedirectedTo('/');
		
		// $crawler = $this->client->request('GET', '/agendas');
		// $this->assertRedirectedTo('/users/login');
		// $this->call('GET', '/agendas');
		// $this->assertRedirectedTo('/users/login');

		// $crawler = $this->client->request('GET', '/portal');
		// $this->assertRedirectedTo('/users/login');
	}

	public function testBasicUrlLogin()
	{
		// Auth::loginUsingId(2);

	}

}
