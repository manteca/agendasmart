<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


class ReservasClean extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'reservas:clean';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Borrar todas las resevras que no estan confirmadas y que tienen mas de 30min.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// $logFile = 'Cron_laravel.log';
		// $view_log = new Logger('View Cron Logs');
		// $view_log->pushHandler(new StreamHandler(storage_path().'/logs/'.$logFile, Logger::INFO));
		// $view_log->addInfo("User clicked", array('text' => 'additional message'));
		$monolog = new Logger('TestLog');
		$monolog->pushHandler(new Logentries\Handler\LogentriesHandler('b2a5a4df-821a-4b1e-9cfd-02af26e2b6bd'));
		print_r($monolog->addWarning('Cron runned3'));
		// echo "Hello World";
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
