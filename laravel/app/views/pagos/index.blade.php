@extends('layout.default')

@section('main')

<h1>Unidades de Medida</h1>
<div class="col-md-12">
<p>{{ 
  link_to_route('pagos.create', 'Nueva Medida', null, array('class'=>'btn btn-default')) 
}}</p>
@if ($pagos->count())
  <div class="col-md-6">
    <table class="table table-condensed table-vertical-center table-primary table-thead-simple">
      <thead>
        <tr>
          <th width="1%">Id</th>
          <th>Nombre</th>
          <th width="100">Acciones</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($pagos as $umedida)
          <tr>
            <td>{{ $umedida->id }}</td>
            <td>{{{ $umedida->nombre }}}</td>
            <td>
              {{ Form::open(array('method' => 'DELETE', 'route' => array('pagos.destroy', $umedida->id))) }}
                {{ HTML::decode(link_to_route('pagos.edit', '<i></i>', array($umedida->id), array('class' => 'btn-action glyphicons pencil btn-success'))) }}
                {{ Form::button('<i></i>', array('class' => 'btn-action glyphicons remove_2 btn-danger','type'=>'submit')) }}
              {{ Form::close() }}
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>

@else
  No hay Unidades de Medida

@endif
</div>
@stop
