<?php 

// $orden_id = $db->EntregaCampo(0);
// $orden_valor = $db->EntregaCampo(1);
// $orden_descripcion = $db->EntregaCampo(2);

$date = new DateTime();
$timestamp = $date->format('YmdHis');

$orden_compra = $orden_id;//$_POST['orden_compra    '];
$monto = $orden_valor;//$_POST['monto'];
$concepto = $orden_descripcion;//$_POST['concepto'];
// Se define en el metodo new_orde, por defecto y al momento de hacer  la llama de la clase, se debe optimizar.
// $tipo_comision = $flow_tasa_default;
$orden_tasa_comision = $orden_tasa_comision;
//Agregra el ID de comercio;
if(isset($flow_comercio))
  $flowAPI = new Flow\flowAPI($flow_comercio);
else
  $flowAPI = new Flow\flowAPI();
try {
	$flow_pack = $flowAPI->new_order($orden_compra, $monto, $concepto, $orden_tasa_comision);
	
} catch (\RuntimeException $e) {
	header('location: error.php?e='.$e);
}

?>
@extends('layout.default-clean')

@section('main')

<h1>Confirme su orden</h1>
<div class="row">
	<div class="col-md-8">
		<table class="table table-striped">
			<tr>
				<th class="text-right" width="180px;">Orden N°:</th>
				<td><?php echo $orden_compra?></td>
			</tr>
			<tr>
				<th class="text-right">Valor Reserva:</th>
				<td><?php echo $monto-($monto*0.0429)?></td>
			</tr>
			<tr>
				<th class="text-right">Valor Costo Reserva Online:</th>
				<td><?php echo $monto*0.0429 ?></td>
			</tr>
			<tr class="info">
				<th class="text-right">Valor Total:</th>
				<td><?php echo $monto?></td>
			</tr>
			<tr>
				<th class="text-right">Descripción:</th>
				<td><?php echo $concepto?></td>
			</tr>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		<form method="post" action="{{Config::get('flow.flow_url_pago'); }}">
		<input type="hidden" name="parameters" value="<?php echo $flow_pack ?>" />
		@if($hasCertificate)
			<button type="submit" class="btn btn-success">Generar Pago</button>
		@else
			<div class="bg-warning text-warning" style="padding:15px 15px 15px 15px;">
				<strong>Reserva Realizada</strong>, pero no se puede realizar el pago. Contactese con <strong>{{ $cliente->nombre }}</strong>
			</div>	
		@endif
		</form>
	</div>
</div>

@stop