@extends('layout.default')

@section('main')
	<h1>Reserva Index</h1>
	<div class="col-md-10">
		<div class="row">
			<table class="table">
        <thead>
            <tr>
                <th>Plaza</th>
                <th>Hora Inicio</th>
                <th>Hora Fin</th>
                <th>Fecha</th>
                <th>Nombre</th>
                <th>Rut</th>
                <th>Telefono</th>
                <th>Celular</th>
                <th>Email</th>
                <th>Total Reserva</th>
                <th>Acciones</center></th>
            </tr>
        </thead>
        <tbody>
        
        @foreach($reservas as $reserva)
          <tr>
              <td>{{ $reserva->plaza->NOMBRE }}</td>
              <td>{{ $reserva->horario_ini }}</td>
              <td>{{ $reserva->horario_fin }}</td>
              <td>{{ $reserva->fecha }}</td>
              <td>{{ $reserva->nombre; }}</td>
              <td>{{ $reserva->rut; }}</td>
              <td>{{ $reserva->telefono; }}</td>
              <td>{{ $reserva->celular; }}</td>
              <td>{{ $reserva->email; }}</td>
              <td>{{ $reserva->total_reserva; }}</td>
							<td>
  							<center>
                  {{ link_to_route('reservas.destroy', 'Quitar', $reserva->id_reserva, array('class'=> 'btn btn-small btn-danger','data-method' => 'delete', 'data-confirm' => "¿Estas seguro que deseas borrar la Reserva?")) }}
  							</center>
							</td>
                        
            </tr>
				@endforeach
        </tbody>
    </table>

		</div>
	</div>
@stop
