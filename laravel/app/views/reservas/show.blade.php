@extends('layout.default-clean')

@section('main')

<h1>Reserva Nº{{ $reserva->id_reserva }}</h1>
<div class="col-md-10">
	<div class="row">
		<div class="colmd-12">&nbsp;</div>
	</div>
  <div class="row">
    
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
  </div>
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
  </div>
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
  </div>
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
  </div>
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
  </div>

  <table class="table table-striped">
    <tr>
      <th class="text-right">Cliente:</th>
      <td>
        {{ $cliente->nombre }} <br>
        {{ $cliente->contacto }} <br>
        {{ $cliente->fono }} <br>
        {{ $cliente->celular }} <br>
        {{ $cliente->direccion }} <br>
        {{ $cliente->url }} <br>
      </td>
    </tr>
    <tr>
      <th class="text-right">Horario:</th>
      <td>Inicio: {{ $reserva->horario_ini }} - Fin: {{ $reserva->horario_fin }}</td>
    </tr>
    <tr>
      <th class="text-right">Fecha:</th>
      <td>{{ $reserva->dia }}/{{ $reserva->mes }}/{{ $reserva->anho }}</td>
    </tr>
    <tr>
      <th class="text-right">Nombre:</th>
      <td>{{ $reserva->nombre }}</td>
    </tr>
    <tr>
      <th class="text-right">Celular:</th>
      <td>{{ $reserva->celular }}</td>
    </tr>
    <tr>
      <th class="text-right">Correo:</th>
      <td>{{ $reserva->email }}</td>
    </tr>
    <tr>
      <th class="text-right">Valor Reserva:</th>
      <td>{{ $reserva->total_reserva }}</td>
    </tr>
  </table>
</div>
@stop
