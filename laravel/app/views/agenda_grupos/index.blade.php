@extends('layout.default')

@section('main')

<h1>Agrupar Agendas</h1>

<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
    {{ link_to_route('agenda_grupos.create','Crear Grupo',array(),array('class'=>'btn btn-xsmall btn-primary')) }}
    	<table class="table">
        <thead>
            <tr>
                <th data-field="nombre">NOMBRE</th>
                <th data-field="precios"><center>Agendas</center></th>
                <th data-field="precios"><center>Default</center></th>
                <th data-field="precios"><center>Estado</center></th>
                <th data-field="editar"><center>EDITAR</center></th>
                <th data-field="quitar"><center>QUITAR</center></th>
            </tr>
        </thead>
        <tbody>
        
        @foreach($agenda_grupos as $grupo)
          <tr>
              <td>{{ $grupo->nombre }}</td>
              <td>
              	<ul>
              	@foreach($grupo->agendas as $value)
              		<li>{{ $agendas[$value] }}</li>
              	@endforeach
              	</ul>
              </td>
              <td>
                @if($grupo->default)
                  <a href="{{ route('agenda_grupos.defecto', $grupo->id) }}">
                    <span class="glyphicon glyphicon-ok text-succes" aria-hidden="true"></span>
                  </a>
                @else
                  <a href="{{ route('agenda_grupos.defecto', $grupo->id) }}">
                    <span class="glyphicon glyphicon-remove text-danger" aria-hidden="true"></span>
                  </a>
                @endif
              </td>
              <td>
                @if($grupo->estado)
                  <a href="{{ route('agenda_grupos.estado', $grupo->id) }}">
                    <span class="glyphicon glyphicon-ok text-succes" aria-hidden="true"></span>
                  </a>
                @else
                  <a href="{{ route('agenda_grupos.estado', $grupo->id) }}">
                    <span class="glyphicon glyphicon-remove text-danger" aria-hidden="true"></span>
                  </a>
                @endif
              </td>
							<td>
  							<center>
                  {{ link_to_route('agenda_grupos.edit', 'Editar', $grupo->id, array('class'=> 'btn btn-small btn-warning')) }}
  							
  							</center>
							</td>

							<td>
  							<center>
                  @if(!$grupo->block)
                    {{ link_to_route('agenda_grupos.destroy', 'Quitar', $grupo->id, array('class'=> 'btn btn-small btn-danger','data-method' => 'delete', 'data-confirm' => "¿Estas seguro que deseas borrar la Agenda?")) }}
                  @endif
  							</center>
							</td>
                        
            </tr>
				@endforeach
        </tbody>
    </table>

    </div>
  </div>
</div>
@stop