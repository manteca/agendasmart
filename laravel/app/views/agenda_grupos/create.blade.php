@extends('layout.default')

@section('main')

<h1>Grupo</h1>

<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
    
	    {{ Form::open(array('route'=> 'agenda_grupos.store')) }}      
	    
		    <div class="form-group">
		      {{ Form::label('Nombre:') }}
		      {{ Form::text('nombre',Input::old('nombre'), array('id' => 'nombre', 'placeholder' => 'Ingrese Nombre del Grupo', 'class' => 'form-control')) }}
		      {{ $errors->first('nombre', '<p class="bg-danger has-success has-feedback"><small class="text-danger">:message</small></p>') }}
		    </div>
		    <div class="form-groupp">
		    	{{ Form::label('Tipo de Vista') }}
		    	{{ Form::select('tipo', $tipo_vista , null, array('id' => 'tipo')) }}
		    </div>
		    <div class="form-group">
		    	<h3>Seleccione las agendas que quieres agreagr a este grupo:</h3>
		    @foreach($agendas as $agenda)
				  <label class="checkbox-inline">
				    <input type="checkbox" id="agendas_{{ $agenda->id_plaza }}" class="tipo tipo_{{ $tipo_vista_union[$agenda->tipo] }}" name="agendas[]" value="<?php echo $agenda->id_plaza; ?>">{{ $agenda->nombre }} ({{ $agenda->tipo }})
				  </label> 
				@endforeach
					<br>
					{{ $errors->first('agendas', '<p class="bg-danger"><small class="text-danger">:message</small></p>') }}
				</div>
		    <div class="form-group">
		    	{{ Form::submit('Crear Grupo', array('class' => 'btn btn-success')) }}
		    </div>

		  {{ Form::close() }}

    </div>
  </div>
</div>
@stop

@section('javascript')
<script>
$(function(){


	$('.tipo').prop("disabled", true).prop('checked', false).parent().addClass('disable');
	$('.tipo_'+$('#tipo').val()).prop("disabled", false).parent().removeClass('disable');

	$('#tipo').change( function(){
		$('.tipo').prop("disabled", true).prop('checked', false).parent().addClass('disable');
		$('.tipo_'+$(this).val()).prop("disabled", false).parent().removeClass('disable');
	});
});
</script>
@stop