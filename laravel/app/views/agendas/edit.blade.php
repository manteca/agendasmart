@extends('layout.default')

@section('main')

<h1>Agendas</h1>

<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
    
	    {{ Form::open(array('route'=> array('agendas.update', $agenda->id_plaza), 'method' => 'put')) }}      
	    
		    <div class="form-group">
	        <label for="nombre">Nombre:</label>
	        <input type="text" id="nombre" name="nombre" placeholder="Ingrese Nombre Agenda (solo letras y números)" class="form-control" value="{{ $agenda->nombre}}">
		    </div>

		    <div class="form-group">
		    {{ Form::submit('Editar Agenda', array('class' => 'btn btn-warning')) }}
		    </div>
		  {{ Form::close() }}

    </div>
  </div>
</div>
@stop