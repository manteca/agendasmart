@extends('layout.default')

@section('main')

<h1>Agendas</h1>

<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
    
	    {{ Form::open(array('route'=> 'agendas.store')) }}      
	    
		    <div class="form-group">
		      <label for="nombre">Nombre:</label>
		      <input type="text" id="nombre" name="nombre" placeholder="Ingrese Nombre Agenda (solo letras y números)" class="form-control" value="">
		    </div>
		    <div class="form-group">
		    	{{ Form::label('Intervalo de reserva') }}
		    	{{ Form::select('tipo', $tipo ) }}
		    </div>
		    <div class="form-group">
		    {{ Form::submit('Crear Agenda', array('class' => 'btn btn-success')) }}
		    </div>
		  {{ Form::close() }}

    </div>
  </div>
</div>
@stop