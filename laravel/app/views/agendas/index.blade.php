@extends('layout.default')

@section('main')

<h1>Agendas</h1>

<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
    {{ link_to_route('agendas.create','Crear Agenda',array(),array('class'=>'btn btn-xsmall btn-primary')) }}
    	<table class="table">
        <thead>
            <tr>
                <th data-field="nombre">NOMBRE</th>
                <th data-field="tipo">Tipo</th>
                <th data-field="precios"><center>PRECIOS</center></th>
                <th data-field="editar"><center>EDITAR</center></th>
                <th data-field="quitar"><center>QUITAR</center></th>
            </tr>
        </thead>
        <tbody>
        
        @foreach($agendas as $agenda)
          <tr>
              <td>{{ $agenda->nombre; }}</td>
              <td>{{ $tipo[$agenda->tipo] }}</td>
							<td>
								<center>
								  {{ link_to_route('agendas.rangos.index','Configurar', $agenda->id_plaza, array('class' => 'btn btn-small btn-info')) }}
								</center>
							</td>                        

							<td>
  							<center>
                  {{ link_to_route('agendas.edit', 'Editar', $agenda->id_plaza, array('class'=> 'btn btn-small btn-warning')) }}
  							
  							</center>
							</td>

							<td>
  							<center>
                  {{ link_to_route('agendas.destroy', 'Quitar', $agenda->id_plaza, array('class'=> 'btn btn-small btn-danger','data-method' => 'delete', 'data-confirm' => "¿Estas seguro que deseas borrar la Agenda?")) }}
  							</center>
							</td>
                        
            </tr>
				@endforeach
        </tbody>
    </table>

    </div>
  </div>
</div>
@stop

@section('javascript')
<script>
(function() {

  var laravel = {
    initialize: function() {
      this.methodLinks = $('a[data-method]');

      this.registerEvents();
    },

    registerEvents: function() {
      this.methodLinks.on('click', this.handleMethod);
    },

    handleMethod: function(e) {
      var link = $(this);
      var httpMethod = link.data('method').toUpperCase();
      var form;

      // If the data-method attribute is not PUT or DELETE,
      // then we don't know what to do. Just ignore.
      if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
        return;
      }

      // Allow user to optionally provide data-confirm="Are you sure?"
      if ( link.data('confirm') ) {
        if ( ! laravel.verifyConfirm(link) ) {
          return false;
        }
      }

      form = laravel.createForm(link);
      form.submit();

      e.preventDefault();
    },

    verifyConfirm: function(link) {
      return confirm(link.data('confirm'));
    },

    createForm: function(link) {
      var form = 
      $('<form>', {
        'method': 'POST',
        'action': link.attr('href')
      });

      var token = 
      $('<input>', {
        'type': 'hidden',
        'name': 'csrf_token',
          'value': '<?php echo csrf_token(); ?>' // hmmmm...
        });

      var hiddenInput =
      $('<input>', {
        'name': '_method',
        'type': 'hidden',
        'value': link.data('method')
      });

      return form.append(token, hiddenInput)
                 .appendTo('body');
    }
  };

  laravel.initialize();

})();
</script>
@stop