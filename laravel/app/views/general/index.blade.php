<!-- agenda -->
@section('css')
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
  <link rel="stylesheet" media="print" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.print.css"/>
@stop

@section('main')
<h1>Agenda</h1>
{{ debug($errors->has()) }}
<div class="col-md-11">
  <div class="row">
    <div class="col-md-9" style="top:15px;">
      <!-- <div id="calendar"></div> -->
      {{ $calendario->calendar() }}
    </div>
    <div class="col-md-3" style="top:15px;">
      <h4>Grupos</h4>
      <ul>
        @if(isset($shortname))
          @foreach($grupos as $grupo)
            <li>{{ link_to_route( Route::currentRouteName(),$grupo->nombre, array('shortname' => $shortname,'grupo' => $grupo->id ) , array('class'=>($grupo_defecto->id == $grupo->id)?'btn btn-default active':'btn btn-default' )) }}</li>
          @endforeach
        @else
          @foreach($grupos as $grupo)
            <li>{{ link_to_route( Route::currentRouteName(),$grupo->nombre, array('grupo' => $grupo->id ) , array('class'=>($grupo_defecto->id == $grupo->id)?'btn btn-default active':'btn btn-default' )) }}</li>
          @endforeach
        @endif
      </ul>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Reserva para: <span id="fecha"></span></h4>
      </div>
        {{ Form::open(array('route'=> 'reservas.store')) }}

        {{ Form::hidden('id_cliente', $cliente->id_cliente ,array('id' => 'cliente_id')) }}
        {{ Form::hidden('show_reservados', $reservados ,array('id' => 'show_reservados')) }}
        {{ Form::hidden('checkpago',$checkpago ) }}
        {{ Form::hidden('dia', '' ,array('id' => 'dia')) }}
        {{ Form::hidden('mes', '' ,array('id' => 'mes')) }}
        {{ Form::hidden('anho', '' ,array('id' => 'anho')) }}
      <div class="modal-body">

            <div class="form-group">
              <label for="servicios">Servicios:</label>
              <select id="servicios" name="id_plaza" class="form-control">
                <option value="">Cargando...</option>
              </select>
              @if ($errors->has('servicios')) <span class="text-danger">{{ $errors->first('servicios') }}</span> @endif
            </div>
            <div class="form-group">
              <label>Desde: </label>
              <input type="text" id="hora_ini" name="horario_ini" class="form-control" value="{{Input::old('horario_ini')}}" readonly>
              <label>Hasta: </label>
              <input type="text" id="hora_fin" name="horario_fin" class="form-control" value="{{Input::old('horario_fin')}}" readonly>
            </div>
            <div class="form-group">
              <label for="nombre">Nombre</label>
              <input type="text" id="nombre" name="nombre" placeholder="Ingrese Nombre" class="form-control" value="{{Input::old('nombre')}}">
               @if ($errors->has('nombre')) <span class="text-danger">{{ $errors->first('nombre') }}</span> @endif
            </div>
            <div class="form-group">
              <label for="rut">RUT:</label>
              <input type="text" id="rut" name="rut" placeholder="Ingrese RUT" class="form-control" value="{{Input::old('rut')}}">
              @if ($errors->has('rut')) <span class="text-danger">{{ $errors->first('rut') }}</span> @endif
            </div>
            <div class="form-group">
              <label for="telefono">Teléfono (Optional): </label>
              <input type="text" id="telefono" name="telefono" placeholder="Ingrese Teléfono" class="form-control" value="{{Input::old('telefono')}}">
              @if ($errors->has('telefono')) <span class="text-danger">{{ $errors->first('telefono') }}</span> @endif
            </div>
            <div class="form-group">
              <label for="celular">Celular:</label>
              <input type="text" id="celular" name="celular" placeholder="Ingrese Celular" class="form-control" value="{{Input::old('celular')}}">
              @if ($errors->has('celuler')) <span class="text-danger">{{ $errors->first('celuler') }}</span> @endif
            </div>
            <div class="form-group">
              <label for="email">E-Mail:</label>
              <input type="email" id="email" name="email" placeholder="Ingrese E-Mail" class="form-control" value="{{Input::old('email')}}">
              @if ($errors->has('email')) <span class="text-danger">{{ $errors->first('email') }}</span> @endif
            </div>
            <div class="form-group">
              <label for="valor">Valor:</label>
              <input type="text" id="precio_div" name="total_reserva" class="form-control" value="{{Input::old('valor')}}" readonly>
            </div>
            <div class="form-group">
              {{ Form::captcha() }}
              @if ($errors->has('g-recaptcha-response')) <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span> @endif
            </div>
            <div id="debug"></div>
        <!--  -->
      </div>
      <div class="modal-footer">
        &nbsp;
        {{ Form::button('Cerrar', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
        {{ Form::submit('Reservar', array('class' => 'btn btn-primary')) }}
      </div>
    {{ Form::close() }}
    </div>
  </div>
</div>


@stop

@section('javascript')
  <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script> -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
  <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script> -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.4.0/fullcalendar.min.js"></script>
  {{ HTML::script('fullcalendar/lang-all.js') }}
  <script>
    $(document).ready(function() {
      @if($errors->has())
        $('#myModal').modal('show');
        var dataArray = {
              cliente_id: $('#cliente_id').val(),
              horario_ini: $('#hora_ini').val(),
              horario_fin: $('#hora_fin').val(),
              anho: $('#dia').val(),
              mes: $('#mes').val(),
              dia: $('#anho').val()
            };
        $.post("/reservas/horarioDisponible",dataArray).done(function(data){
          $servicio = $("#servicios");
          $servicio.find("option").remove().end().append(
            $("<option></option>")
              .attr("value", "")
              .text("Seleccione..."));
            $.each(JSON.parse(data), function (key, value) {
              $servicio.append(
                $("<option></option>")
                  .attr("value", value.id_plaza)
                  .text(value.nombre));
            });
          });
      @endif
      $('#servicios').on('change', function() {
         var dataArray = {
          hora_ini: $('#hora_ini').val(),
          hora_fin:$('#hora_fin').val(),
          fecha: $('#fecha').text(),
          cliente_id: $('#cliente_id').val(),
          dia: $('#dia').val(),
          mes: $('#mes').val(),
          anho: $('#anho').val(),
          id_plaza: $('#servicios').val()
        };
        $.post('/rango/precio', dataArray).done(function(data){
          data = JSON.parse(data);
          $('#precio_div').val(data['valor']);
        });
      });

    });
  </script>
  {{ $calendario->script() }}
@stop