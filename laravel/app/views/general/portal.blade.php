@extends('layout.default')

@section('main')
<div class="col-md-10">
  <div class="row">
    <div class="col-md-11">
			<div>
				<!--  -->
      <?php if($cliente['shortname']){ ?>
				<div class="col-md-12">
          <h1 class="page-header">Bienvenido</h1>
          <dl class="dl-horizontal">
            <dt>URL Agenda</dt>
            <dd>
                {{ link_to_route('agenda.calendario',route('agenda.calendario', array($cliente['shortname'])),array($cliente['shortname']),array('target'=>'_blank')) }}
            </dd>
            <dt>Fecha Término del Plan</dt>
            <dd>
            
                <?php if($planCaducado['expired']){ ?>
                <div>Plan Caducado <a href="pagos/proceso_plan/<?php echo $cliente['id_cliente'].'/'.$cliente['id_plan'] ?>" class="btn btn-primary">Renovar</a></div>
                <?php }else{ ?>
                <diV><?php $date = new Date($cliente['fin_plan']); echo ($planCaducado['free'])?'
                <i class="glyphicon glyphicon-question-sign"></i>':$date->format('d-m-Y').'<br>'; //$cliente['fin_plan'] ?> 
                  Plan Caduca <?php echo $planCaducado['time'] ?></diV>
                <?php } ?>
                </dd>
            {{-- Escondido hasta que se habilite  --}}
            
            <!--
            <dt>Mejorar plan a:</dt>
            <dd>
              <?php if($planUpgrade['plan']){?>
                  <a href="#" class="btn btn-primary">Plan (<?php echo $planUpgrade['plan'] ?>)</a>
                <?php } ?> 
            </dd>
            -->

            <dt><hr></dt>
            <dd><hr></dd>
            </dl>
          <div class="row">
            <div class="col-md-12">
              <p>Copiar y pegar codigo del boton para que puede mostrarlo en su pagian web: </p>
                <!-- TODO: (El codigo debe aparecer en un modulo POPUP) -->
                <pre>
            &lt;h2&gt; 
              &lt;a href="{{ route('agenda.calendario', array($cliente['shortname'])) }}" &gt;
              Reservas en Línea 
              &lt;/a&gt;
            &lt;/h2&gt;
                </pre>
                
            </div>
          </div>
        </div>
<?php } ?>  
				<!--  -->
	 		</div>
		</div>
	</div>
</div>

@stop