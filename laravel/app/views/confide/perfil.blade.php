@extends('layout.default')

@section('main')
<h1>Perfil</h1>
<div class="col-md-10">
  <div class="row">
  		@if ($errors->any())
			  <div class="row">
			    <div class="col-md-11 bg-danger" style="padding:15px 15px 5px 15px;">
			      @foreach($errors->all() as $e)
			      	<div>{{ $e }}</div>
			      @endforeach
			    </div>
			  </div>
			@endif
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Datos Personales</h3>
		  </div>
		  <div class="panel-body">
		    {{ Form::open(array('route'=> array('usuarios.update', $user->id), 'method' => 'put')) }}
			  	<div class="form-group form-inline">
			      <label for="username">Username</label>
			      <input class="form-control" placeholder="Username" type="text" name="username" id="username" value="{{ $user->username }}" disabled>
			    
		        <label for="email">Email</label>
		        <input class="form-control" placeholder="Email" type="text" name="email" id="email" value="{{ $user->email }}">
		      </div>
					<div class="form-group">
						<div class="checkbox"><label><input type="checkbox" id="cambio_contrasena" name="cambio_contrasena" value="1"><strong>Cambiar Contraseña</strong></label></div>
					</div>
					<div class="form-group form-inline" style="display:none;">
		        <label for="password">Password</label>
		        <input class="form-control" placeholder="Password" type="password" name="password" id="password">
		      </div>
		      <div class="form-group form-inline" style="display:none;">
		        <label for="password_confirmation">Confirm Password</label>
		        <input class="form-control" placeholder="Confirm Password" type="password" name="password_confirmation" id="password_confirmation">
		      </div>
		      <div class="form-actions form-group">
		        <button type="submit" class="btn btn-default">Modificar</button>
		      </div>
			  {{ Form::close() }}
		  </div>
		</div>
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Datos Empresa</h3>
		  </div>
		  <div class="panel-body">
		    {{ Form::open(array('class' => 'form-horizontal')) }}
		    
		    <div class="form-group">
		    	{{ Form::label('Rut', '', array('class' => 'col-md-2 text-right')) }}
		    	<div class="col-md-4">
		    		{{ Form::text('rut', $user->cliente->rut, array('class' => 'form-control')) }}
		    	</div>
		    </div>

		    <div class="form-group">
		    	{{ Form::label('Nombre', '', array('class' => 'col-md-2 text-right')) }}
		    	<div class="col-md-4">
		    		{{ Form::text('nombre', $user->cliente->nombre, array('class' => 'form-control')) }}
		    	</div>
		    </div>

		    <div class="form-group">
		    	{{ Form::label('contacto', 'Mail-Contaco', array('class' => 'col-md-2 text-right')) }}
		    	<div class="col-md-4">
		    		{{ Form::text('contacto', $user->cliente->contacto, array('class' => 'form-control')) }}
		    	</div>
		    </div>

		  </div>
		</div>

  	
	</div>
</div>
@stop

@section('javascript')
<script type="text/javascript">
	$(document).ready(function(){
		
		$("#cambio_contrasena").click(function() {
	    if($(this).is(":checked")) {
	        $('#password').parent().show(300);
					$('#password_confirmation').parent().show(300);
	    } else {
	    		$('#password').parent().hide(300);
					$('#password_confirmation').parent().hide(300);
	    }
		});
	});
</script>
@stop