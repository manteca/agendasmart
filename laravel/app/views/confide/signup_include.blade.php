<!--  sectio on frontpage -->
<section id="registro">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4 text-center">
        <h2 class="section-heading">Registro</h2>
        <i class="fa fa-pencil-square-o fa-5x azul"></i>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-md-offset-4 text-center">
        <!-- Se debe hacer un include del formulario  -->
        <div class="alert alert-info" role="alert">
        <h4>Momentaneamente Inhabilitado.</h4>
        <p>En estos momentos estamos en BETA. <br>Si te interesa participar y probar nuestra plataforma, escribenos en el formulario de contacto o al correo <a href="mailto:contacto@agendasmart.cl">contacto@agendasmart.cl</a></p>
        <p>¡Te Esperamos!</p>
        </div>
      </div>
    </div>
  </div>
</section>