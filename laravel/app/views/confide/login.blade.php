@extends('layout.frontend.index')

@section('main')
<section id="login">
<div class="container">
@if(Session::has('message')&& Session::has('message-type'))
  <div class="row">
    <div class="col-md-8 col-md-offset-2  alert alert-{{ Session::get('message-type') }} alert-dismissible" id="message-login" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <p class="">{{ Session::get('message') }}</p>
    </div>
  </div>
@endif

<div class="row">
  <div class="col-md-4 col-md-offset-4">
    <div class="row">
      <div class="col-md-12 text-center">
        <img src="{{ url('img/logo-blanco.png') }}" alt="logo">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="titulo text-center">Ingresa</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        
          <form class="form-horizontal" role="form" method="POST" action="{{{ URL::to('/users/login') }}}" accept-charset="UTF-8">
              <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
              <div class="form-group">
                <label for="email" class="sr-only">{{{ Lang::get('confide::confide.e_mail') }}}</label>
                  <input class="form-control" tabindex="1" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
              </div>
              <div class="form-group">
                <label for="password" class="sr-only">{{{ Lang::get('confide::confide.password') }}}</label>
                <input class="form-control" tabindex="2" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
              </div>
              <div class="form-group form-inline">
                <div class="checkbox">
                  <label for="remember" class="celeste">
                      <input tabindex="4" type="checkbox" name="remember" id="remember" value="1"> {{{ Lang::get('confide::confide.login.remember') }}}
                  </label>
                </div>
                <button tabindex="3" type="submit" class="btn btn-default btn-agendasmart pull-right">{{{ Lang::get('confide::confide.login.submit') }}}</button>
              </div>

              <p class="text-center">
                <a href="{{{ URL::to('/users/forgot_password') }}}" class="celeste">{{{ Lang::get('confide::confide.login.forgot_password') }}}</a>
              </p>
              @if (Session::get('error'))
                  <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
              @endif

              @if (Session::get('notice'))
                  <div class="alert">{{{ Session::get('notice') }}}</div>
              @endif
          </form>
        
      </div>
    </div>
  </div>
</div>
</div>
</section>
@stop