<!-- Form for signup -->

  
        {{ Form::open( array('url' => URL::to('users')) ) }}
        
        <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">

          <div class="form-group">
            {{ Form::label('plan','Plan',array('class' => 'sr-only')) }}
            {{ Form::select('plan', 
              $planes,
              array(),
              array('class' => 'form-control')
            ) }}
          </div>
          <div class="form-group">
            {{ Form::label('rubro','Rubro',array('class' => 'sr-only')) }}
            {{ Form::select('rubro', 
              $rubro,
              array(),
              array('class' => 'form-control')
            ) }}
          </div>
          <div class="form-group">
            {{ Form::label('empresa','Nombre Empresa',array('class' => 'sr-only')) }}
            {{ Form::text('empresa', '',array('class' => 'form-control', 'placeholder' => 'Nombre Empresa')) }}
          </div>
          

          <!--
          <div class="form-group">
            {{ Form::label('shortname','Shortname',array('class' => 'sr-only')) }}
            {{ Form::text('shortname', '',array('class' => 'form-control', 'placeholder' => 'Nombre Corto')) }}
            <span class="help">Link para tu AgendaSmart: http://www.agendasmart.cl/visita/<span class="f-light celeste">Shortname<span>/</span>
          </div>

          <div class="form-group">
            {{ Form::label('username','Nombre Usuario',array('class' => 'sr-only')) }}
            {{ Form::text('username', '',array('class' => 'form-control', 'placeholder' => 'Nombre de Usuario')) }}
          </div>
          -->

          
          <div class="form-group">
            {{ Form::label('email','Email',array('class' => 'sr-only')) }}
            {{ Form::text('email', '',array('class' => 'form-control', 'placeholder' => 'Email')) }}
          </div>
          <div class="form-group">
            {{ Form::label('password','Password',array('class' => 'sr-only')) }}
            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Contraseña')) }}
          </div>
          <div class="form-group">
            {{ Form::label('password_confirmation','Confirmar Password',array('class' => 'sr-only')) }}
            {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Confirmar Contraseña')) }}
          </div>
          <div class="form-group form-inline">
            {{ Form::button('Enviar', array('class' => 'btn btn-default btn-agendasmart pull-right', 'type' => 'submit')) }}
          </div>

          @if (Session::get('notice'))
            <div class="alert">{{ Session::get('notice') }}</div>
          @endif

          @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
              @if (is_array(Session::get('error')))
                {{ head(Session::get('error')) }}
              @endif
            </div>
          @endif

        {{ Form::close() }}