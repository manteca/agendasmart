@extends('layout.frontend.index')
@section('main')

<section id="login">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="row">
          <div class="col-md-12 text-center">
            <img src="{{ url('img/logo-blanco.png') }}" alt="logo">
          </div>
        </div>
         <div class="row">
          <div class="col-md-12">
            <h2 class="titulo text-center">Nueva Contraseña</h2>
          </div>
        </div>
        <div class="row">
            <form method="POST" action="{{{ URL::to('/users/reset_password') }}}" accept-charset="UTF-8">
                <input type="hidden" name="token" value="{{{ $token }}}">
                <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">

                <div class="form-group">
                    <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
                </div>
                <div class="form-group">
                    <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="password_confirmation">
                </div>

                @if (Session::get('error'))
                    <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
                @endif

                @if (Session::get('notice'))
                    <div class="alert">{{{ Session::get('notice') }}}</div>
                @endif

                <div class="form-actions form-group">
                    <button type="submit" class="btn btn-primary">{{{ Lang::get('confide::confide.forgot.submit') }}}</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
