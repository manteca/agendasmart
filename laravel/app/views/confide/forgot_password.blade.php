@extends('layout.frontend.index')
@section('main')

<section id="login">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="row">
          <div class="col-md-12 text-center">
            <img src="{{ url('img/logo-blanco.png') }}" alt="logo">
          </div>
        </div>
         <div class="row">
          <div class="col-md-12">
            <h2 class="titulo text-center">Recupera tu Contraseña</h2>
          </div>
        </div>
        <div class="row">
          <form method="POST" action="{{ URL::to('/users/forgot_password') }}" accept-charset="UTF-8">
              <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
          
              <div class="form-group">
                  <div class="input-append input-group">
                      <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
                      <span class="input-group-btn">
                          <input class="btn btn-default" type="submit" value="{{{ Lang::get('confide::confide.forgot.submit') }}}">
                      </span>
                  </div>
              </div>

              @if (Session::get('error'))
                  <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
              @endif

              @if (Session::get('notice'))
                  <div class="alert">{{{ Session::get('notice') }}}</div>
              @endif
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

@stop