@extends('layout.default')

@section('main')
<h1>Editar Horario</h1>
<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
      {{ Form::open(array('route'=> array('horarios.update', $horario->id_horario ), 'method' => 'put')) }}      
        <div class="form-group">
          <label for="desde">Desde:</label>
          <select id="desde" name="desde">    
            <?php for($i=1;$i<=24;$i++){ ?>
        		  <option value="<?php echo $i; ?>:00" <?php if($i==$horario->desde){ ?> SELECTED<?php } ?>>
              <?php echo $i; ?>:00</option>
        		<?php } ?>  
          </select>
                

          <label for="hasta">Hasta:</label>
          <select id="hasta" name="hasta">
            <?php for($j=1;$j<=24;$j++){ ?>
        		  <option value="<?php echo $j; ?>:00" <?php if($j==$horario->hasta){ ?> SELECTED<?php } ?>><?php echo $j; ?>:00</option>
        		<?php } ?> 
          </select>
        </div> 
        <br>

        <div class="form-group">
          <button type="submit" class="btn btn-warning">Editar Horario</button>
        </div>      
      {{ Form::close()}}
    </div>
  </div>
</div>
@stop