<?php 
// Buffers de Operacin
// $mensaje = ''; // Buffer de mensaje
// $a = intval($_REQUEST['a']); // BUffer de accin
// $d = 0; // Buffer despliague
// $horarios = new Horarios();


// Lâ€”gica de negocio
// switch($a) {
// 	case 1: // Solicitar crear
// 		$d = 1;
// 		break;
			
// 	case 3: // Solicitud de edicin

// 		$id_horario = intval($_REQUEST['id_horario']);
			
// 		if(!$horarios->Selecciona($id_horario)) {
// 			$d = 1;
// 			$mensaje = $horarios->EntregaError();
// 			break;
// 		}

// 		$id_horario = $horarios->Id_horario();
// 		$id_cliente = $horarios->Id_cliente();
// 		$dia = $horarios->Dia();
// 		$hora_inicio = $horarios->Desde();
// 		$hora_fin = $horarios->Hasta();

// 		$d = 1;
// 		break;
			
// 	case 4: // Actualizacin
		
// 		$id_horario = intval($_REQUEST['id_horario']);
// 		$id_cliente = intval($_SESSION['id_cliente']);
// 		$desde = intval($_REQUEST['desde']);
// 		$hasta = intval($_REQUEST['hasta']);
			
// 		if(!$horarios->Actualiza($id_horario,$id_cliente,$desde,$hasta)) {
// 			$d = 1;
// 			$mensaje = $horarios->EntregaError();
// 			break;
// 		}
		
// 		echo '<meta http-equiv="refresh" content="0;URL='.$web_url.'admhorario/">';
// 		die();
// 		$mensaje = 'Se ha actualizado el horario.';
// 		break;

		
// }


//$web_url = 'xxxx';
?>
@extends('layout.default')

@section('main')

<h1>Horarios</h1>

<div class="col-md-10">

    <div class="row">
        <div class="col-md-11" style="top:15px;">
            <h3><center>¿Qu&eacute; considerar para definir tu Horario?</center></h3>

            <table class="table" align="center" border="2">
                <tr>
                <td><center><b>ACTIVIDADES</b></center></td>
                <td><center><b>ALOJAMIENTOS</b></center></td>
                <td><center><b>ARRIENDOS</b></center></td>
                </tr>

                <tr>
                <td style="padding:5px;">Define tu Horario considerando los d&iacute;as de funcionamiento y la hora de apertura y cierre de tus instalaciones o servicios.<br><br><center>Ej: 8:00am - 19:00pm.</center></td>
                <td style="padding:5px;">Define solo una hora al d&iacute;a como tu horario de funcionamiento, la misma hora para todos los d&iacute;as de la semana. <br><br>Ese horario de agendamiento ser&aacute; equivalente a una noche de reserva.<br><br><center>Ej: 8.00am - 9:00am</center></td>
                <td style="padding:5px;">Si tus arriendos son por hora (1): considera los d&iacute;as de funcionamiento y hora de apertura y cierre de tus instalaciones o servicios.<br><br>Si tus arriendos son por d&iacute;a completo (2): define una hora al d&iacute;a como tu horario de funcionamiento, la misma hora para todos los d&iacute;as de la semana en que tus servicios est&eacute;n disponibles. Ese horario de agendamiento ser&aacute; equivalente a un d&iacute;a de reserva.<br><br><center>Ej(1): 8.00am - 19:00pm<br>Ej(2): 8.00am - 9:00am</center></td>
                </tr>
            </table>
            <br>

            <table class="table">
                <thead>
                    <tr>
                        <th data-field="dia"><center>DIA</center></th>
                        <th data-field="desde"><center>DESDE</center></th>
                        <th data-field="hasta"><center>HASTA</center></th>
                        <th data-field="editar"><center>EDITAR</center></th>
                    </tr>
                </thead>
                <tbody>
                            
                <?php //while($horarios->Listar($_SESSION['id_cliente'])){ ?>
                @foreach ($horarios as $horario)         
                    <tr>
                        <td>
                        <?php if($horario->dia==1){ ?>Lunes<?php } ?>
                        <?php if($horario->dia==2){ ?>Martes<?php } ?>
                        <?php if($horario->dia==3){ ?>Mi&eacute;rcoles<?php } ?>
                        <?php if($horario->dia==4){ ?>Jueves<?php } ?>
                        <?php if($horario->dia==5){ ?>Viernes<?php } ?>
                        <?php if($horario->dia==6){ ?>S&aacute;bado<?php } ?>
                        <?php if($horario->dia==7){ ?>Domingo<?php } ?>
                        </td>
                        <td><center><?php echo $horario->desde; ?>:00</center></td>
                        <td><center><?php echo $horario->hasta; ?>:00</center></td>            
                        <td>
                            <center>
                            {{ link_to_route('horarios.edit','Editar', $horario->id_horario ,array('class'=> 'btn btn-small btn-warning')) }}
                            </center>
                        </td>       
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
