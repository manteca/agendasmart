@extends('layout.default')

@section('main')
<h1>Configuracion de Pago</h1>

@if ($errors->any())

  <div class="row">
    <div class="col-md-11 bg-danger" style="padding:15px 15px 5px 15px;">
      
      @if($errors->first('code') !== '' )
        <p class="text-center text-danger">Error al crear usuarios: <strong> {{ Config::get('flow.flow_err')[$errors->first('code')] }} </strong></p>
      @else
        <p class="text-center text-danger">Error en el formulario. favor de revisar y volver a intentar.</p>
      @endif

    </div>
  </div>
  <br>
@endif
<div class="col-md-11">

      <div class="row" style="top:15px;">
        <div class="col-md-8">
          {{ Former::open('clientes/flowConfigStore')
              ->rules(array(
                  'email' => 'required|max:50',
                ))
              ->method('POST')}}
          {{ Form::hidden('cliente_id', $cliente_id) }}

          {{ Former::text('email')
            ->required()
            ->help('Correo al cual le llegaran los comprobantes de pago')
            ->prepend('@')
            ->placeholder('correo@dominio.cl') }}
          {{-- Former::text('clave')
            ->required()
            ->placeholder('')
            ->help('ayuuda') --}}
          {{-- Former::text('pagos')
            ->required()
            ->placeholder('')
            ->help('ayuuda') --}}
          <h3>Datos para deposito en Banco</h3>
          {{ Former::text('rut')
            ->required()
            ->placeholder('11222333-2')
            ->help('Rut de la cuenta') }}
          {{ Former::text('nombres')
            ->required()
            ->placeholder('Sebastian')
            ->help('Nombre del dueño de la cuenta') }}
          {{ Former::text('apellidos')
            ->required()
            ->placeholder('Sanchez')
            ->help('Apellido del dueño de la cuenta') }}
          {{ Former::select('banco')->options($bancos)
            ->placeholder('Seleccion Banco...')
            ->help('Banco al cual se debe despositar') }}
          {{ Former::select('cuenta')->options($tipo_cuenta)
            ->required()
            ->placeholder('Tipo de Cuenta...')
            ->help('Tipo de Cuenta') }}
          {{ Former::text('numeroCta')
            ->label('Numero de Cuenta Corriente')
            ->prepend('#')
            ->required()
            ->placeholder('399912993')
            ->help('Numero de la cuenta, sin puntos ni guiones.') }}
           {{ Former::checkbox('factura')
            ->inlineHelp('¿Desea recibir factura una Factura Excenta por los pagos realizado? ')->stacked()}}
            <div id="factura-container" style="display:none;">
              <h3>Datos para Facturacion</h3>
              <p>TEXTP_EXPLICATIVO DE LAS 2 FACTURAS QIE LLEGARAN</p>
              {{ Former::text('rutFactura')
                ->placeholder('11222333-4')
                ->help('Rut de la Factura, Sin puntos.') }}
              {{ Former::text('razonSocial')
                ->placeholder('Perez Rodriguez. ltda.')
                ->help('Razon Social') }}
              {{ Former::text('direccion')
                ->placeholder('Bustamanet 76')
                ->help('Direccion asociada a la Facturación') }}
              {{ Former::text('comuna')
                ->placeholder('Providencia')
                ->help('Comuna asociada a la Facturación') }}
              {{ Former::text('ciudad')
                ->placeholder('Santiago')
                ->help('Ciudad asociada a la Facturación') }}
              {{ Former::text('telefono')
                ->placeholder('22334455')
                ->help('Telefono asociada a la Facturación') }}
              {{ Former::text('giro')
                ->placeholder('Restaurant')
                ->help('Tipo de Giro de la Facturación.') }}
              {{ Former::text('nombreFantasia')
                ->label('Nombre de Fantasia')
                ->placeholder('La Lechería')
                ->help('El nombre con el cual se conoce a la solciedad') }}
          </div>
          {{ Former::framework('Nude')}}
          <div class="form-group">
            <div class="checkbox col-lg-10 col-sm-8 col-lg-offset-2 col-sm-offset-4">
                <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#myModal">
                  Leer Acuerdo y condiciones de uso de Flow como medio de pago
                </button>
                {{ Former::checkbox('acuerdo')
                    ->required()
                    ->label('')
                    ->check()
                    ->text('Declaro haber leído y acepto los términos y condiciones del servicio de <a href="http://www.flow.cl" target="_blank">www.flow.cl</a>')}}
              
            </div>
          </div>
          {{Former::framework('TwitterBootstrap3')}}
          {{ Former::actions()
              ->success_submit('Submit')
              ->inverse_reset('Reset') }}

          {{ Former::close() }}
        </div>
        <div class="col-md-4">
          <h3>WikiAgenda</h3>
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    Horarios
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  En <strong>horarios</strong> debes indicar la hora de apertura y cierre de tu negocio
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>



</div>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">AgendaSmart - Flow</h4>
      </div>
      <div class="modal-body">
        @include('disclosure.agendasmart-flow')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        {{-- <button type="button" class="btn btn-primary">Aceptar</button> --}}
      </div>
    </div>
  </div>
</div>
@stop

@section('javascript')
<script type="text/javascript">
  $(document).ready(function(){
    if($("#factura").is(":checked")) {
      $('#factura-container').show(300);
    }

    $("#factura").click(function() {
      if($(this).is(":checked")) {
        $('#factura-container').show(300);
      } else {
        $('#factura-container').hide(300);
      }
    });
  });
</script>
@stop
