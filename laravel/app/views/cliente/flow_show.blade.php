@extends('layout.default')

@section('main')
<h1>Vista Configuracion de Pago</h1>

<div class="col-md-6">
  <div class="row">
    <div class="col-md-12" style="top:15px;">
    @if(Session::get('pass'))
      <div class="row">
        <div class="col-md-11 bg-info" style="padding:15px 15px 5px 15px;">
          <p class="text-center text-info">La contraseña creada es <strong> {{{Session::get('pass')}}} </strong></p>
          <p>ATENCIÓN: Antes de poder recibir pagos en linea, debes relizar 2 cosas:</p>
          <ul>
            <li>Confirmar el link en el correo que te envia flow a la dirección emial que acabas de registrar.</li>
            <li>Una vez confirmada la cuenta con el link, debes descargar el certificado presionado el boton "Descargar Certificado"</li>
          </ul>
        </div>
      </div>
      <br>
    @endif
  {{ debug($bancos); debug($cliente);}}
      <div class="row">
          <div class="text-right col-lg-3 col-sm-4"><strong>Email:</strong></div>
          <div class="col-lg-9 col-sm-8">{{ $flow_config['email'] }}</div>
      </div>
      <div class="row">
          <div class="text-right col-lg-3 col-sm-4"><strong>Factura</strong></div>
          <div class="col-lg-9 col-sm-8">
            @if($flow_config['factura'])
              <span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span>
            @else
              <span class="glyphicon glyphicon-remove text-danger" aria-hidden="true"></span>
            @endif
          </div>
      </div>
      <div class="row">
          <div class="text-right col-lg-3 col-sm-4"><strong>Rut</strong></div>
          <div class="col-lg-9 col-sm-8">{{ $flow_config['rut'] }}</div>
      </div>
      <div class="row">
          <div class="text-right col-lg-3 col-sm-4"><strong>Nombres</strong></div>
          <div class="col-lg-9 col-sm-8">{{ $flow_config['nombres'] }}</div>
      </div>
      <div class="row">
          <div class="text-right col-lg-3 col-sm-4"><strong>Apellidos</strong></div>
          <div class="col-lg-9 col-sm-8">{{ $flow_config['apellidos'] }}</div>
      </div>
      <div class="row">
          <div class="text-right col-lg-3 col-sm-4"><strong>Banco</strong></div>
          <div class="col-lg-9 col-sm-8">{{ $bancos[$flow_config['banco']] }}</div>
      </div>
      <div class="row">
          <div class="text-right col-lg-3 col-sm-4"><strong>Cuenta</strong></div>
          <div class="col-lg-9 col-sm-8">{{ $flow_config['cuenta'] }}</div>
      </div>
      <div class="row">
          <div class="text-right col-lg-3 col-sm-4"><strong>Numero Cta.</strong></div>
          <div class="col-lg-9 col-sm-8">{{ $flow_config['numeroCta'] }}</div>
      </div>
      @if($flow_config['factura'])
        <div class="row">
            <div class="text-right col-lg-3 col-sm-4"><strong>Rut Factura</strong></div>
            <div class="col-lg-9 col-sm-8">{{ $flow_config['rutFactura'] }}</div>
        </div>
        <div class="row">
            <div class="text-right col-lg-3 col-sm-4"><strong>Razon Social</strong></div>
            <div class="col-lg-9 col-sm-8">{{ $flow_config['razonSocial'] }}</div>
        </div>
        <div class="row">
            <div class="text-right col-lg-3 col-sm-4"><strong>Direccion</strong></div>
            <div class="col-lg-9 col-sm-8">{{ $flow_config['direccion'] }}</div>
        </div>
        <div class="row">
            <div class="text-right col-lg-3 col-sm-4"><strong>Comuna</strong></div>
            <div class="col-lg-9 col-sm-8">{{ $flow_config['comuna'] }}</div>
        </div>
        <div class="row">
            <div class="text-right col-lg-3 col-sm-4"><strong>Ciudad</strong></div>
            <div class="col-lg-9 col-sm-8">{{ $flow_config['ciudad'] }}</div>
        </div>
        
        <div class="row">
            <div class="text-right col-lg-3 col-sm-4"><strong>Telefono</strong></div>
            <div class="col-lg-9 col-sm-8">{{ $flow_config['telefono'] }}</div>
        </div>
        <div class="row">
            <div class="text-right col-lg-3 col-sm-4"><strong>Giro</strong></div>
            <div class="col-lg-9 col-sm-8">{{ $flow_config['giro'] }}</div>
        </div>
        <div class="row">
            <div class="text-right col-lg-3 col-sm-4"><strong>Nombre Fantasia</strong></div>
            <div class="col-lg-9 col-sm-8">{{ $flow_config['nombreFantasia'] }}</div>
        </div>
      @endif
     

    </div>
  </div>
</div>
<div class="col-md-6">
   <button class="btn btn-default" id="btn-certificado">Descargar Certificado</button>
    <small class="help-block">Certificado que permitirá la comunicación con la plataforma de pago.</small>
    <div id="certificado_status"></div>
    <input id="client_id" type="hidden" value="{{$cliente->ID_CLIENTE}}"> 
</div>
@stop

@section('javascript')
  {{ HTML::script('js/cliente.flow_show.js') }}
@stop