@extends('layout.default')

@section('main')
<h1>Rangos para "{{ $agenda->nombre }}" para el dia {{ $dia_nombre }}</h1>
<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">

      {{ Form::open(array('route'=> 'agendas.rangos.store')) }}      
      <input type="hidden" name="id_plaza" value="{{ $agenda_id }}">
      <input type="hidden" name="dia" value="{{ $dia }}">
      @if($agenda->tipo == 1440)
      <input type="hidden" name="desde" value="00:00:00">
      <input type="hidden" name="hasta" value="23:59:00">
      @else
        <div class="form-group">
            <label for="desde">Hora Inicio:</label>
            <select name="desde" id="desde" class="form-control">
            @for($horaInicio ; $horaInicio<=24 ; $horaInicio++)
              <option value="{{ $horaInicio; }}:00">{{ $horaInicio; }}:00</option>
            @endfor
        </select>
        </div>
        <div class="form-group">
          <label for="hasta">Hora Término:</label>
          <select name="hasta" id="hasta" class="form-control">
          @for($horaFin ; $horaFin<=24 ; $horaFin++)
            <option value="{{ $horaFin; }}:00">{{ $horaFin; }}:00</option>
          @endfor
        </select>
        </div>
      @endif   
        <div class="form-group">
          <label for="valor">Valor:</label>
          <input type="text" id="valor" name="valor" placeholder="Ingrese Valor" class="form-control" >
        </div> 
         <div class="form-group">
          {{ Form::submit('Establecer Precio', array('class' => 'btn btn-success')) }}
        </div>
      {{ Form::close() }}

    </div>
  </div>
</div>
@stop