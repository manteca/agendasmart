@extends('layout.default')

@section('main')

<h1>Rangos para "{{ $agenda->nombre }}"</h1>
<h3>Tipo: {{ $tipo[$agenda->tipo] }}</h3>
<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
      @for ($i = 1; $i <= 7; $i++)
	    <h4>{{ $dias[$i] }}
      <?php 
      //$sala->UltimaHoraRango($_REQUEST['id_plaza'],1); 
      //if(str_replace(':00:00','',$sala->ultima_hora)!=24){
      ?>  
      <small>
      @if( ($agenda->tipo == 1440 && !isset($rangos[$i])) || ($agenda->tipo != 1440) )
        {{ link_to_route('agendas.rangos.new','Nuevo Rango de Precios',array($agenda->id_plaza, $i), array('class' => 'btn btn-primary btn-xs')) }}
      @endif
      @if($i > 1)
      <!-- <button type="button" class="btn btn-primary btn-xs" onclick="javascript:RepetirDiaAnterior({{ $i }},{{ $i-1 }},{{ $agenda->id_plaza }})">Repetir D&iacute;a Anterior</button> -->
      @endif
      </small>
    <?php //} ?>  
      </h4>
      <br>
        @if(isset($rangos[$i]))
    	  <ul>
        @foreach($rangos[$i] as $key => $rango)
        
    	  <li>Desde {{ $rango['desde'] }} 
        Hasta {{ $rango['hasta'] }}
        Valor: {{ $rango['valor'] }}
          {{ link_to_route('agendas.rangos.destroy', 'Quitar', array($agenda->id_plaza, $key), array('class'=> 'btn btn-small btn-danger','data-method' => 'delete', 'data-confirm' => "¿Estas seguro que deseas Quitar el rango de Horario?")) }}
        </li>
    	  @endforeach
    	  </ul>
        @endif
      <br>
      <hr>
      @endfor

    </div>
  </div>
</div>
@stop
