@extends('layout.default')

@section('main')
<h1>Usuarios</h1>
<div class="col-md-10">
  <div class="row">
  		@if ($errors->any())
			  <div class="row">
			    <div class="col-md-11 bg-danger" style="padding:15px 15px 5px 15px;">
			      @foreach($errors->all() as $e)
			      	<div>{{ $e }}</div>
			      @endforeach
			    </div>
			  </div>
			@endif

  	{{ Form::open(array('route'=> array('usuarios.update', $user->id), 'method' => 'put')) }}
	  	<div class="form-group">
	      <label for="username">Username</label>
	      <input class="form-control" placeholder="Username" type="text" name="username" id="username" value="{{ $user->username }}" disabled>
	    </div>
	    <div class="form-group">
        <label for="email">Email</label>
        <input class="form-control" placeholder="Email" type="text" name="email" id="email" value="{{ $user->email }}">
      </div>
			<div class="form-group">
				<div class="checkbox"><label><input type="checkbox" id="cambio_contrasena" name="cambio_contrasena" value="1"><strong>Cambiar Contraseña</strong></label></div>
			</div>
			<div class="form-group" style="display:none;">
        <label for="password">Password</label>
        <input class="form-control" placeholder="Password" type="password" name="password" id="password">
      </div>
      <div class="form-group" style="display:none;">
        <label for="password_confirmation">Confirm Password</label>
        <input class="form-control" placeholder="Confirm Password" type="password" name="password_confirmation" id="password_confirmation">
      </div>
      <div class="form-actions form-group">
        <button type="submit" class="btn btn-primary">Modificar</button>
      </div>
	  {{ Form::close() }}
	</div>
</div>
@stop

@section('javascript')
<script type="text/javascript">
	$(document).ready(function(){
		
		$("#cambio_contrasena").click(function() {
	    if($(this).is(":checked")) {
	        $('#password').parent().show(300);
					$('#password_confirmation').parent().show(300);
	    } else {
	    		$('#password').parent().hide(300);
					$('#password_confirmation').parent().hide(300);
	    }
		});
	});
</script>
@stop