@extends('layout.default')

@section('main')

<h1>Usuarios</h1>
<div class="col-md-10">
  <div class="row">
	  <div class="col-md-11" style="top:15px;">
	  	{{ link_to_route('usuarios.create','Crear Usuario',array(),array('class'=>'btn btn-xsmall btn-primary')) }}
			<table class="table">
				<thead>
					<tr>
						<th>Id</th>
						<th>E-Mail</th>
						<th>Usuario</th>
						<th>Editar</th>
						<th>Quitar</th>
					</tr>
				</thead>
				@foreach($users as $user)
				<tbody>
					<tr>
						<td>{{ $user->id }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->username }}</td>
						<td>{{ link_to_route('usuarios.edit', 'Editar', $user->id, array('class'=> 'btn btn-small btn-warning')) }}</td>
						<td>{{ link_to_route('usuarios.destroy', 'Quitar', $user->id, array('class'=> 'btn btn-small btn-danger','data-method' => 'delete', 'data-confirm' => "¿Estas seguro que deseas borrar la Agenda?")) }}</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>
	</div>    
</div>

@stop
