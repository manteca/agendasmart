@extends('layout.default')

@section('main')
<h1>Usuarios</h1>
<div class="col-md-10">
  <div class="row">
  		@if ($errors->any())
			  <div class="row">
			    <div class="col-md-11 bg-danger" style="padding:15px 15px 5px 15px;">
			      @foreach($errors->all() as $e)
			      	<div>{{ $e }}</div>
			      @endforeach
			    </div>
			  </div>
			@endif

  	{{ Form::open(array('route'=> 'usuarios.store')) }}
	  	<div class="form-group">
	        <label for="username">Username</label>
	        <input class="form-control" placeholder="Username" type="text" name="username" id="username" value="{{Input::old('username')}}">
	    </div>
	    <div class="form-group">
          <label for="email">Email</label>
          <input class="form-control" placeholder="Email" type="text" name="email" id="email" value="{{Input::old('email')}}">
      </div>
			<div class="form-group">
          <label for="password">Password</label>
          <input class="form-control" placeholder="Password" type="password" name="password" id="password">
      </div>
      <div class="form-group">
          <label for="password_confirmation">Confirm Password</label>
          <input class="form-control" placeholder="Confirm Password" type="password" name="password_confirmation" id="password_confirmation">
      </div>
      <div class="form-actions form-group">
        <button type="submit" class="btn btn-success">Crear</button>
      </div>
	  {{ Form::close() }}
	</div>
</div>
@stop