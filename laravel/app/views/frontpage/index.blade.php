@extends('layout.frontend.index')

@section('main')
<section id="como_funciona">
	<div class="container">
		<div class="row">
        <div class="col-md-12 text-center">
            <h3 class="section-subheading text-muted">AgendaSmart es una herramienta para administrar tu negocio, que simplifica<br> el proceso de reservas y te permite recibir pagos en linea.</h3>
            <h2 class="section-heading">¿Como Funciona?</h2>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-6 col-md-offset-3 text-center box-container ">
    		<div class="box">
    			<div class="box-inside"><div class="square square1">1</div><i class="fa fa-play"></i></div>
    			<div class="box-titulo"><h4>Regístrate</h4></div>
    			<div class="exp">
    				Crea tu cuenta
    			</div>
    		</div>
    		<div class="box">
    			<div class="box-inside"><div class="square square2">2</div><i class="fa fa-play"></i></div>
    			<div class="box-titulo"><h4>Crea tus agendas</h4></div>
    			<div class="exp">
    				Define horarios y precios
    			</div>
    		</div>
    		<div class="box">
    			<div class="box-inside"><div class="square square3">3</div><i class="fa fa-play"></i></div>
    			<div class="box-titulo"><h4>Crea tus Usuarios</h4></div>
    			<div class="exp">
    				Define quién puede acceder y administrar tus reservas
    			</div>
    		</div>
    		<div class="box">
    			<div class="box-inside"><div class="square square4">4</div><i class="fa fa-play"></i></div>
    			<div class="box-titulo"><h4>¡LISTO!</h4></div>
    			<div class="exp">
    				Tus usuarios ya pueden acceder a su cuenta y realizar o modificar reservas
    			</div>
    		</div>
    		<div class="box">
    			<div class="box-inside"><div class="square square5">5</div></div>
    			<div class="box-titulo"><h4>Reservas en tu sitio web</h4></div>
    			<div class="exp">
    				Instala el link en tu página web y tus clientes podrán reservar directamente desde tu sitio.
    			</div>
    		</div>
    	</div>
    </div>
	</div>
</section>

<section id="servicios">
	<div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="section-heading">¿Que tipo de servicio ofreces?</h2>
      </div>
    </div>
    <div class="row text-center">
      <div class="col-md-4">
        <span class="fa-stack fa-4x">
          <i class="fa fa-circle fa-stack-2x text-primary"></i>
          <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
        </span>
        <h4 class="service-heading">Actividades</h4>
        <p class="service-text">Kayaks, bicicletas, cancha de futbol, cancha de tenis...</p>
      </div>
      <div class="col-md-4">
        <span class="fa-stack fa-4x">
          <i class="fa fa-circle fa-stack-2x text-primary"></i>
          <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
        </span>
        <h4 class="service-heading">Alojamiento</h4>
        <p class="service-text">Por temporadas, camping, cabañas, departamento...</p>
      </div>
      <div class="col-md-4">
        <span class="fa-stack fa-4x">
          <i class="fa fa-circle fa-stack-2x text-primary"></i>
          <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
        </span>
        <h4 class="service-heading">Arriendo</h4>
        <p class="service-text">Oficinas, loft, casa, departamento, taller...</p>
      </div>
    </div>
  </div>
</section>
<section id="precios">
  <div class="container">
  <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="section-heading">Precios</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
        <table>
          <thead>
            <tr>
              <th></th>
              <th class="head">Prueba Gratis</th>
              <th class="head">Plan MENSUAL</th>
              <th class="head">Plan ANUAL</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="precio1">Descripcion</td>
              <td class="precio1">Prueba nuestro servicios</td>
              <td class="precio1">Conocenos mes a mes</td>
              <td class="precio1">Disfruta tu plan por un año con un 20% de descuento</td>
            </tr>
            <tr>
              <td class="precio2">Máximo de Usuarios</td>
              <td class="precio2 text-large">3</td>
              <td class="precio2 text-large">4</td>
              <td class="precio2 text-large">10</td>
            </tr>
            <tr>
              <td class="precio3">Máximo de Reservas</td>
              <td class="precio3 text-large">2</td>
              <td class="precio3 text-large">10.000</td>
              <td class="precio3 text-large">10.000</td>
            </tr>
            <tr>
              <td class="precio4">Precio</td>
              <td class="precio4 text-large">$0</td>
              <td class="precio4 text-large">$20.000</td>
              <td class="precio4 text-large">$190.000</td>
            </tr>
          </tbody>
      </table>
      </div>
    </div>
  </div>
</section>

@include('confide.signup_include', array('planes' => $planes, 'rubro'=>$rubro))

<section id="contacto">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4 text-center">
        <h2 class="section-heading">¡Contáctanos!</h2>
        <i class="fa fa-user fa-5x azul"></i>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-md-offset-4 text-center">
        {{ Form::open(array('id'=>'contactForm')) }} 
          <div class="form-group">
            {{ Form::label('nombre','Nombre',array('class' => 'sr-only')) }}
            <input type='text' name="nombre" id="nombre" class="form-control" placeholder="Nombre" 
            data-validation-required-message="El campo es requerido."
            maxlength="50"
            />
            <p class="help-block"></p>
          </div>
          <div class="form-group">
            {{ Form::label('emailc','Email',array('class' => 'sr-only')) }}
            <input type='email' name="emailc" id="emailc" class="form-control" placeholder="Email" 
            data-validation-required-message="El campo es requerido."
            data-validation-email-message="El cambo debe ser un correo."
            maxlength="50"
            />
            <p class="help-block"></p>
          </div>
          <div class="form-group">
            {{ Form::label('asunto','Asunto',array('class' => 'sr-only')) }}
            <input type='text' name="asunto" id="asunto" class="form-control" placeholder="Asunto" 
            data-validation-required-message="El campo es requerido." 
            maxlength="50"maxlength="50"/>
            <p class="help-block"></p>
          </div>
          <div class="form-group">
            {{ Form::label('mensaje','Mensaje',array('class' => 'sr-only')) }}
            <textarea name="mensaje" id="mensaje" class="form-control" placeholder="Mensaje" col="50" rows="10" 
            data-validation-required-message="El campo es requerido." maxlength="1500"></textarea>
            <p class="help-block"></p>
          </div>
          
          <div class="form-group form-inline">
            {{ Form::button('Enviar', array('class' => 'btn btn-default btn-agendasmart pull-right', 'type' => 'submit')) }}
          </div>
          <div id="success"></div>
          @if (Session::get('error'))
              <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
          @endif

          @if (Session::get('notice'))
              <div class="alert">{{{ Session::get('notice') }}}</div>
          @endif
        {{ Form::close() }}
        
      </div>
      <div>
    </div>
  </div>
</section>
@stop

@section('javascript')
  {{ HTML::script('frontend-asset/js/jqBootstrapValidation.js') }}
  <script>
  $(function(){
    $("#contactForm input,#contactForm select,#contactForm textarea").not("[type=submit]").jqBootstrapValidation({
      preventSubmit: true,
      sniffHtml: false,
      submitError: function($form, event, errors) {
            // additional error messages or events
        },
       submitSuccess: function($form, event) {
          event.preventDefault();
          var nombre = $('#nombre').val();
          var email = $('#emailc').val();
          var asunto = $('#asunto').val();
          var mensaje = $('#mensaje').val();
         $.ajax({
                  url: "/contacto",
                  type: "POST",
                  data: {
                      nombre: nombre,
                      email: email,
                      asunto: asunto,
                      mensaje: mensaje
                  },
                  cache: false,
                  success: function(data) {
                    data = JSON.parse(data);
                      // Success message
                      $('#success').html("<div class='alert alert-success'>");
                      $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                          .append("</button>");
                      $('#success > .alert-success')
                          .append("<strong>Tu contacto se ha enviado. </strong>");
                      $('#success > .alert-success')
                          .append('</div>');

                      //clear all fields
                      $('#contactForm').trigger("reset");
                  },
                  error: function() {
                      // Fail message
                      $('#success').html("<div class='alert alert-danger'>");
                      $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                          .append("</button>");
                      $('#success > .alert-danger').append("<strong>Lo sentimos, pero al parecer nuestro servidor de correo tiene problemas. ¡Porfavor intentelo mas tarde!");
                      $('#success > .alert-danger').append('</div>');
                      //clear all fields
                      $('#contactForm').trigger("reset");
                  },
              });

       }
    });
    // $('#contactForm').submit(function(event){
    //   event.preventDefault();
      
    // })

  })
  </script>
@stop