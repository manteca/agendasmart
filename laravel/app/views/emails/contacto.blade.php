<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Contacto desde AgendaSmart</h2>

    <div>
      <table>
        <tbody>
          <tr>
            <td>Nombre:</td>
            <td>{{{ $nombre }}}</td>
          </tr>
          <tr>
            <td>Email:</td>
            <td>{{{ $email }}}</td>
          </tr>
          <tr>
            <td>Asunto:</td>
            <td>{{{ $asunto }}}</td>
          </tr>
          <tr>
            <td>Mensaje:</td>
            <td>{{{ $mensaje }}}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </body>
</html>
