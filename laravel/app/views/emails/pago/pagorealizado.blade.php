<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Confirmacion: Pago de Reserva</h2>

    <div>
      <p>Estiamdo usuario: {{$user}}.</p>
      <p>Su reserva para {{$cl_lugar}} ya esta fue pagada.</p>
      <div>
        <ul>
          <li>Día: {{$dia}}</li>
          <li>Hora:{{$hora_ini}} - {{$hora_fin}} </li>
          <li>Valor Cancelado: {{$valor}} </li>
          <li>---</li>
          <li>Lugar: {{$cl_direccion}}</li>
          <li>Telefono: {{$cl_fono}}</li>
          <li>Celular: {{$cl_celular}}</li>
          <li>Mail: {{$cl_mail}}</li>
          <li>Web: {{$cl_web}}</li>
        </ul>
      </div>

      <p>
        Muchas gracias por utilizar nuestros servicios<br>
        Equipo de AgendaSmart<br>
        <a href="http://www.agendasamrt.cl" alt="AgendaSmart">www.agendasamrt.cl</a>
      </p>
    </div>
  </body>
</html>
