<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Confirmacion de Reserva</h2>

    <div>
      <p>Estimado: {{$nombre}}</p>
      <p>Su reserva para XXX ya esta en proceso.</p>
      <table>
        <tr>
          <td>Hora Inicio: {{$horario_ini}}</td>
        </tr>
        <tr>
          <td>Hora Fin: {{$horario_fin}}</td>
        </tr>
        <tr>
          <td>Fecha: {{$fecha}}</td>
        </tr>
        <tr>
          <td>Estado Reserva: {{$estado}}</td>
        </tr>
        <tr>
          <td>Total Reserva: $ {{$total_reserva}}</td>
        </tr>
      </table>
      <p>
        Equipo de AgendaSmart<br>
        <a href="http://www.agendasamrt.cl" alt="AgendaSmart">www.agendasamrt.cl</a>
      </p>
    </div>
  </body>
</html>
