@extends('layout.default')

@section('main')

<h1>Precios Especiales</h1>

<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
    
	    {{ Form::open(array('route'=> array('precio_especiales.update', $precio_especial->id_especial), 'method' => 'put')) }}       

				<div class="form-group">
	        <label for="tipo_especiallbl">Seleccione Tipo de Descuento:</label>
	        <select name="tipo_especial" id="tipo_especial">
		        <option value="">Seleccione Tipo de Descuento</option>
		        <option value="1" {{ ($precio_especial->tipo == 1)?'selected':'' }} >Fecha Específica</option>
		        <option value="2" {{ ($precio_especial->tipo == 2)?'selected':'' }}>Díaa de la Semana</option>
	        </select>
  			</div>
  			
  			<div class="form-group">
        	<label for="fecha">Día de la Semana:</label>
	        <select name="dia_tipo" id="dia_tipo">
	        <option value="1" {{ ($precio_especial->dia_tipo == 1)?'selected':'' }}>Los Lunes</option>
	        <option value="2" {{ ($precio_especial->dia_tipo == 2)?'selected':'' }}>Los Martes</option>
	        <option value="3" {{ ($precio_especial->dia_tipo == 3)?'selected':'' }}>Los Miércoles</option>
	        <option value="4" {{ ($precio_especial->dia_tipo == 4)?'selected':'' }}>Los Jueves</option>
	        <option value="5" {{ ($precio_especial->dia_tipo == 5)?'selected':'' }}>Los Viernes</option>
	        <option value="6" {{ ($precio_especial->dia_tipo == 6)?'selected':'' }}>Los Sábado</option>
	        <option value="7" {{ ($precio_especial->dia_tipo == 7)?'selected':'' }}>Los Domingos</option>
	        </select>
  			</div>

				<div class="form-group">
        	<label for="fecha">Fecha Específica:</label>
        	<input type="text" id="fecha" name="fecha" placeholder="Ingrese Fecha" value="{{ $precio_especial->fecha }}" >
  			</div>

  			<div class="form-group">
          <label for="hora_inicio">Desde:</label>
          <select id="hora_inicio" name="hora_inicio">
          <?php for($inicioh = 8 ;$inicioh<=24;$inicioh++){ ?>
  		  		<option value="<?php echo $inicioh; ?>:00" {{ ($inicioh == $precio_especial->hora_inicio)?'selected':'' }}><?php echo $inicioh; ?>:00</option>
  				<?php } ?>  
          </select>
          
          <label for="hora_fin">Hasta:</label>
          <select id="hora_fin" name="hora_fin">
          <?php for($finh = 9;$finh<=24;$finh++){ ?>
  		  		<option value="<?php echo $finh; ?>:00" {{ ($finh == $precio_especial->hora_fin)?'selected':'' }}><?php echo $finh; ?>:00</option>
  				<?php } ?> 
          </select>
      	</div>

      	 <div class="form-group">
			    <label for="valor">Valor:</label>
			    <input type="text" id="valor" name="valor" placeholder="Ingrese Valor" class="form-control" value="{{ $precio_especial->valor }}">
			  </div>
				
				<div class="form-group">
				  <label>Agendas:</label><br>
				  <label class="checkbox-inline">
				    <input type="checkbox" id="todos" name="todos" value="0"> Todas las Agendas
				  </label>

					@foreach($agendas as $agenda)
					  <label class="checkbox-inline">
					    <input type="checkbox" id="sala_{{ $agenda->id_plaza }}" name="sala[]" value="<?php echo $agenda->id_plaza; ?>" {{ (in_array($agenda->id_plaza, $agendas_seleccionadas) )?'checked':'' }}>{{ $agenda->nombre}}
					  </label> 
				  @endforeach
				</div> 

		    <div class="form-group">
		    {{ Form::submit('Crear Precio Especial', array('class' => 'btn btn-success')) }}
		    </div>
		  {{ Form::close() }}

    </div>
  </div>
</div>
@stop

@section('javascript')
	<script>
	$tipo = {{ $precio_especial->tipo }};  
	</script>
  {{ HTML::script('js/precio_especial.js') }}
  {{ HTML::script('js/bootstrap-datepicker.min.js') }}
@stop