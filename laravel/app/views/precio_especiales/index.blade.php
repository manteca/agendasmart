@extends('layout.default')

@section('main')

<h1>Ofertas y Promociones</h1>

<div class="col-md-10">
  <div class="row">
    <div class="col-md-11">
      {{ link_to_route('precio_especiales.create','Crear Precio Especial', array(),  array('class' => 'btn btn-primary')) }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-11" style="top:15px;">
      <table class="table table-striped">
        <tr>
          <th>Dia</th>
          <th>Mes</th>
          <th>Año</th>
          <th>Valor</th>
          <th>Dia</th>
          <th>Tipo</th>
          <th>Editar</th>
          <th>Quitar</th>
        </tr>

      
      @foreach($precio_especial as $pe)
        <tr>
          <td>{{ ($pe->tipo == 1) ? $pe->dia : 'No Aplica' }}</td>
          <td>{{ ($pe->tipo == 1) ? $pe->mes : 'No Aplica'}}</td>
          <td>{{ ($pe->tipo == 1) ? $pe->anho : 'No Aplica'}}</td>
          <td>{{ $pe->valor }}</td>
          <td>
            @if($pe->tipo == 2)
              {{ ($pe->dia_tipo == 1) ? 'Lunes' : Null }}
              {{ ($pe->dia_tipo == 2) ? 'Martes' : Null }}
              {{ ($pe->dia_tipo == 3) ? 'Miercoles' : Null }}
              {{ ($pe->dia_tipo == 4) ? 'Jueves' : Null }}
              {{ ($pe->dia_tipo == 5) ? 'Viernes' : Null }}
              {{ ($pe->dia_tipo == 6) ? 'Sabado' : Null }}
              {{ ($pe->dia_tipo == 7) ? 'Domingo' : Null }}
            @else
              No Aplica
            @endif
          </td>
          <td>
            @if($pe->tipo == 1)
              Fecha Especial
            @elseif($pe->tipo == 2)
              Día de la Semana
            @else
              No Definido
            @endif
          </td>
          <td>{{ link_to_route('precio_especiales.edit', 'Editar', $pe->id_especial, array('class'=> 'btn btn-small btn-warning')) }}</td>
          <td>{{ link_to_route('precio_especiales.destroy', 'Quitar', $pe->id_especial, array('class'=> 'btn btn-small btn-danger','data-method' => 'delete', 'data-confirm' => "¿Estas seguro que deseas borrar el precio especial?")) }}</td>
        </tr>
      @endforeach
      </table>
    </div>
  </div>
</div>
@stop