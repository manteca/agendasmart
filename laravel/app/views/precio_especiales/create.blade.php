@extends('layout.default')

@section('main')

<h1>Precios Especiales</h1>

<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
    
	    {{ Form::open(array('route'=> array('precio_especiales.store'))) }}      

				<div class="form-group">
	        <label for="tipo_especiallbl">Seleccione Tipo de Descuento:</label>
	        <select name="tipo" id="tipo_especial">
		        <option value="">Seleccione Tipo de Descuento</option>
		        <option value="1">Fecha Específica</option>
		        <option value="2">Díaa de la Semana</option>
	        </select>
  			</div>
  			
  			<div class="form-group">
        	<label for="fecha">Día de la Semana:</label>
	        <select name="dia_tipo" id="dia_tipo">
	        <option value="1">Los Lunes</option>
	        <option value="2">Los Martes</option>
	        <option value="3">Los Miércoles</option>
	        <option value="4">Los Jueves</option>
	        <option value="5">Los Viernes</option>
	        <option value="6">Los Sábado</option>
	        <option value="7">Los Domingos</option>
	        </select>
  			</div>

				<div class="form-group">
        	<label for="fecha">Fecha Específica:</label>
        	<input type="text" id="fecha" name="fecha" placeholder="Ingrese Fecha" value="" >
  			</div>

  			<div class="form-group">
          <label for="hora_inicio">Desde:</label>
          <select id="hora_inicio" name="hora_inicio">
          <?php for($inicioh = 8 ;$inicioh<=24;$inicioh++){ ?>
  		  		<option value="<?php echo $inicioh; ?>:00"><?php echo $inicioh; ?>:00</option>
  				<?php } ?>  
          </select>
          
          <label for="hora_fin">Hasta:</label>
          <select id="hora_fin" name="hora_fin">
          <?php for($finh = 9;$finh<=24;$finh++){ ?>
  		  		<option value="<?php echo $finh; ?>:00" ><?php echo $finh; ?>:00</option>
  				<?php } ?> 
          </select>
      	</div>

      	 <div class="form-group">
			    <label for="valor">Valor:</label>
			    <input type="text" id="valor" name="valor" placeholder="Ingrese Valor" class="form-control" value="">
			  </div>
				
				<div class="form-group">
				  <label>Agendas:</label><br>
				  <label class="checkbox-inline">
				    <input type="checkbox" id="todos" name="todos" value="0"> Todas las Agendas
				  </label>

					@foreach($agendas as $agenda)
					  <label class="checkbox-inline">
					    <input type="checkbox" id="sala_{{ $agenda->id_plaza }}" name="sala[]" value="<?php echo $agenda->id_plaza; ?>">{{ $agenda->nombre}}
					  </label> 
				  @endforeach
				</div> 

		    <div class="form-group">
		    {{ Form::submit('Crear Precio Especial', array('class' => 'btn btn-success')) }}
		    </div>
		  {{ Form::close() }}

    </div>
  </div>
</div>
@stop

@section('javascript')
	<script>
	$tipo = 0;   
	</script>
  {{ HTML::script('js/precio_especial.js') }}
  {{ HTML::script('js/bootstrap-datepicker.min.js') }}
@stop