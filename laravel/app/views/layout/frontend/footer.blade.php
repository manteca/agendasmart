<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <p id="copyright">Copyright &copy; AgendaSmart 2015</p>
                </div>
            </div>
        </div>
    </footer>