
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header page-scroll">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Activar Navegación</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand page-scroll" href="#page-top">
        <img src="{{ asset('img/logo-blanco.png') }}" alt="" id="logo">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="hidden"><a href="#page-top"></a></li>
        <li><a class="page-scroll" href="#como_funciona">¿Como funciona?</a></li>
        <li><a class="page-scroll" href="#servicios">Servicios</a></li>
        <li><a class="page-scroll" href="#precios">Precios</a></li>
        @if(!$aunthenticate)
          <li><a class="page-scroll" href="#registro">Registrarse</a></li>
        @endif
        <li><a class="page-scroll" href="#contacto">Contacto</a></li>
        @if($aunthenticate)
          <li>{{ link_to('users/logout','Logout' ,array('class' => 'page-scroll azul')) }}</li>
          <li>{{ link_to('portal','Admin',array('class' => 'page-scroll azul')) }}</li>
        @else
          <li>{{ link_to_route('login','Ingresar', array() ,array('class' => 'page-scroll')) }}</li>
        @endif
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>