<header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-heading">Reservas en Linea <br>fáciles para tu negocio</div>
                <div class="intro-lead-in">Acepta reservas online, administra tu disponibilidad <br> y fija tus precios desde todas partes.</div>
                <a href="#registro" class="page-scroll btn btn-xl"> <i class="fa fa-heart fa-fw"></i>&nbsp; Registrate</a>
            </div>
        </div>
    </header>