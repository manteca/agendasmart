<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Agenda perzonalizada para tu empresa">
    <meta name="author" content="AgendaSmart">
    <meta name="google-site-verification" content="CSatdeM-2mf_WjfKdxfop2HDuSYGxgyOARXCLfPzoRY" />
    <meta name="robots" content="noindex">
    <title>AgendaSmart {{{ $title or '' }}}</title>
    
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,900' rel='stylesheet' type='text/css'>
    {{ HTML::style('css/bootstrap.min.css') }}
    <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    {{ HTML::style('frontend-asset/css/agency.css') }}
    {{ HTML::style('frontend-asset/font-awesome/css/font-awesome.min.css') }}
    {{ HTML::style('https://fonts.googleapis.com/css?family=Montserrat:400,700') }}
    {{ HTML::style('https://fonts.googleapis.com/css?family=Kaushan+Script') }}
    {{ HTML::style('https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic') }}
    {{ HTML::style('https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700') }}
    {{ HTML::style('css/main.css') }}
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-72775619-1', 'auto');
      ga('send', 'pageview');

    </script>
  </head>
  <body id="page-top" class="index">
    <!-- Navigation -->
    @if($temp_include['navbar'])
      @include('layout.frontend.navbar')
    @endif

    <!-- Header -->
    @if($temp_include['header'])
      @include('layout.frontend.header')    
    @endif

    <!-- Main -->
    @yield('main','Blank')
    
    @if($temp_include['footer'])
      @include('layout.frontend.footer')
    @endif




      
      <!-- script references -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    {{ HTML::script('js/bootstrap.min.js') }}

    <!-- Plugin JavaScript -->
    @if($temp_include['navbar'])
      {{ HTML::script('http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js') }}
      {{ HTML::script('frontend-asset/js/classie.js') }}
      {{ HTML::script('frontend-asset/js/cbpAnimatedHeader.js') }}
    @endif
    <!-- Custom Theme JavaScript -->
    {{ HTML::script('frontend-asset/js/agency.js') }}
    @yield('javascript')
  </body>
</html>