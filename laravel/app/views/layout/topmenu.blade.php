<div id="top-nav" class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Dashboard</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        @if(Auth::user()->hasRole('Admin'))
          <li><a href="#">( Id-Cliente {{ Session::get('id_cliente') }} )</a></li>
          <li><a href="#">( Id-User {{ Session::get('id_user') }} )</a></li>
        @endif
        <li><a href="{{ url('wiki') }}"><i class="glyphicon glyphicon-question-sign"></i></a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
            <i class="glyphicon glyphicon-user"></i> @if(Auth::check()) {{ Auth::user()->email }} @endif<span class="caret"></span>
          </a>
          <ul id="g-account-menu" class="dropdown-menu" role="menu">
            <!-- <li><a href="#">Mi Perfil</a></li> -->
            @if(Auth::user()->hasRole('Admin'))
              <li>{{link_to_action('AdminController@createClient','Crear Cliente')}}</li>
            @endif
          </ul>
        </li>
        <li>
          @if(Auth::check())
            <a href="{{url('users/logout')}}"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
          @else
            {{ link_to_route('login','Login') }}
          @endif

      </ul>
    </div>
  </div><!-- /container -->
</div>