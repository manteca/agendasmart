<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AgendaSmart {{{ $title or '' }}}</title>
    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    {{ HTML::style('css/bootstrap.min.css') }}
    <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    @yield('css')
    {{ HTML::style('css/dashboard.css') }}
    {{ HTML::style('css/styles.css') }}
  </head>
  <body>
<!-- Header -->
@include('layout.topmenu-clean')
<!-- /Header -->

<!-- Main -->
<div class="container-fluid">
<div class="row">
  <div class="col-md-3">
      <!-- Left column -->
      

    </div><!-- /col-3 -->
    <div class="col-md-9 col-md-offset-3">
        
      <!-- column 2 --> 
      {{-- @include('layout.subtopmenu') --}}
      <a href="#"><strong><i class="glyphicon glyphicon-dashboard"></i> My Dashboard</strong></a>  
      
        <hr>
      
      <div class="row">
        @yield('main','<h3>Blank page</h3> <p>This is a light-weight blank page, with minimum to none plugins loaded, a simpler menu example and perfect to get you started, as you build up your next cool web app. You can <a href="index.html?lang=en&amp;layout_type=fluid&amp;menu_position=menu-left&amp;style=style-default-menus-dark&amp;sidebar_type=collapsible" class="glyphicons single link"><i></i>return to the fully featured version</a> anytime.</p>')       
          
      </div><!--/row-->
      
      
    </div><!--/col-span-9-->
</div>
</div>
<!-- /Main -->

@include('layout.footer')
{{-- @include('layout.widget') --}}





  
  <!-- script references -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/scripts.js') }}
    @yield('javascript')
  </body>
</html>