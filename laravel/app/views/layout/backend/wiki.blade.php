@extends('layout.default')

@section('main')
<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
      <h1>WikiAgenda</h1>
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                Horarios
              </a>
            </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              En <strong>horarios</strong> debes indicar la hora de apertura y cierre de tu negocio
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Agendas
              </a>
            </h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
              <p>En <strong>configurar agendas</strong> deberás crear las agendas que necesites y configurar los precios de cada una de ellas.</p>
              <p>Recuerda que una agenda puede ser una cancha, una habitación, un servicio, un producto o lo que tú desees que tus clientes reserven  y que debes crear una agenda por cada una de ellas</p>
              <p>Por ejemplo si tienes 5 canchas debes crear 5 agendas y te sugerimos poner un nombre que la identifique: Cancha 1, Cancha 2, Cancha 3.</p>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Agrupar Agendas
              </a>
            </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa atque error impedit excepturi ipsam ratione harum expedita. Distinctio aliquam deleniti, voluptates tenetur eligendi atque aperiam nihil nisi, error vel, porro.</p>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingFour">
            <h4 class="panel-title">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                Ofertas y Promociones
              </a>
            </h4>
          </div>
          <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
            <div class="panel-body">
              En <strong>Precios Especiales</strong> podrás hacer excepciones a los precios configurados para cada agenda para que puedas hacer ofertas, promociones o descuentos para días u horas determinadas.
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingFive">
            <h4 class="panel-title">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                Usuarios
              </a>
            </h4>
          </div>
          <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
            <div class="panel-body">
              <p>En <strong>Usuarios</strong> podrás crear, si es que quieres, a los perfiles  de los trabajadores de tu negocio que podrán hacer o modificar reservas.</p>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingSix">
            <h4 class="panel-title">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                Descargas Reservas
              </a>
            </h4>
          </div>
          <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
            <div class="panel-body">
              <p>En <strong>Descargar Reservas</strong> podrá bajar archivos Excel con todas las reservas realizadas en tu negocio.</p>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingSeven">
            <h4 class="panel-title">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                Ver Agenda
              </a>
            </h4>
          </div>
          <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
            <div class="panel-body">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa atque error impedit excepturi ipsam ratione harum expedita. Distinctio aliquam deleniti, voluptates tenetur eligendi atque aperiam nihil nisi, error vel, porro.</p>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingEight">
            <h4 class="panel-title">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                Editar Reserva
              </a>
            </h4>
          </div>
          <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
            <div class="panel-body">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa atque error impedit excepturi ipsam ratione harum expedita. Distinctio aliquam deleniti, voluptates tenetur eligendi atque aperiam nihil nisi, error vel, porro.</p>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingNine">
            <h4 class="panel-title">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                Habilitar pago en linea
              </a>
            </h4>
          </div>
          <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
            <div class="panel-body">
              <p>En <strong>Habilitar Pagos en Línea</strong> podrás configurar la cuenta bancaria donde recibirás los pagos por las reservas que hagan tus clientes en tú página web y el correo electrónico donde recibirás las notificaciones de estos pagos.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop


