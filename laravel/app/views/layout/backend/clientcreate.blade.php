@extends('layout.default')

@section('main')
<div class="col-md-10">
  <div class="row">
    <div class="col-md-11" style="top:15px;">
      <h1>Crear Cliente </h1>
      @include('confide.signup_form', array('planes' => $planes, 'rubro'=>$rubro))
    </div>
  </div>
</div>
@stop


