
<div class="row">
  <div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
      <li class="{{ isActiveRoute('portal') }}c">{{ link_to('portal','Inicio') }}</li>
      @if(Auth::user()->hasRole('Cliente') OR Auth::user()->hasRole('Admin'))
        <li class="{{ areActiveRoutes(['horarios.index','horarios.edit']) }}">{{ link_to_route('horarios.index', 'Horarios') }} </li>
        <li class="{{ areActiveRoutes(['agendas.index','agendas.create','agendas.rangos.index','agendas.edit']) }}">{{ link_to_route('agendas.index','Agendas') }}</li>
        <li class="{{ areActiveRoutes(['agenda_grupos.index','agenda_grupos.create','agenda_grupos.edit']) }}">{{ link_to_route('agenda_grupos.index','Agrupar Agendas') }}</li>
        <li class="{{ areActiveRoutes(['precio_especiales.index','precio_especiales.create','precio_especiales.edit']) }}">{{ link_to_route('precio_especiales.index','Ofertas y Promociones') }}</li>
        <li class="{{ areActiveRoutes(['usuarios','usuarios.create','usuarios.edit']) }}">{{ link_to_route('usuarios', 'Usuarios') }}</li>
        <li role="separator" class="divider"></li>
      @endif
      <li>{{ link_to('exportar/reservas', 'Descargar Reservas') }}</li>
      <li class="{{ isActiveRoute('reservas.calendario') }}">{{ link_to_route('reservas.calendario', 'Ver Agenda') }}</li>
      <li class="{{ isActiveRoute('reservas.index') }}">{{ link_to_route('reservas.index', 'Editar Reserva') }}</li>
      @if(Auth::user()->hasRole('Admin') )
        @if(!Session::get('flow_available'))
            <li class="{{ isActiveRoute('flowconfig.create') }}">{{ link_to('clientes/flowConfig/'.Session::get('id_cliente'), 'Habilitar pago en linea') }}</li>
        @else
            <?php $test = Session::get('id_cliente'); ?>
            <li class="{{ isActiveRoute('flowconfig.show') }}">{{ link_to('clientes/flowConfigShow/'.Session::get('id_cliente'), 'Datos pago en linea') }}</li>
        @endif 
      @endif
    </ul>
  </div>
</div>

