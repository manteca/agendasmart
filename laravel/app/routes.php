<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@frontpage');
Route::post('contacto', 'HomeController@contacto');
/* Calendario Publico INICIO*/
Route::get('agendas/calendario/{shortname?}/{grupo?}', array('as' => 'agenda.calendario', 'uses' => 'AgendasController@calendario'));
Route::get('reservas/events', array('as' => 'reservas.eventos', 'uses' => 'ReservasController@getEvents'));
Route::post('reservas/guardar', array('uses' => 'ReservasController@guardar'));
Route::post('reservas/horarioDisponible', array('as' => 'reservas.horario-disponible', 'uses' => 'ReservasController@horarioDisponible'));
Route::post('rango/precio', array('as' => 'rangos.precio', 'uses' => 'RangosController@getPrecio'));
Route::post('reservas', array('as' => 'reservas.store', 'uses' => 'ReservasController@store'));

/* Calendario Publico FIN */
/* Pago Reserva */
Route::get('pagos/proceso_reserva/{cliente_id}/{reserva_id?}', array('as'=>'pagos.procesoreserva','uses'=> 'PagosController@procesoReserva'));
Route::post('pagos/exito', array('uses'=> 'PagosController@exito'));
Route::post('pagos/fracaso', array('uses'=> 'PagosController@fracaso'));
Route::get('pagos/confirmarOrden', array('uses'=> 'PagosController@confirmarOrden')); // Antes del envio
Route::get('pagos/proceso_plan/{cliente_id}/{plan_id}', array('uses'=> 'PagosController@procesoPlan'));
//Flow function
// Route::get('flows/testconfirm',array('uses' => 'FlowsController@testConfirm'));
Route::post('flows/confirmar',array('uses' => 'FlowsController@confirmar')); // Duarnte el proceso de compra

Route::group(array('before' => 'auth'), function()
{
	//Admin
	Route::get('admin','AdminController@createClient');

	Route::get('portal', array('as' => 'portal', 'uses' => 'HomeController@dashboard'));
	Route::get('wiki','HomeController@wiki');

	//Pagos
	Route::resource('pagos', 'PagosController');

	//Horarios
	Route::resource('horarios', 'HorariosController');
	//Reservas
	Route::get('reservas/calendario/{grupo?}', array('as' => 'reservas.calendario', 'uses' => 'ReservasController@calendario'));
	Route::resource('reservas', 'ReservasController', array('except' => array('store','show')));

	//Route::get('rangos/{idplaza}', array('as' => 'rangos.index', 'uses'=>'RangosController@index'));
	//Route::get('agendas.rangos', array( 'uses'=>'RangosController@index'));

	//Agendas
	Route::resource('agendas', 'AgendasController');
	// Agregar Grupo
	Route::get('agenda_grupos/defecto/{id}', array('as' => 'agenda_grupos.defecto', 'uses' => 'AgendaGruposController@defecto'));
	Route::get('agenda_grupos/estado/{id}', array('as' => 'agenda_grupos.estado', 'uses' => 'AgendaGruposController@estado'));
	Route::resource('agenda_grupos', 'AgendaGruposController');
	//Rangos
	Route::get('agendas/{agendas}/diaa/{dia}', array('as' => 'agendas.rangos.new', 'uses' => 'RangosController@create'));
	Route::post('agendas/{agendas}/rangos', array('as' => 'agendas.rangos.store', 'uses' => 'RangosController@store'));
	Route::get('agendas/{agendas}/diar/{dia}', array('as' => 'agendas.rangos.duplicate', 'uses' => 'RangosController@duplicate'));
	Route::resource('agendas.rangos', 'RangosController', array('except' => array('create','store','edit','update')));
	//Precios Especiales
	Route::resource('precio_especiales', 'PrecioEspecialesController');

	Route::get('clientes/planCaducado/{cliente_id}',array('uses'=>'ClientesController@planCaducado'));
	Route::get('clientes/planUpgrade/{cliente_id}',array('uses'=>'ClientesController@planUpgrade'));

	Route::get('clientes/flowConfigIs/{cliente_id}', array( 'uses'=>'ClientesController@flowConfigIs')); // existe
	Route::get('clientes/flowConfig/{cliente_id}', array('as' =>'flowconfig.create', 'uses'=>'ClientesController@flowConfig')); // Crear
	Route::post('clientes/flowConfigStore/', array( 'uses'=>'ClientesController@flowConfigStore')); // Guardar
	Route::get('clientes/flowConfigShow/{cliente_id?}', array('as' =>'flowconfig.show',  'uses'=>'ClientesController@flowConfigShow')); // Ver
	Route::post('clientes/flowCertificate/', array( 'uses'=>'ClientesController@flowCertificate')); // descargar Certificado

	Route::get('exportar/reservas', array('uses' => 'ReservasController@exportReservas'));

	//
	Route::get('usuarios',array('as'=>'usuarios','uses'=>'UsersController@usuarioIndex'));
	Route::get('usuarios/create',array('as'=>'usuarios.create','uses'=>'UsersController@usuarioCreate'));
	Route::post('usuarios/store',array('as'=>'usuarios.store','uses'=>'UsersController@usuariostore'));
	Route::get('usuarios/{usuario_id}/edit',array('as'=>'usuarios.edit','uses'=>'UsersController@usuarioEdit'));
	Route::put('usuarios/{usuario_id}',array('as'=>'usuarios.update','uses'=>'UsersController@usuarioUpdate'));
	Route::delete('usuarios/{usuario_id}',array('as'=>'usuarios.destroy','uses'=>'UsersController@usuarioDestroy'));

	// Perfil
	Route::get('users/perfil', 'UsersController@perfil');
	Route::post('users/perfils', 'UsersController@perfilStore');
});

// Pruebas
Route::controller('pruebas','PruebasController');
// Confide routes

Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('users/login', array('as' => 'login', 'uses' => 'UsersController@login'));
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');
