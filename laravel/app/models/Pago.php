<?php

class Pago extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];
	public static $rules_factura = [
		'rutFactura' => 'required', 
		'razonSocial' => 'required', 
		'direccion' => 'required', 
		'comuna' => 'required',
		'ciudad' => 'required',
		'telefono' => 'required', 
		'giro' => 'required', 
		'nombreFantasia' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['tipo_id','tipo','app_pago_id','app_pago','valor','detalle_id','detalle_table','detalle','estado'];

}