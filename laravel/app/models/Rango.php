<?php

class Rango extends \Eloquent {
	protected $table = 'rango';
  protected $primaryKey = 'ID_RANGO';
  public $timestamps = false;
	
	protected $fillable = ['id_plaza','dia','desde','hasta','valor'];
	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

  public function agenda()
  {
    return $this->belongsTo('Agenda','ID_PLAZA');
  }



  public function __get($key)
  {
    if (is_null($this->getAttribute($key))) {
        return $this->getAttribute(strtoupper($key));
    } else {
        return $this->getAttribute($key);
    }
  }

}