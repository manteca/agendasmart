<?php

class Horario extends \Eloquent {
	protected $table = 'horario';
	protected $primaryKey = 'id_horario';
	public $timestamps = false;

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['dia','desde','hasta'];

	public function clientes()
  {
    return $this->hasMany('Cliente','ID_CLIENTE','ID_PLAN');
  }
}