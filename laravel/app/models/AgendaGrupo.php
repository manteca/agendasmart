<?php

class AgendaGrupo extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
			'nombre'	=>	'required',
			'agendas'	=>	'array|required'
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['nombre','agendas', 'default','cliente_id','estado', 'block', 'vista'];

	public function isDefault()
	{
		if($this->default)
			return $this;
	}

	public function selectGroup($id)
	{
		if($this->id == $id)
			return $this;	
	}

}