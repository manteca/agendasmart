<?php

class Rubro extends \Eloquent {
	protected $table = 'rubro';
  protected $primaryKey = 'ID_RUBRO';
  protected $fillable = ['id_rubro','nombre'];
  public static $rules = [
    // 'title' => 'required'
  ];


	public function __get($key)
  {
    if (is_null($this->getAttribute($key))) {
        return $this->getAttribute(strtoupper($key));
    } else {
        return $this->getAttribute($key);
    }
  }
}