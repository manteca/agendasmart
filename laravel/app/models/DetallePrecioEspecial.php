<?php

class DetallePrecioEspecial extends \Eloquent {
	protected $table = 'detalle_precio_especial';
  protected $primaryKey = 'ID_DETALLE';
  public $timestamps = false;
	
	protected $fillable = ['id_detalle','id_especial', 'id_plaza'];

	public function precioEspecial()
  {
    return $this->belongsTo('PrecioEspecial','id_especial');
  }  

	public function __get($key)
  {
    if (is_null($this->getAttribute($key))) {
        return $this->getAttribute(strtoupper($key));
    } else {
        return $this->getAttribute($key);
    }
  }
}