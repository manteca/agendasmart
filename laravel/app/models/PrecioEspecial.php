<?php

class PrecioEspecial extends \Eloquent {
	protected $table = 'precio_especial';
  protected $primaryKey = 'ID_ESPECIAL';

  public $timestamps = false;
	
	protected $fillable = ['id_cliente','dia', 'mes', 'anho','hora_inicio','hora_fin','valor','tipo','dia_tipo'];

  public static $rules = [
    // 'title' => 'required'
  ];

	public function detallesPrecioEspecial()
  {
    return $this->hasMany('DetallePrecioEspecial','id_especial');
  }

	public function __get($key)
  {
    if (is_null($this->getAttribute($key))) {
        return $this->getAttribute(strtoupper($key));
    } else {
        return $this->getAttribute($key);
    }
  }
}