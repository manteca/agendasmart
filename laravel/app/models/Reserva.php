<?php

class Reserva extends \Eloquent {
	protected $table = 'reserva';
  protected $primaryKey = 'ID_RESERVA';
  protected $fillable = ['id_cliente','id_plaza','horario_ini','horario_fin','dia','mes' ,'anho','nombre','rut', 'telefono', 'celular', 'email', 'total_reserva','fecha','estado'];
  public static $rules = [
    'id_plaza' => 'required',
    'horario_ini' => 'required',
    'horario_fin' => 'required',
    'dia' => 'required',
    'mes' => 'required',
    'anho' => 'required',
    'nombre' => 'required',
    'rut' => 'required',
    // 'telefono' => 'required'
    'celular' => 'required',
    'email' => 'required',
    'total_reserva' => 'required',
    // 'fecha' => 'required',
    // 'estado' => 'required',
    'g-recaptcha-response' => 'required|recaptcha',
  ];


  public function cliente()
  { 
    // TODO: ID_CLIENTE
    return $this->belongsTo('Cliente','ID_CLIENTE');
  }  

  public function plaza()
  {
    return $this->belongsTo('Plaza','ID_PLAZA');
  }


  public function __get($key)
  {
    if (is_null($this->getAttribute($key))) {
        return $this->getAttribute(strtoupper($key));
    } else {
        return $this->getAttribute($key);
    }
  }
}