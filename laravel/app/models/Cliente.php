<?php

class Cliente extends \Eloquent {
  public $timestamps = false;
  protected $table = 'cliente';
  protected $primaryKey = 'ID_CLIENTE';
	protected $fillable = ['ID_CATEGORIA','ID_PLAN','ID_RUBRO','RUT','NOMBRE','SHORTNAME' ,'INI_PLAN','FIN_PLAN','USUARIO','flow_conf'];

  public function users()
  {
    return $this->hasMany('User','id_cliente','ID_CLIENTE');
  }

  public function plan()
  {
    return $this->belongsTo('Plan','ID_PLAN');
  }

  public function reservas()
  {
    return $this->hasMany('Reserva','ID_CLIENTE');
  }

  public function __get($key)
  {
    if (is_null($this->getAttribute($key))) {
        return $this->getAttribute(strtoupper($key));
    } else {
        return $this->getAttribute($key);
    }
  }
}