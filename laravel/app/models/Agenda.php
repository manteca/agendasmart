<?php

class Agenda extends \Eloquent {
	protected $table = 'plaza';
  protected $primaryKey = 'ID_PLAZA';
  //For Fullcalendar.io
  protected $dates = ['start', 'end'];

  public $timestamps = false;
	
	protected $fillable = ['id_cliente','nombre','horario_ini','horrio_fin', 'tipo'];
	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

  public function reservas()
  {
    return $this->hasMany('Reserva','id_plaza');
  }

  public function rangos()
  {
    return $this->hasMany('Rango','id_plaza');
  }

  public function __get($key)
  {
    if (is_null($this->getAttribute($key))) {
        return $this->getAttribute(strtoupper($key));
    } else {
        return $this->getAttribute($key);
    }
  }

  
  /* TOTO: Revosar si se utiliza
  *
  * 
  *
  */ 
  public function getTitle(){return $this->title;}
  public function isAllDay(){return (bool)$this->all_day = false;}
  public function getStart()
  {
    return $this->start;
  }
  public function getEnd(){return $this->end;}
}