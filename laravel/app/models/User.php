<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements ConfideUserInterface
{
    use ConfideUser;
    use HasRole;

    public function cliente()
    {
    	return $this->belongsTo('Cliente','id_cliente','ID_CLIENTE');
    }
}