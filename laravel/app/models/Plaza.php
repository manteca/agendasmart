<?php


/* NO USAR */

class Plaza extends \Eloquent {
  protected $table = 'plaza';
  protected $primaryKey = 'ID_PLAZA';
	protected $fillable = ['ID_CLIENTE','NOMBRE','HORARIO_INI','HORRIO_FIN'];

  public function reservas()
  {
    return $this->hasMany('Reserva','ID_PLAZA');
  }
}