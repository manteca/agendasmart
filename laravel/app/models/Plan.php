<?php

class Plan extends \Eloquent {
  protected $table = 'plan';
  protected $primaryKey = 'ID_PLAN';
	protected $fillable = ['NOMBRE','VALOR','USUARIOS','RESERVAS','ESTADO'];

  public function clientes()
  {
    return $this->hasMany('Cliente','ID_CLIENTE','ID_PLAN');
  }

  public static function planOrder()
  {
    // orden Forzado de los planes desde los ID's de la BD.
    return array(1,4,5);
  }
}