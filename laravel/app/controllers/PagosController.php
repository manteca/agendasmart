<?php

class PagosController extends \BaseController {

	/**
	 * Display a listing of pagos
	 *
	 * @return Response
	 */
	public function index()
	{
		$pagos = Pago::all();
		
		return View::make('pagos.index', compact('pagos'));
	}

	/**
	 * Show the form for creating a new pago
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('pagos.create');
	}

	/**
	 * Store a newly created pago in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$validator = Validator::make($data = Input::all(), Pago::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		
		Pago::create($data);

		return Redirect::route('pagos.index');
	}

	/**
	 * Display the specified pago.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$pago = Pago::findOrFail($id);

		return View::make('pagos.show', compact('pago'));
	}

	/**
	 * Show the form for editing the specified pago.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$pago = Pago::find($id);

		return View::make('pagos.edit', compact('pago'));
	}

	/**
	 * Update the specified pago in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$pago = Pago::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Pago::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$pago->update($data);

		return Redirect::route('pagos.index');
	}

	/**
	 * Remove the specified pago from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Pago::destroy($id);

		return Redirect::route('pagos.index');
	}

	//***********************//

	public function procesoPlan($cliente_id,$plan_id)
	{

		$cliente = Cliente::find($cliente_id);
		$planes = $cliente->plan;

		$pago = new Pago();
		$pago->tipo_id = $cliente_id;
		$pago->tipo = 'cliente';
		//$pago->app_pago_id = ;
		$pago->app_pago = 'flow';
		$pago->valor = $planes->VALOR;
		$pago->detalle_id = $planes->ID_PLAN;
		$pago->detalle_table = 'plan';
		$pago->detalle = 'Tipo de plan: '.$planes->NOMBRE;
		$pago->estado = 'creado';
		$pago->save();

		// Si el valor del plan es CERO porque es el Plan gratis, se debe controlar y retornar un error

			// return Redirect::to(Config::get('app.url_alt').'/adm/php/kpf/orden.php')->with('orden', $pago->id)->with('monto',$pago->valor)->with('concepto',$planes->NOMBRE);
		if($cliente->plan->ID_PLAN == $plan_id){
			// return Redirect::to(Config::get('app.url_alt').'/adm/php/kpf/orden.php?orden='.$pago->id);
			$var = array(
				'orden_id'=>$pago->id,
				'orden_valor'=>$pago->valor,
				'orden_descripcion'=>'Pago de Plan N:'.$pago->tipo_id,
				'orden_tasa_comision' => 3
				);
			return View::make('pagos.flow.orden',$var);
		}else
			die('Oops the server made a boobo!! (1001)');
		// dd($cliente);
		// dd($cliente->ID_PLAN);
		//dd($cliente->plan);
		// if($cliente->plan_id == $plan_id)
		// 	dd($c);
		// else
		// 	echo 'fracaso';
		//$plan = Plan::find(1);
		//dd($plan->clientes);

		return View::make('hello');
	}

	public function actualizarPago($pago_data, $status = '')
	{		
		if($status == 'exito'){
			$pago_estado = Config::get('variables.estado_pago')[1];
			$reserva_estado = Config::get('variables.estado_reserva')[1];
		}
		elseif($status == 'fracaso'){
			$pago_estado = Config::get('variables.estado_pago')[3];
			$reserva_estado = Config::get('variables.estado_reserva')[3];
		}
		else{
			$pago_estado = $reserva_estado = 'no_def';
		}
		Log::info('[actualizarPago] Id de orden Compra '. $pago_data['ordenCompra']);
		Log::info('[actualizarPago] Id de orden Flow '.$pago_data['flowOrden']);
		Log::info('[actualizarPago] Estado Orden '.$pago_estado);
		
		$pago = Pago::find($pago_data['ordenCompra']);
		$pago->app_pago_id = $pago_data['flowOrden'];
		$pago->estado = $pago_estado;
		$pago->save();

		if($pago->tipo == 'reserva'){
			$reserva = Reserva::find($pago->tipo_id);
			$reserva->estado = $reserva_estado;
			$reserva->save();
		}
		return true;

	}

	private function enviarCorreoConfirmacion($pago_id)
	{
		
		$pago = Pago::find($pago_id);
		$reserva = Reserva::find($pago->tipo_id); // TODO: Corroborar que el tipo (tabla) es de reserva y no de cliente
		$cliente = $reserva->cliente;

		$user = new stdClass();
		$user->name = $reserva->NOMBRE;
		$user->mail = $reserva->EMAIL;

		// $date = new Date::createFromDate($reserva->ANHO, $reserva->MES, $reserva->DIA);

		$data = array(
			'user'=> $reserva->NOMBRE,
			'dia'=> Date::createFromDate($reserva->ANHO, $reserva->MES, $reserva->DIA),
			'hora_ini' => $reserva->HORARIO_INI,
			'hora_fin' => $reserva->HORARIO_FIN,
			'valor'=> $reserva->TOTAL_RESERVA,
			'cl_direccion'=> $cliente->DIRECCION,
			'cl_fono' => $cliente->FONO,
			'cl_celular' => $cliente->CELULAR ,
			'cl_mail' => $cliente->CONTACTO,
			'cl_lugar' => $cliente->NOMBRE,
			'cl_web' => $cliente->URL,
			);

		Mailgun::send('emails.pago.pagorealizado', $data, function($message) use ($user)
		{
			$message->to($user->mail, $user->name)->subject('Su Pago ha si realizado con exito');
		});
	}

	public function procesoReserva($cliente_id, $reserva_id)
	{
		//Guardar Reserva
		// Enviar Correo de la Reserva
		// TODO: Controlar que la orden ya esta ingresaer?? Vele la pena?
		$cliente = Cliente::find($cliente_id);
		
		if($cliente->flow_conf == NULL){
			return Redirect::route('reservas.show', array('$id'=>$reserva_id));
		}
		$flow_conf = Crypt::decrypt($cliente->flow_conf);
		$flow_comercio = $flow_conf['email'];

		$reserva = $cliente->reservas()->where('ID_RESERVA', '=', $reserva_id)->first();
		$plaza = $reserva->plaza;
		//Debugbar::info($reserva);

		$pago = new Pago();
		$pago->tipo_id = $reserva->ID_RESERVA;
		$pago->tipo = 'reserva';
		//$pago->app_pago_id = ; // TODO: Se debe actualizar al momento de pagar.
		$pago->app_pago = 'flow'; // TODO: se debe dejar dinamico al moemto de agregar otro medio de pago.
		$pago->valor = $reserva->TOTAL_RESERVA;
		$pago->detalle_id = $reserva->ID_PLAZA;
		$pago->detalle_table = 'plaza';
		$pago->detalle = 'Reserva de Plaza: '.$plaza->NOMBRE.'('.$plaza->ID_PLAZA.') ';
		$pago->detalle.= 'A Cliente: '.$cliente->NOMBRE.'('.$cliente->ID_CLIENTE.') ';
		$pago->detalle.= 'Fecha: '.$cliente->DIA.'/'.$cliente->MES.'/'.$cliente->ANHO.' '.$cliente->HORARIO_INI.' hasta '.$cliente->HORARIO_FIN;
		$pago->estado = Config::get('variables.estado_pago')[2];
		if($pago->save()){
			$var = array(
				'orden_id'=>$pago->id,
				'orden_valor'=>$pago->valor,
				'orden_descripcion'=>'Pago de Reserva N:'.$pago->tipo_id,
				'orden_tasa_comision' => 3,
				'flow_comercio' => $flow_comercio,
				'cliente' => $cliente
				);
			
			// Corroborar que el archivo de certificado existe
			$flowAPI = new Flow\flowAPI($flow_comercio);
			$hasCertificate = false;
			if($flowAPI->hasCertificate()){
				$hasCertificate = true;
			}
			$var['hasCertificate'] = $hasCertificate;

			return View::make('pagos.flow.orden',$var);
		}else{
			die('Oops the server made a boobo!! (1002)');
		}

	}

	public function confirmarOrden(){
		return View::make('pagos.flow.confirma');
	}

	public function exito()
	{
		// $flowAPI = new Flow\flowAPI();
		// try {
		//   // Lee los datos enviados por Flow
		//   $flowAPI->read_result();
		  
		// } catch (\RuntimeException $e) {
		//   error_log($e->getMessage());
		//   header($_SERVER['SERVER_PROTOCOL'] . ' 500 Ha ocurrido un error interno', true, 500);
		//   return;
		// }
		// $data = array(
		// 	'ordenCompra' => $flowAPI->getOrderNumber(), 
		// 	'monto' => $flowAPI->getAmount(),
		// 	'concepto' => $flowAPI->getConcept(),
		// 	'pagador' => $flowAPI->getPayer(),
		// 	'flowOrden' => $flowAPI->getFlowNumber(),
		// 	);

		$data = $this->readFlowResult();
		$this->actualizarPago($data, 'exito');
		$this->enviarCorreoConfirmacion($data['ordenCompra']);
		return View::make('pagos.flow.exito');
	}

	public function fracaso()
	{
		$data = $this->readFlowResult();
		$this->actualizarPago($data, 'fracaso');
		return View::make('pagos.flow.fracaso');
	}

	private function readFlowResult(){
		$flowAPI = new Flow\flowAPI();
		try {
		  // Lee los datos enviados por Flow
		  $flowAPI->read_result();
		  
		} catch (\RuntimeException $e) {
		  error_log($e->getMessage());
		  header($_SERVER['SERVER_PROTOCOL'] . ' 500 Ha ocurrido un error interno', true, 500);
		  return;
		}
		$data = array(
			'ordenCompra' => $flowAPI->getOrderNumber(), 
			'monto' => $flowAPI->getAmount(),
			'concepto' => $flowAPI->getConcept(),
			'pagador' => $flowAPI->getPayer(),
			'flowOrden' => $flowAPI->getFlowNumber(),
			);
		return $data;
	}

}
