<?php

class HorariosController extends \BaseController {

	/**
	 * Display a listing of horarios
	 *
	 * @return Response
	 */
	public function index()
	{
		//$horarios = Horario::all();
		$horarios = Horario::where('id_cliente', '=', Auth::user()->id_cliente)->get();

		return View::make('horarios.index', compact('horarios'));
	}

	/**
	 * Show the form for creating a new horario
	 *
	 * @return Response
	 */
	// public function create()
	// {
	// 	return View::make('horarios.create');
	// }

	/**
	 * Store a newly created horario in storage.
	 *
	 * @return Response
	 */
	// public function store()
	// {
	// 	$validator = Validator::make($data = Input::all(), Horario::$rules);

	// 	if ($validator->fails())
	// 	{
	// 		return Redirect::back()->withErrors($validator)->withInput();
	// 	}

	// 	Horario::create($data);

	// 	return Redirect::route('horarios.index');
	// }

	/**
	 * Display the specified horario.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		App::abort(404);
		$horarios = Horario::where('id_cliente', '=', Auth::user()->id_cliente)->get() ;

		return View::make('horarios.show', compact('horarios'));
	}

	/**
	 * Show the form for editing the specified horario.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$horario = Horario::find($id);
		//dd($horario->toArray());
		return View::make('horarios.edit', compact('horario'));
	}

	/**
	 * Update the specified horario in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$horario = Horario::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Horario::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$horario->update($data);

		return Redirect::route('horarios.index');
	}

	/**
	 * Remove the specified horario from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Horario::destroy($id);

		return Redirect::route('horarios.index');
	}

	/******/



}
