<?php

class HomeController extends \BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function frontpage()
	{
		$temp_include = array(
			'navbar' 	=> true,
			'header' 	=> true,
			'footer'	=> true
			);
		$planes = Plan::orderBy('valor')->lists('nombre','id_plan');
		$rubro = Rubro::orderBy('nombre')->lists('nombre','id_rubro');
		$aunthenticate = Auth::check();
		return View::make('frontpage.index', compact('temp_include','planes','rubro'))
										->with('aunthenticate', $aunthenticate);
	}

	public function dashboard()
	{

		// TODO: revisar esta query, no tiene sentigo
		$user = User::with('cliente')->find(Auth::user()->id);
		$cliente_id = $user->cliente->id_cliente;

		$cliente = Cliente::findOrFail($cliente_id);
		$request = Request::create('/clientes/planCaducado/'.$cliente_id, 'GET');
		$response = Route::dispatch($request);
		$planCaducado = $response->getOriginalContent();

		$request = Request::create('/clientes/planUpgrade/'.$cliente_id, 'GET');
		$response = Route::dispatch($request);
		$planUpgrade = $response->getOriginalContent();

		$request = Request::create('/clientes/flowConfigIs/'.$cliente_id, 'GET');
		$response = Route::dispatch($request);
		$flowConfig = $response->getOriginalContent();

		return View::make('general.portal')
				->with('planCaducado', $planCaducado)
				->with('planUpgrade', $planUpgrade)
				->with('flowConfig', $flowConfig)
				->with('cliente', $cliente);
	}

	public function contacto()
	{
		$data = Input::all();
		if($data['nombre'] != '' && $data['email'] != '' && $data['asunto'] != '' && $data['mensaje'] != '' ){
			Mailgun::send('emails.contacto', $data, function($message)
			{
				$message->to('fvillalobos@medioclick.com', 'Medioclick')->subject('Contacto desde AgendaSmart ');
			});
			$array = array($data,'success');
		}
		else{
			$array = array($data,'error');	
		}
		return json_encode($array);
	}

	public function wiki()
	{
		return View::make('layout.backend.wiki');
	}

}
