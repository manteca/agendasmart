<?php

class AgendasController extends \BaseController {
	/**
	 * Display a listing of agendas
	 *
	 * @return Response
	 */
	public function index()
	{
		$agendas = Agenda::where('id_cliente', '=', Auth::user()->id_cliente)->get();
		$tipo = Config::get('variables.tipo_agenda');
		return View::make('agendas.index', compact('agendas'))->withTipo($tipo);
	}

	/**
	 * Show the form for creating a new agenda
	 *
	 * @return Response
	 */
	public function create()
	{
		$tipo = Config::get('variables.tipo_agenda');
		return View::make('agendas.create')->withTipo($tipo);
	}

	/**
	 * Store a newly created agenda in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$validator = Validator::make($data = Input::all(), Agenda::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		
		$data['id_cliente'] = Auth::user()->id_cliente;
		
		$agenda = Agenda::create($data);
		$tipo_agenda = Config::get('variables.tipo_agenda');
		$grupo_defecto = Config::get('variables.tipo_grupo_defecto');
		$sin_grupo = AgendaGrupo::where('cliente_id', '=', Auth::user()->id_cliente)->get();

		$grupo = AgendaGrupo::where('nombre','=', $grupo_defecto[$agenda->tipo])->where('cliente_id', '=', Auth::user()->id_cliente)->first();
		if(is_null($grupo)){
			AgendaGrupo::create(array(
							'nombre' => $grupo_defecto[$agenda->tipo],
							'agendas' => json_encode(array((string)$agenda->id_plaza)),
							'default' => $sin_grupo->isEmpty()?1:0,
							'cliente_id' => Auth::user()->id_cliente,
							'estado' => 1,
							'block' => 1,
							'vista' => Config::get('variables.tipo_agenda_vista_union')[$agenda->tipo]
					));
		}else{
			$agendas = json_decode($grupo->agendas);
			array_push($agendas, (string)$agenda->id_plaza);
			$grupo->update(array(
							'agendas' => json_encode($agendas)
					));
		}

		return Redirect::route('agendas.index');
	}

	/**
	 * Display the specified agenda.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$agenda = Agenda::findOrFail($id);

		return View::make('agendas.show', compact('agenda'));
	}

	/**
	 * Show the form for editing the specified agenda.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$agenda = Agenda::find($id);
		$tipo = Config::get('variables.tipo_agenda');
		return View::make('agendas.edit', compact('agenda'))->withTipo($tipo);
	}

	/**
	 * Update the specified agenda in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$agenda = Agenda::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Agenda::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$agenda->update($data);
		
		return Redirect::route('agendas.index');
	}

	/**
	 * Remove the specified agenda from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Agenda::destroy($id);

		return Redirect::route('agendas.index');
	}

	public function calendario($shortname = '', $grupo = '')
	{
		
		$cliente = Cliente::where('shortname','=', $shortname)->first();
		debug('Cliente_id: '. $cliente->id_cliente);
		$grupos =	AgendaGrupo::where('cliente_id','=', $cliente->id_cliente)->get();

		// Selecciona el grupo de vista que se debe mostrar.
		if($grupo == ''){
			debug('grupo esta vacio');
			$grupo_defecto = $grupos->filter(function($grupo_t){
				return $grupo_t->isDefault();
			})->first();
		}else{
			debug('grupo no esta vacio');
			$grupo_defecto = $grupos->filter(function($grupo_t) use ($grupo){
				return $grupo_t->selectGroup($grupo);
			})->first();
		}
		if($grupo_defecto == NULL)
			App::abort(404, 'Calendario no encontrato o no existe un calendario');
		
		debug($grupo_defecto);

		$calendario = Calendar::setOptions([
				'firstDay'					=> 1,
				'defaultView'				=> "$grupo_defecto->vista",
        'lang'							=> 'es',
        'allDaySlot'				=> false,
        'axisFormat'				=> 'H:mm',
        'columnFormat'			=> 'ddd D/M',
        'slotEventOverlap'	=> false,
        'eventLimit'				=> false,
        'header'						=> []
			]);
		$calendario->setCallbacks([ 
        'events'			=> '{url: "/reservas/events", type: "GET", data: { cliente_id: $("#cliente_id").val(), show_reservados: $("#show_reservados").val(), grupo_vista: '.$grupo_defecto->id.'} }',
        'eventClick' 	=> 'function(calEvent, jsEvent, view){
        										if(calEvent.className == "fv-event-green"){
        											$modal = $("#myModal");
        											var res = calEvent.id.split(".");

        											$("#hora_ini").val(calEvent.hora_ini);
        											$("#hora_fin").val(calEvent.hora_fin);
        											$("#fecha").text(res[2]+"-"+res[1]+"-"+res[0]);
        											//$("#cliente_id").val(calEvent.client_id);
        											$("#dia").val(res[2]);
        											$("#mes").val(res[1]);
        											$("#anho").val(res[0]);

        											$modal.modal();
        											var dataArray = {
        												cliente_id: calEvent.client_id,
        												horario_ini: calEvent.hora_ini,
        												horario_fin: calEvent.hora_fin,
        												anho: res[0],
        												mes: res[1],
        												dia: res[2]
        											};
        											$.post("/reservas/horarioDisponible",dataArray).done(function(data){
        												$servicio = $("#servicios");
        												$servicio.find("option").remove().end().append(
        													$("<option></option>")
        														.attr("value", "")
        														.text("Seleccione..."));
																	$.each(JSON.parse(data), function (key, value) {
																		$servicio.append(
																			$("<option></option>")
																				.attr("value", value.id_plaza)
																				.text(value.nombre));
																	});
																});
															}

													}'
    ]);

	
		$this->layout = View::make('layout.default-clean');
		$this->layout->content = View::make('general.index', compact('cliente','grupos', 'grupo_defecto','calendario'))->withCheckpago(true)->withReservados(false)->withShortname($shortname);	
	}

}
