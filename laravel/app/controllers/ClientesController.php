<?php

class ClientesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /clientes
	 *
	 * @return Response
	 */
	public function index()
	{
		$clientes = Cliente::all();
		return View::make('clientes.index', compact('clientes'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /clientes/create
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /clientes
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Flow::$rules);
		
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Flow::create($data);

		return Redirect::route('flows.index');
	}

	/**
	 * Display the specified resource.
	 * GET /clientes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /clientes/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /clientes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /clientes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/*********************/

	// public function planCaduca($fecha)
	// {
	// 	$date = new Date($fecha);
	// 	return $date->add('6 month');
	// }
	public function planCaducado($cliente_id)
	{
		$cl = Cliente::findOrFail($cliente_id);
		$ahora = Date::now();
		$plan_inicio = new Date($cl->INI_PLAN);
		$plan_caduca = new Date($cl->FIN_PLAN);
		$data['res'] = true;
		$data['free'] = false;
		// el plan es gratis e indifinido
		if($plan_inicio->eq($plan_caduca)){
			$data['expired'] = false; 
			$data['time'] = ': Nunca, Esta en el plan gratis';
			$data['free'] = true;

			return $data;
		}

		// EL plan ya esta Caducado
		if($ahora->gt($plan_caduca)){
			$data['expired'] = true; 
			$data['time'] = 'hace '.$plan_caduca->diffForHumans($ahora,true);
			
			return $data;
		}

		// El plan esta por caducar
		if($ahora->lte($plan_caduca)){
			$data['expired'] = false;
			$data['time'] = 'en '.$ahora->diffForHumans($plan_caduca, true);
			return $data;
		}

		// no se llega a esta àrte nunca....
		$data['res'] = false;
		return $data;
	}

	public function planUpgrade($cliente_id)
	{
		//TODO: Lista todos los planes a los cuales puede acceder, siempre mejores planes, estos deben estar como botones independeintes
		$planOrden = Plan::planOrder();
		$planCantidad = count($planOrden);

		// TODO: controlar que no arroja informacon si no encientra el cliente
		$cliente = Cliente::findOrFail($cliente_id);

		if ( in_array($cliente->ID_PLAN, $planOrden)){
			$plan_key = array_search($cliente->ID_PLAN, $planOrden);
			$plan_key++;
			
			if(isset($planOrden[$plan_key])){
				//TODO: debe enviar el Nombre del Plan y el ID
				return array('plan'=>$planOrden[$plan_key]);
			}else{
				return array('plan'=>false);
			}
		}
		App::abort('404','Plan de usuario no existe');
	}

	public function flowConfigIs($id)
	{

		$cliente = Cliente::find($id);
		if($cliente->flow_config != NULL || $cliente->flow_conf != '')
			$data['res'] = true;
		else
			$data['res'] = false;

		return $data;
	}

	public function flowConfig($id)
	{
		$flowAPI = new Flow\flowAPI();
		$flowAPI->soap_c();
		$bancos = $flowAPI->getBancos();

		$tipo_cuenta = array(1 => 'Corriente', 2 => 'Vista' );

		// $ar = array(
		// 	'email' => 'fvillalobos@medioclick.com', 
		// 	'factura' => 1, 
		// 	'rut' => '14122645-2', 
		// 	'nombres' => 'Felipe', 
		// 	'apellidos' => 'Villalobos', 
		// 	'banco' => 1, 
		// 	'cuenta' => 1, 
		// 	'numeroCta' => '3991400', 
		// 	'rutFactura' => '14122645-2', 
		// 	'razonSocial' => 'Felipe', 
		// 	'direccion' => 'Bustamanet 76', 
		// 	'comuna' => 'Providencia', 
		// 	'ciudad' => 'Santiago',
		// 	'telefono' => '77667191',
		// 	'giro' => 'Restaurant', 
		// 	'nombreFantasia' => 'La Clascia',
		// 	'acuerdo' => 1
		// 	);

		//Former::populate($ar);
		$data = array(
			'title' => 'Configuración para Flow',
			'cliente_id' => $id,
			'bancos' => $bancos,
			'tipo_cuenta' => $tipo_cuenta
		);

		return View::make('cliente.flow',$data);
	}

	public function flowConfigStore()
	{	
		
		$validator = Validator::make($data = Input::all(), Pago::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(!Rut::check($data['rut'])){
			return Redirect::back()->withInput()
						->with('message', 'Rut ingresado no es válido')
		        ->with('message-type', 'info');	
		}
		
		$cliente = Cliente::find($data['cliente_id']);
		$data = array_except($data, array('cliente_id','_token'));

		$pass = str_random(20);

		

		$ar = array(
			'email' => $data['email'], 
			'clave1' => sha1($pass), 
			'clave2' => sha1($pass), 
			'pagos' => 1, 
			'factura' => isset($data['factura'])? (int) $data['factura']:0, // Si no se chequea el box de factura, arrojara un error.
			'rut' => str_replace('.','',$data['rut']), 
			'nombres' => $data['nombres'], 
			'apellidos' => $data['apellidos'], 
			'banco' => (int) $data['banco'], 
			'cuenta' => (int) $data['cuenta'], 
			'numeroCta' => $data['numeroCta']
			);
		if(isset($data['factura']))
		{
			if($data['factura'] == 1){
				$validator = Validator::make($data = Input::all(), Pago::$rules_factura);
				if ($validator->fails())
				{
					return Redirect::back()->withErrors($validator)->withInput();
				}
				$ar2 = array(
					'rutFactura' => $data['rutFactura'], 
					'razonSocial' => $data['razonSocial'], 
					'direccion' => $data['direccion'], 
					'comuna' => $data['comuna'],
					'ciudad' => $data['ciudad'],
					'telefono' => $data['telefono'], 
					'giro' => $data['giro'], 
					'nombreFantasia' => $data['nombreFantasia'] 
				);
			}else{
				die('Valor de Indice de Factura no corresponde');
			}
		}
		else
		{
			$ar2 = array(
				'rutFactura' => null, 
				'razonSocial' => null, 
				'direccion' => null, 
				'comuna' => null,
				'ciudad' => null,
				'telefono' => null, 
				'giro' => null, 
				'nombreFantasia' => null 
			);
		}
		
		$flow_ar = $ar = array_merge($ar, $ar2);
		$flowAPI = new Flow\flowAPI();
		$flowAPI->soap_c();
	
		// dd($flowAPI->soap->registerUser());

		try{
			if(!$flow_ar['factura']){
				$result = $flowAPI->soap->registerUser(
					$flow_ar['email'],
					$flow_ar['clave1'],
					$flow_ar['clave2'],
					$flow_ar['pagos'],
					$flow_ar['factura'],
					$flow_ar['rut'],
					$flow_ar['nombres'],
					$flow_ar['apellidos'],
					$flow_ar['banco'],
					$flow_ar['cuenta'],
					$flow_ar['numeroCta']
					);
			}else{
				$result = $flowAPI->soap->registerUser(
					$flow_ar['email'],
					$flow_ar['clave1'],
					$flow_ar['clave2'],
					$flow_ar['pagos'],
					$flow_ar['factura'],
					$flow_ar['rut'],
					$flow_ar['nombres'],
					$flow_ar['apellidos'],
					$flow_ar['banco'],
					$flow_ar['cuenta'],
					$flow_ar['numeroCta'],

					$flow_ar['rutFactura'],
					$flow_ar['razonSocial'],
					$flow_ar['direccion'],
					$flow_ar['comuna'],
					$flow_ar['ciudad'],
					$flow_ar['telefono'],
					$flow_ar['giro'],
					$flow_ar['nombreFantasia']
					);	
			}	
		}catch(\ss $exception){
			dd($exception);
			//TODO dejarla en el log y mandar el error devuelta al formulario
			return Response::make('Error en llamada SOAP-> ' . $exception->getMessage());
		}
		
		
		$flow_ar['resultado'] = $result;
		if($flow_ar['resultado']->code != 'NO_ERR'){

			Former::populate($flow_ar);
			return Redirect::back()->withErrors($flow_ar['resultado']);

			//die('Error al registrar usuario: '. $flow_ar['resultado']->code); // TODO: Se debe arrojar un error correcto
		}

		$ar3 = array(
			'create_at' => date('Y-m-d H:i:s'), 
			'updated_at' => date('Y-m-d H:i:s') 
			);
		$ar = array_merge($ar, $ar3);
		$ar = Crypt::encrypt($ar);
		
		$cliente->flow_conf = $ar;
		$cliente->save();

		debug($pass);
		debug($flow_ar);
		
		return Redirect::action('ClientesController@flowConfigShow')
				->with('id_cliente',$cliente->ID_CLIENTE)
				->with('pass',$pass)
				->with('flow_ar',$flow_ar);
	}

	public function flowConfigShow($id = null)
	{
		// TODO: corrobora que la llave existe y que se puede interactuar con flow.
		if($id == null)
			$id = Session::get('id_cliente');

		$pass = Session::get('pass');
		$cliente = Cliente::find($id);
		$flow_config = Crypt::decrypt($cliente->flow_conf);

		$flowAPI = new Flow\flowAPI();
		$flowAPI->soap_c();
		$bancos = $flowAPI->getBancos();

		//dd($flow_config);
		return View::make('cliente.flow_show', compact('cliente','flow_config','bancos'))
			->with('pass',$pass);
			// ->with('flow_ar',Session::get('flow_ar'));
	}

	// public function flowConfigCertificate(){
	// 	// TODO: Revosar que es lo que hace esto???
	// 	$flowAPI = new Flow\flowAPI('fvillalobos@medioclick.com');
	// 	$res = $flowAPI->checkPrivateKey();
	// }


	/**
	 * Descarga el Certificado desde Flow al sitio
	 */
	public function flowCertificate()
	{
		// TODO; Se debe dejar el ID del clienet que esta legueado
		// debug(Request::method());
		$client_id = Input::get('client_id');
		$cliente = Cliente::find($client_id);
		$flow_config = Crypt::decrypt($cliente->flow_conf);

		$flowAPI = new Flow\flowAPI();
		$flowAPI->soap_c();
		
		$ar = array('status'=> false);
		if($flowAPI->saveCertificate($flow_config['email'],$flow_config['clave1'])){
			$ar = array('status'=> true);
		}
		return Response::json($ar);
	}

}