<?php

class ReservasController extends \BaseController {

	/**
	 * Display a listing of reservas
	 *
	 * @return Response
	 */
	public function index()
	{
		$cliente_id = Auth::user()->id_cliente;
		$reservas = Reserva::with('plaza')->where('id_cliente', '=', $cliente_id)->orderBy('fecha','desc')->get();

		return View::make('reservas.index', compact('reservas'));
	}

	/**
	 * Show the form for creating a new reserva
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('reservas.create');
	}

	/**
	 * Store a newly created reserva in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Reserva::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$data['fecha'] = $data['anho'].'-'.$data['mes'].'-'.$data['dia'];
		// Estado 2 "en proceso" y estao 1 "confirmada"
		$data['estado'] = $data['checkpago']?Config::get('variables.estado_reserva')[2]:Config::get('variables.estado_reserva')[1];
		$reserva = new Reserva($data);
		$reserva->save();
		// Enviar Correo indicando que la reserva se realizo
		Mailgun::send('emails.confirmacion', $data, function($message) use ($data)
		{
			$message->to($data['email'], $data['nombre'])->subject('Confirmación de Reserva');
		});
		// Se debe redirigira al proceso de la reserva. (Confirmar la reserva, pagando, si tiene el servicios de pago habilitado.)
		if($data['checkpago']){
			// TODO: Agregar la variable al Redirect
			return Redirect::route('pagos.procesoreserva',array($data['id_cliente'], $reserva->id_reserva));	
		}

		// return View::make('reservas.index');
		return Redirect::route('reservas.index');
	}

	/**
	 * Display the specified reserva.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		die('no se usa');
		$reserva = Reserva::findOrFail($id);
		$cliente = $reserva->cliente;

		return View::make('reservas.show', compact('reserva','cliente'));
	}

	/**
	 * Show the form for editing the specified reserva.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$reserva = Reserva::find($id);

		return View::make('reservas.edit', compact('reserva'));
	}

	/**
	 * Update the specified reserva in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$reserva = Reserva::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Reserva::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$reserva->update($data);

		return Redirect::route('reservas.index');
	}

	/**
	 * Remove the specified reserva from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Reserva::destroy($id);

		return Redirect::route('reservas.index');
	}

	public function exportReservas()
	{
		
		$client_id = Auth::user()->id_cliente;

    $reservas = Reserva::where('id_cliente','=',$client_id)->with('plaza')->orderBy('id_reserva')->get()->toArray();
    
    foreach ($reservas as $key => $value) {
    	$reservas[$key]['agenda'] = $value['plaza']['NOMBRE'];
    	unset($reservas[$key]['plaza']);
    	unset($reservas[$key]['ID_CLIENTE']);
    	unset($reservas[$key]['ID_PLAZA']);
    }

    Excel::create('Filename', function($excel) use($reservas) {
    	$excel->setTitle('Descarga de Reservas AgendaSmart');
    	
    	$excel->sheet('Reservas al '.date('d-m-Y', time()), function($sheet) use($reservas) {
      	$sheet->fromArray($reservas);

    	});
		})->export('xls'); 
	}

	public function calendario($grupo = '')
	{
		$cliente =	Cliente::find(Auth::user()->id_cliente);
		$grupos =	AgendaGrupo::where('cliente_id','=',Auth::user()->id_cliente)->get();
		
		// Selecciona el grupo de vista que se debe mostrar.
		if($grupo == ''){
			$grupo_defecto = $grupos->filter(function($grupo_t){
				return $grupo_t->isDefault();
			})->first();
		}else{
			$grupo_defecto = $grupos->filter(function($grupo_t) use ($grupo){
				return $grupo_t->selectGroup($grupo);
			})->first();
		}
		if($grupo_defecto == NULL)
			App::abort(404, 'Calendario no encontrato o no existe un calendario');
		
		debug($grupo_defecto);

		$calendario = Calendar::setOptions([
				'firstDay'					=> 1,
				'defaultView'				=> "$grupo_defecto->vista",
        'lang'							=> 'es',
        'allDaySlot'				=> false,
        'axisFormat'				=> 'H:mm',
        'columnFormat'			=> 'ddd D/M',
        'slotEventOverlap'	=> false,
        'eventLimit'				=> false,
        'header'						=> []
			]);
		$calendario->setCallbacks([ 
        'events'			=> '{url: "/reservas/events", type: "GET", data: { cliente_id: $("#cliente_id").val(), show_reservados: $("#show_reservados").val(), grupo_vista: '.$grupo_defecto->id.'} }',
        'eventClick' 	=> 'function(calEvent, jsEvent, view){
        										if(calEvent.className == "fv-event-green"){
        											$modal = $("#myModal");
        											var res = calEvent.id.split(".");

        											$("#hora_ini").val(calEvent.hora_ini);
        											$("#hora_fin").val(calEvent.hora_fin);
        											$("#fecha").text(res[2]+"-"+res[1]+"-"+res[0]);
        											//$("#cliente_id").val(calEvent.client_id);
        											$("#dia").val(res[2]);
        											$("#mes").val(res[1]);
        											$("#anho").val(res[0]);

        											$modal.modal();
        											var dataArray = {
        												cliente_id: calEvent.client_id,
        												horario_ini: calEvent.hora_ini,
        												horario_fin: calEvent.hora_fin,
        												anho: res[0],
        												mes: res[1],
        												dia: res[2]
        											};
        											$.post("/reservas/horarioDisponible",dataArray).done(function(data){
        												$servicio = $("#servicios");
        												$servicio.find("option").remove().end().append(
        													$("<option></option>")
        														.attr("value", "")
        														.text("Seleccione..."));
																	$.each(JSON.parse(data), function (key, value) {
																		$servicio.append(
																			$("<option></option>")
																				.attr("value", value.id_plaza)
																				.text(value.nombre));
																	});
																});
															}

													}'
    ]);

		$this->layout = View::make('layout.default');
		$this->layout->content = View::make('general.index', compact('cliente','grupos', 'grupo_defecto','calendario'))->withCheckpago(false)->withReservados(true);

		// return View::make('general.index', compact('cliente'))->withCheckpago(false)->withReservados(true);	
	}

	public function getEvents()
	{
		$star = new Date(Input::get('start'));
		$end = new Date(Input::get('end'));
		$end->sub('1 day'); // Se elimina un dia para que este dentro del ultimo dia de la vista.
		debug($star, $end);
		$grupo_visita = Input::get('grupo_vista');
		$agendas_vista = json_decode(AgendaGrupo::find($grupo_visita)->agendas);

		$client_id = Input::get('cliente_id');
		$show_reservados = Input::get('show_reservados');

		if($client_id == '')
			$client_id = Auth::user()->id_cliente;

		$horarios = Horario::where('id_cliente' ,'=', $client_id)->orderBy('dia')->get();
		$horarios = $horarios->toArray();
		
		// Reseras
		$events = Reserva::whereBetween('fecha', array($star->format('Y-m-d'), $end->format('Y-m-d')))->whereIn('id_plaza',$agendas_vista)->get();
		$events->client_id = $client_id;
		$event_array = $this->eventArray($events,'red');
		
		// Libres
		// TODO: Arreglar la busqueda de libres para que sirva para todos los intervalos de tiempo. 60 hasta un dia completo, y para las vistas de week y mes.
		$agendas = Agenda::where('id_cliente' ,'=', $client_id)->whereIn('id_plaza',$agendas_vista)->get();
		$events2 = array();

		foreach ($agendas as $agenda) {
			$rangoDia =  $agenda->rangos->groupBy('DIA');

			$current = clone($star);
			// dd($current, $end);
			while ($current->lt($end) ) {

				if($current->dayOfWeek == 0)
					$current_day_of_week = 7; // Compatibilidad para tabla rango. 
				else
					$current_day_of_week = $current->dayOfWeek;

				if($rangoDia->has($current_day_of_week)){
					foreach ($rangoDia->get($current_day_of_week) as $rango) {

						$desde_array = explode(':', $rango->desde);
						$hasta_array = explode(':', $rango->hasta);

						$desde = Date::createFromTime($desde_array[0], $desde_array[1], $desde_array[2]); // delimitar esto con el valor regitringido por "horario"
						$hasta = Date::createFromTime($hasta_array[0], $hasta_array[1], $hasta_array[2]); // delimitar esto con el valor regitringido por "horario"
						
						while($desde < $hasta){
							$intervalo = $agenda->tipo; // Minutos

							$ev = new stdClass();
							$ev->anho = $current->format('Y');
							$ev->mes = $current->format('n');
							$ev->dia = $current->format('j');
							$ev->horario_ini = $desde->format('H:i:s');
							
							$desde->add($intervalo.' minutes');							
							
							if($desde->format('H:i:s') == '00:00:00')
								$desde->sub('1 minute');
							$ev->horario_fin = $desde->format('H:i:s');
							if($desde->format('H:i:s') == '23:59:00')
								$desde->add('1 minute');
							
							$ev->fecha = $current->format('Y-m-d');
							$ev->id_reserva = 'a'.$agenda->ID_PLAZA;
							$ev->id_unico = $rango->id_rango.'-'.$current->format('Ynd').$ev->horario_ini;
							$ev->client_id = $client_id;
							$events2[] = $ev;

							//Incremento de Inervalro
							// $desde = strtotime('+'.$intervalo.' minutes', $desde);
						}
					}
				}
				$current->add('1 day');
			}
		}
		
		$event_array2 = $this->eventArray($events2,'green'); 
		debug('Disponibles->',$event_array2);
		debug('Reservados->',$event_array);

		//  Eliminar los disponibles segun las reservas
		$temp_event_array2 =$event_array2;
		foreach ($event_array as $value) {
			foreach ($event_array2 as $key => $value2) {
				$clave = array_search($value['id'], $value2);
				if($clave != false){
					debug($value['id']);
					$result = $event_array2[$key]['title'] = $value2['title'] - $value['title'];
					if($result <= 0){
						array_pull($event_array2, $key);
					}
				}
			}
		}
		// debug($clave);

		// Merge y Orde del array
		$event_array = $show_reservados?$event_array:array(); // Indica si muestra los reservados
		$event_array = array_merge($event_array,$event_array2); // Une los arrays de los reservados y disponibles
		$event_array = array_values(array_sort($event_array, function($value)
		{
		    return $value['start'];
		}));
		// dd($event_array);
		return json_encode($event_array);
		// return $event_array;
	}

	// Forma el Jjson para Fullcalendar
	private function eventArray($events,$color){
		$event_array = array();
		$event_contol = array();
		foreach ($events as $ev) {
			$even_ident = $ev->anho.'.'.$ev->mes.'.'.$ev->dia.'.'.$ev->horario_ini;
			if(!isset($event_contol[$even_ident])){
	    	$event_array[] = array(
	            'id' => $even_ident,
	            'title' => 1,
	            'start' => $ev->fecha.'T'.$ev->horario_ini,
	            'end' => $ev->fecha.'T'.$ev->horario_fin,
	            
	            'allDay' => false,
	            'className' => 'fv-event-'.$color,
	            'id_reserva' => $ev->id_reserva,
	            'hora_ini' => $ev->horario_ini, 
	            'hora_fin' => $ev->horario_fin,
	            'client_id' => $ev->client_id
	    	);
	    	end($event_array);
				$event_contol[$even_ident] = key($event_array);
			}else{
				$event_array[$event_contol[$even_ident]]['title']++;
				$event_array[$event_contol[$even_ident]]['id_reserva'] = $event_array[$event_contol[$even_ident]]['id_reserva'].','.$ev->id_reserva;

			}

		}
		
		return $event_array;
	}

	public function horarioDisponible($data = '')
	{
		$data = Input::all();
		// TODO: esto se debe evaluar si los rangos son variables
		$horario_ini = $data['horario_ini'] = substr($data['horario_ini'],0,7).'1';
		$data['horario_fin'] = substr($data['horario_ini'],0,3).'59:59';
		$client_id = $data['cliente_id'];
		$dia = $data['dia'];
		$mes = $data['mes'];
		$anho = $data['anho'];
		$num_dia = date('N',strtotime($anho.'-'.$mes.'-'.$dia));

		$agenda = DB::select("SELECT id_plaza, nombre
		 	FROM plaza
		 WHERE id_cliente = $client_id
		 AND id_plaza 
		 NOT IN (SELECT id_plaza FROM reserva WHERE id_cliente = $client_id AND dia = $dia AND mes = $mes AND anho = $anho AND time('$horario_ini') 
		 					BETWEEN horario_ini AND horario_fin)
 		 	AND ID_PLAZA IN ( SELECT ID_PLAZA 
  	   										FROM rango 
		 										WHERE dia = $num_dia
		 										AND time('$horario_ini') 
		 										BETWEEN desde AND hasta)"
			);

		return json_encode($agenda);
	}

// END CLASS
}
