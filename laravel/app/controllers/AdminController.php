<?php

class AdminController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /admincontroler
	 *
	 * @return Response
	 */
	public function createClient()
	{
		$planes = Plan::orderBy('valor')->lists('nombre','id_plan');
		$rubro = Rubro::orderBy('nombre')->lists('nombre','id_rubro');
		// $aunthenticate = Auth::check();
		return View::make('layout.backend.clientcreate', compact('temp_include','planes','rubro'));
	}


}