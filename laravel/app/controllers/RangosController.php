<?php

class RangosController extends \BaseController {

	/**
	 * Display a listing of rangos
	 *
	 * @return Response
	 */
	public function index($idplaza)
	{
		$agenda_id = $idplaza;
		$agenda =  Agenda::find($agenda_id);
		$tipo = $tipo = Config::get('variables.tipo_agenda');
		$rangos_temp = $agenda->rangos;

		// dd(DB::getQueryLog());
		$dias = array(1 => 'Lunes',2 => 'Martes', 3 => 'Miercoles', 4 => 'Jueves' , 5 => 'Viernes', 6 => 'Sabado', 7 => 'Domingo');

		foreach ($rangos_temp as $r_t) {
			
			$rangos[$r_t->dia][$r_t->id_rango]['desde'] = $r_t->desde;
			$rangos[$r_t->dia][$r_t->id_rango]['hasta'] = $r_t->hasta;
			$rangos[$r_t->dia][$r_t->id_rango]['valor'] = $r_t->valor;
		}
		//dd($rangos);

		return View::make('rangos.index', compact('rangos','agenda','dias'))->withTipo($tipo);
	}

	/**
	 * Show the form for creating a new rango
	 *
	 * @return Response
	 */
	public function create($agendaid, $diaa)
	{
		$dayNames = array(
	    'Domingo',
	    'Lunes', 
	    'Martes', 
	    'Miercoles', 
	    'Jueves', 
	    'Viernes', 
	    'Sabado', 
	 );
		$agenda =  Agenda::find($agendaid);
		$data = array(
			'agenda_id' => $agendaid, 
			'dia' => $diaa,
			'dia_nombre' => ($diaa != 7)?$dayNames[$diaa]:$dayNames[0]
		);
		
		if($agenda->tipo != 1440){
			$ultimaHora = Rango::where('ID_PLAZA','=',$agendaid)->where('DIA', '=', $diaa)->max('HASTA');
			if($ultimaHora){
				$horaInicio = (int) str_replace(':00:00','',$ultimaHora);
				// TODO: Esto se debe definir por el intervalo de tiempo definido en la agenda
				$horaFin = ($horaInicio+1); 
			}else{
				$horaInicio = 8;
				$horaFin = 9;
			}
			$data['horaInicio'] = $horaInicio;
			$data['horaFin'] = $horaFin;
		}

		
		
		return View::make('rangos.create', $data, compact('agenda'));
	}

	/**
	 * Store a newly created rango in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Rango::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Rango::create($data);
		$agendaid = $data['id_plaza'];
		return Redirect::route('agendas.rangos.index', array('agendas' => $agendaid));
	}

	/**
	 * Display the specified rango.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function show($id)
	// {
	// 	$rango = Rango::findOrFail($id);

	// 	return View::make('rangos.show', compact('rango'));
	// }

	/**
	 * Show the form for editing the specified rango.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function edit($id)
	// {
	// 	$rango = Rango::find($id);

	// 	return View::make('rangos.edit', compact('rango'));
	// }

	/**
	 * Update the specified rango in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function update($id)
	// {
	// 	$rango = Rango::findOrFail($id);

	// 	$validator = Validator::make($data = Input::all(), Rango::$rules);

	// 	if ($validator->fails())
	// 	{
	// 		return Redirect::back()->withErrors($validator)->withInput();
	// 	}

	// 	$rango->update($data);

	// 	return Redirect::route('rangos.index');
	// }

	/**
	 * Remove the specified rango from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($agendaid, $id)
	{
		//corroborar que la plaza de la cual se esta borrando el rango, es del cliente que esta logueado
		//Rango::destroy($id);
		Rango::where('ID_PLAZA', '=',$agendaid)->where('ID_RANGO', '=',$id)->delete();
		
		return Redirect::route('agendas.rangos.index', array('agendas' => $agendaid));
	}

	public function duplicate($agendaid, $idr)
	{
		# code...
	}

	public function getPrecio()
	{
		$data = Input::all();
		//$data['hora_ini'] = substr($data['hora_ini'], 0, 2);
		//$data['hora_fin'] = substr($data['hora_fin'], 0, 2);
		
		// Precio espcial todos el dia
		// TODO: Deberia considerar tambien los horios en el dia completo?
		$precio_especial_dia = DB::table('precio_especial')
												->leftJoin('detalle_precio_especial','precio_especial.id_especial','=','detalle_precio_especial.id_especial')
												->where('precio_especial.id_cliente', '=', $data['cliente_id'])
												->where('detalle_precio_especial.id_plaza', '=', $data['id_plaza'])
												->where('precio_especial.dia_tipo', '=', date('N', strtotime($data['dia'].'-'.$data['mes'].'-'.$data['anho'])))
												->first(['valor']);
		// Precio espcial hora espesifica
		$precio_especial_fecha = DB::table('precio_especial')
												->leftJoin('detalle_precio_especial','precio_especial.id_especial','=','detalle_precio_especial.id_especial')
												->where('precio_especial.id_cliente', '=', $data['cliente_id'])
												->where('detalle_precio_especial.id_plaza', '=', $data['id_plaza'])
												->where('precio_especial.dia', '=', $data['dia'])
												->where('precio_especial.mes', '=', $data['mes'])
												->where('precio_especial.anho', '=', $data['anho'])
												->where('precio_especial.hora_inicio', '<=',$data['hora_ini'])
												->where('precio_especial.hora_fin', '>=',$data['hora_fin'])
												->first(['valor']);

		// Precio rango de Hora
		$precio_rango = Rango::where('id_plaza', '=', $data['id_plaza'])
													->where('dia', '=', date('N', strtotime($data['dia'].'-'.$data['mes'].'-'.$data['anho'])))
													->where('desde', '<=', $data['hora_ini'])
													->where('hasta', '>=', $data['hora_fin'])
													->first(['valor']);

		if(isset($precio_especial_dia->valor))
		{
    	$valor = $precio_especial_dia->valor;
    }elseif(isset($precio_especial_fecha->valor))
    {
    	$valor = $precio_especial_fecha->valor;
    }else{
    	$valor = $precio_rango->valor;
    }
		// TODO: Mostrara error si no existe valor para el rango seleccionado.
    // $dataArray['valor'] = $valor;
    $dataArray['valor'] = $valor;
		debug($precio_especial_dia);
		debug($precio_especial_fecha);
		debug($precio_rango);
		return json_encode($dataArray);
	}	

}
