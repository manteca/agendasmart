<?php

class AgendaGruposController extends \BaseController {

	/**
	 * Display a listing of agenda_grupos
	 *
	 * @return Response
	 */
	public function index()
	{
		$agenda_grupos = AgendaGrupo::where('cliente_id', '=', Auth::user()->id_cliente )->get();
		foreach ($agenda_grupos as $value) {
			$value->agendas = json_decode($value->agendas);
		}
		// dd($value->agendas);
		$agendas_temp = Agenda::where('id_cliente', '=', Auth::user()->id_cliente )->get();
		foreach ($agendas_temp as $value) {
			$agendas[$value->id_plaza] = $value->nombre;
		}
		return View::make('agenda_grupos.index', compact('agenda_grupos','agendas'));
	}

	/**
	 * Show the form for creating a new agendagrupo
	 *
	 * @return Response
	 */
	public function create()
	{
		$agendas = Agenda::where('id_cliente', '=', Auth::user()->id_cliente )->get();
		$tipo_vista = Config::get('variables.tipo_agenda_vista');
		$tipo_vista_union = Config::get('variables.tipo_agenda_vista_union');
		return View::make('agenda_grupos.create', compact('agendas'))->withTipoVista($tipo_vista)->withTipoVistaUnion($tipo_vista_union);
	}

	/**
	 * Store a newly created agendagrupo in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array(
    	'agendas.required' => 'Se debe seleccionar al menos una agenda.',
		);
		$validator = Validator::make($data = Input::all(), AgendaGrupo::$rules, $messages);
		// validar que almenos un checkbox este seleccionado
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(is_array($data['agendas'])){
			foreach ($data['agendas'] as $value) {
				$agendas_ids[] = $value; 	
			}
		}
		$data['agendas'] = json_encode($agendas_ids);
		$data['cliente_id'] = Auth::user()->id_cliente;
		AgendaGrupo::create($data);

		return Redirect::route('agenda_grupos.index');
	}

	/**
	 * Display the specified agendagrupo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$agendagrupo = AgendaGrupo::findOrFail($id);

		return View::make('agenda_grupos.show', compact('agendagrupo'));
	}

	/**
	 * Show the form for editing the specified agendagrupo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$agendagrupo = AgendaGrupo::where('cliente_id', '=', Auth::user()->id_cliente )->find($id);
		$agendas = Agenda::where('id_cliente', '=', Auth::user()->id_cliente )->get();
		$tipo_vista = Config::get('variables.tipo_agenda_vista');
		$tipo_vista_union = Config::get('variables.tipo_agenda_vista_union');
		return View::make('agenda_grupos.edit', compact('agendagrupo','agendas'))->withTipoVista($tipo_vista)->withTipoVistaUnion($tipo_vista_union);
	}

	/**
	 * Update the specified agendagrupo in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messages = array(
    	'agendas.required' => 'Se debe seleccionar al menos una agenda.',
		);

		$agendagrupo = AgendaGrupo::where('cliente_id', '=', Auth::user()->id_cliente )->findOrFail($id);

		$validator = Validator::make($data = Input::all(), AgendaGrupo::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		if(is_array($data['agendas'])){
			foreach ($data['agendas'] as $value) {
				$agendas_ids[] = $value; 	
			}
		}
		$data['agendas'] = json_encode($agendas_ids);

		$agendagrupo->update($data);

		return Redirect::route('agenda_grupos.index');
	}

	/**
	 * Remove the specified agendagrupo from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$bloked = AgendaGrupo::where('cliente_id', '=', Auth::user()->id_cliente )->findOrFail($id)->block;
		if(!$bloked){
			//Agendagrupo::destroy($id);
			return Redirect::route('agenda_grupos.index');
		}else{
			
			return Redirect::route('agenda_grupos.index')->with('message','No se puede eliminar el registro')->with('message-type', 'warning');
		}
	}

	public function defecto($id)
	 {
	 		AgendaGrupo::where('cliente_id', '=', Auth::user()->id_cliente )->update(array('default' => false));
	 		$grupo = AgendaGrupo::where('cliente_id', '=', Auth::user()->id_cliente )->where('id', '=', $id)->firstOrFail();
	 		$grupo->default = true;
	 		$grupo->save();

	 		return Redirect::route('agenda_grupos.index');
	 } 

	 public function estado($id)
	 {
	 		//Agendagrupo::where('cliente_id', '=', Auth::user()->id_cliente )->update(array('default' => false));
	 		$grupo = AgendaGrupo::where('cliente_id', '=', Auth::user()->id_cliente )->where('id', '=', $id)->firstOrFail();
	 		$grupo->estado = !$grupo->estado;
	 		$grupo->save();

	 		return Redirect::route('agenda_grupos.index');
	 }

}
