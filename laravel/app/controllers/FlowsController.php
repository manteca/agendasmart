<?php

class FlowsController extends \BaseController {

	/**
	 * Display a listing of flows
	 *
	 * @return Response
	 */
	public function index()
	{
		$flows = Flow::all();

		return View::make('flows.index', compact('flows'));
	}

	/**
	 * Show the form for creating a new flow
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('flows.create');
	}

	/**
	 * Store a newly created flow in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Flow::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Flow::create($data);

		return Redirect::route('flows.index');
	}

	/**
	 * Display the specified flow.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$flow = Flow::findOrFail($id);

		return View::make('flows.show', compact('flow'));
	}

	/**
	 * Show the form for editing the specified flow.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$flow = Flow::find($id);

		return View::make('flows.edit', compact('flow'));
	}

	/**
	 * Update the specified flow in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$flow = Flow::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Flow::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$flow->update($data);

		return Redirect::route('flows.index');
	}

	/**
	 * Remove the specified flow from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Flow::destroy($id);

		return Redirect::route('flows.index');
	}

	/**************************/

	public function confirmar()
	{	
		\Debugbar::disable(); 
		$flowAPI = new Flow\flowAPI();
		try {
			// Lee los datos enviados por Flow
			$flowAPI ->read_confirm();
			
		} catch (Exception $e) {
			// Si hay un error responde false
			echo $flowAPI ->build_response(false);
			return;
		}

		//Recupera Los valores de la Orden
		$FLOW_STATUS = $flowAPI->getStatus();  //El resultado de la transacción (EXITO o FRACASO)
		$ORDEN_NUMERO = $flowAPI->getOrderNumber(); // N° Orden del Comercio
		$MONTO = $flowAPI->getAmount(); // Monto de la transacción
		$ORDEN_FLOW = $flowAPI->getFlowNumber(); // Si $FLOW_STATUS = "EXITO" el N° de Orden de Flow
		$PAGADOR = $flowAPI->getPayer(); // El email del pagador


		/*Aquí puede validar la Order
		 * Si acepta la Orden responder $flowAPI ->build_response(true)
		 * Si rechaza la Orden responder $flowAPI ->build_response(false)
		 */

		if($FLOW_STATUS == "EXITO") {
			// La transacción fue aceptada por Flow
			Log::info('Orden $ORDEN_NUMERO Confirmda con estado EXITO');
			// Aquí puede actualizar su información con los datos recibidos por Flow
			
			echo $flowAPI->build_response(true); // Comercio acepta la transacción
		} else {
			Log::info('Orden $ORDEN_NUMERO Confirmda con estado FRACASO');
			echo $flowAPI->build_response(false); // Comercio rechaza la transacción
		}

	}

	// stpublic function testconfirm()
	// {
	// 	return View::make('pagos.flow.testconfirmar');
	// }
	

}
