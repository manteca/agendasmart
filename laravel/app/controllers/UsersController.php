<?php



/**
 * UsersController Class
 *
 * Implements actions regarding user management
 */
class UsersController extends Controller
{

    /**
     * Displays the form for account creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {
      $temp_include = array(
      'navbar'  => false,
      'header'  => false,
      'footer'  => true
      );
      $planes = Plan::orderBy('valor')->lists('nombre','id_plan');
      $rubro = Rubro::orderBy('nombre')->lists('nombre','id_rubro');

      return View::make(Config::get('confide::signup_form'), compact('temp_include','planes','rubro'));
    }

    /**
     * Stores new account
     *
     * @return  Illuminate\Http\Response
     */
    public function store()
    {
        
        $repo = App::make('UserRepository');
        // Validacion para la info de cliente
        $validator = Validator::make($data = Input::all(), array(
          'plan' => 'required',
          'rubro' => 'required',
          'empresa' => 'required',
          //'shortname' => 'required',
          //'username' => 'required'
        ));

        $user = $repo->signup($data);
        
        if ( $user->id && !$validator->fails() ) {
          $plan = Plan::find($data['plan']);

          // Agregar el Cliente.
          $cliente = new Cliente();
          $cliente->user_id = $user->id;
          $cliente->id_plan = $data['plan'];
          $cliente->id_rubro = $data['rubro'];
          $cliente->nombre = $data['empresa'];
          $cliente->shortname = $this->shortname();
          $cliente->ini_plan = Date::now()->format('Y-m-d');
          $cliente->fin_plan = Date::now()->parse($plan->dias.' day')->format('Y-m-d'); 
          $cliente->save();
          $user->cliente()->associate($cliente);
          $user->save();
          // Agregar el rol por defecto
          $role_cliente = Role::find(2); // Role Cliente
          $user->attachRole($role_cliente);

          // Carga Inicial de los Dias.
          for($i=1;$i<=7;$i++){
            $horario = new Horario();
            $horario->id_cliente = $cliente->id_cliente;
            $horario->dia = $i;
            $horario->desde = 8;
            $horario->hasta = 24;
            $horario->save();
          }

            if (Config::get('confide::signup_email')) {
                Mail::queueOn(
                    Config::get('confide::email_queue'),
                    Config::get('confide::email_account_confirmation'),
                    compact('user'),
                    function ($message) use ($user) {
                        $message
                            ->to($user->email, $user->username)
                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                    }
                );
            }

            return Redirect::action('UsersController@login')
                ->with('message', Lang::get('confide::confide.alerts.account_created', array('correo' => $user->email)))
                ->with('message-type', 'info');
        } else {
            $error = [];
            $error1 = $validator->messages()->all(':message');
            $error2 = $user->errors()->all(':message');

            if(!empty($error1)){
              $error = $error1;
            }
            if(!empty($error2)){
              $error = array_merge($error, $error2);
            }
            
            return Redirect::action('UsersController@create')
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }
    }

    /**
     * Displays the login form
     *
     * @return  Illuminate\Http\Response
     */
    public function login()
    {
      $temp_include = array(
        'navbar'  => false,
        'header'   => false,
        'footer'  => true
        );
        if (Confide::user()) {
            return Redirect::to('/');
        } else {
            return View::make(Config::get('confide::login_form'), compact('temp_include'));
        }
    }

    /**
     * Attempt to do login
     *
     * @return  Illuminate\Http\Response
     */
    public function doLogin()
    {
        $repo = App::make('UserRepository');
        $input = Input::all();

        if ($repo->login($input)) {
            return Redirect::intended('/portal');
        } else {
            if ($repo->isThrottled($input)) {
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            } elseif ($repo->existsButNotConfirmed($input)) {
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            } else {
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
            }

            return Redirect::action('UsersController@login')
                ->withInput(Input::except('password'))
                ->with('error', $err_msg);
        }
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string $code
     *
     * @return  Illuminate\Http\Response
     */
    public function confirm($code)
    {
        if (Confide::confirm($code)) {
            $notice_msg = Lang::get('confide::confide.alerts.confirmation');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
            return Redirect::action('UsersController@login')
                ->with('error', $error_msg);
        }
    }

    /**
     * Displays the forgot password form
     *
     * @return  Illuminate\Http\Response
     */
    public function forgotPassword()
    {
      $temp_include = array(
        'navbar'  => false,
        'header'   => false,
        'footer'  => true
        );
      return View::make(Config::get('confide::forgot_password_form'), compact('temp_include'));
    }

    /**
     * Attempt to send change password link to the given email
     *
     * @return  Illuminate\Http\Response
     */
    public function doForgotPassword()
    {
        if (Confide::forgotPassword(Input::get('email'))) {
            $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
            return Redirect::action('UsersController@doForgotPassword')
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /**
     * Shows the change password form with the given token
     *
     * @param  string $token
     *
     * @return  Illuminate\Http\Response
     */
    public function resetPassword($token)
    {
      $temp_include = array(
        'navbar'  => false,
        'header'   => false,
        'footer'  => true
        );
      return View::make(Config::get('confide::reset_password_form'), compact('temp_include'))
                ->with('token', $token);
    }

    /**
     * Attempt change password of the user
     *
     * @return  Illuminate\Http\Response
     */
    public function doResetPassword()
    {
        $repo = App::make('UserRepository');
        $input = array(
            'token'                 =>Input::get('token'),
            'password'              =>Input::get('password'),
            'password_confirmation' =>Input::get('password_confirmation'),
        );

        // By passing an array with the token, password and confirmation
        if ($repo->resetPassword($input)) {
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
            return Redirect::action('UsersController@resetPassword', array('token'=>$input['token']))
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @return  Illuminate\Http\Response
     */
    public function logout()
    {
        Confide::logout();

        return Redirect::to('/');
    }

    /**
     * Creacion de usuarios  dentro de cada cliente
     */

    public function usuarioIndex(){
      $users = User::where('id_cliente','=',Session::get('id_cliente'))
          ->whereHas(
          'roles', function($q){
              $q->where('name', 'User');
          }
      )->get();
          
      return View::make('usuarios.index',compact('users'));
    }
    public function usuarioCreate(){
      return View::make('usuarios.create');
    }
    public function usuarioStore(){
      $rules = array(
          'username' => 'required',
          'email'    => 'required',
          'password' => 'required',
          'password_confirmation' => 'required'
        );

      $validator = Validator::make($data = Input::all(), $rules);

      if ($validator->fails())
      {
        return Redirect::back()->withErrors($validator)->withInput();
      }
      

      $user = new User;
      $user->username = array_get($data, 'username');
      $user->email = array_get($data, 'email');
      $user->password = array_get($data, 'password');
      $user->password_confirmation = array_get($data, 'password_confirmation');
      $user->confirmation_code = md5(uniqid(mt_rand(), true));
      $user->confirmed = 1;
      $user->id_cliente = Auth::user()->id_cliente;

      if(! $user->save()) {
          Log::info('Imposible crear usuario '.$user->email, (array)$user->errors());
          return Redirect::back()->withErrors($user->errors())->withInput(Input::except('password','password_confirmation'));

      } else {
          Log::info('Usuario creado '.$user->email);
      }

      // TODO: que rol se le esta asignandi?!?!?!
      // S edebe reasignar lo que se esta hacen, esta malo!!!!
      $role = Role::where('name','=','User')->first();
      debug($role);
      $user->roles()->attach($role->id);

      return Redirect::action('UsersController@usuarioIndex');
    }
    public function usuarioShow($id){}

    public function usuarioEdit($id){
      $user = User::find($id);      
      return View::make('usuarios.edit',compact('user'));
    }

    public function usuarioUpdate($id){
      $data = Input::all();
      if(isset($data['cambio_contrasena'])){
        $rules = array(
            'email'    => 'required',
            'password' => 'required',
            'password_confirmation' => 'required'
          );
      }else{
        $rules = array(
            'email'    => 'required',
          );
      }
      $validator = Validator::make($data, $rules);
      if ($validator->fails())
      {
        return Redirect::back()->withErrors($validator)->withInput();
      }
      $user = User::find($id);
      if($user->email != $data['email'])
        $user->email = $data['email'];

      if(isset($data['cambio_contrasena']))
      {
        $user->password = array_get($data, 'password');
        $user->password_confirmation = array_get($data, 'password_confirmation');
      }

      $user->touch();
      $user->save();      

      return Redirect::action('UsersController@usuarioIndex');
    }

    public function usuarioDestroy($id){
      User::destroy($id);
      return Redirect::action('UsersController@usuarioIndex');
    }


    public function perfil()    
    {
      $user = Auth::user();
      return View::make('confide.perfil', compact('user'));
    }

    private function shortname(){
        $collection = true;

        while($collection != null){
          $var = str_random(5);
          $collection = Cliente::where('shortname','=',$var)->first();
        }
        return $var;
    }

}
