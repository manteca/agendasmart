<?php

class PrecioEspecialesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /precioespecials
	 *
	 * @return Response
	 */
	public function index()
	{
		$client_id = Auth::user()->id_cliente;
		$precio_especial = PrecioEspecial::where('id_cliente', '=', $client_id )->get();
		return View::make('precio_especiales.index',compact('precio_especial','client_id'));	
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /precioespecials/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$client_id = Auth::user()->id_cliente;
		$agendas = Agenda::where('id_cliente', '=', $client_id )->get();

		return View::make('precio_especiales.create',compact('agendas'));	
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /precioespecials
	 *
	 * @return Response
	 */
	public function store()
	{	
		$validator = Validator::make($data = Input::all(), PrecioEspecial::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		
		$data['id_cliente'] = Auth::user()->id_cliente;
		
		if(isset($data['fecha'])){
			// $separa_fecha = split('-',$fecha);
			// $data['dia'] = $separa_fecha[0];
			// $data['mes'] = $separa_fecha[1];
			// $data['anho'] = $separa_fecha[2];
			$date = strtotime(str_replace('/', '-', $data['fecha']));
			
			$data['dia'] = date('j', $date);
			$data['mes'] = date('n', $date);
			$data['anho'] = date('Y', $date);
		}

		$precio = PrecioEspecial::create($data);
		if(is_array($data['sala'])){
			foreach ($data['sala'] as $key => $value) {
				$detalles[] = new DetallePrecioEspecial(array('id_plaza' => $value));		
			}
		}

		$precio->detallesPrecioEspecial()->saveMany($detalles);

		return Redirect::route('precio_especiales.index');
	}

	/**
	 * Display the specified resource.
	 * GET /precioespecials/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('precio_especiales.show');	
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /precioespecials/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$client_id = Auth::user()->id_cliente;
		$agendas = Agenda::where('id_cliente', '=', $client_id )->get();
		$precio_especial = PrecioEspecial::find($id);

		$agendas_seleccionadas = array();
		foreach($precio_especial->detallesPrecioEspecial as $detalle){
			$agendas_seleccionadas[] = $detalle->id_plaza;
		}
		
		$precio_especial->hora_inicio = str_replace(':00:00',':00',$precio_especial->hora_inicio);
		$precio_especial->hora_fin = str_replace(':00:00',':00',$precio_especial->hora_fin);

		if($precio_especial->tipo == 1){
			$precio_especial->fecha = $precio_especial->dia.'/'.$precio_especial->mes.'/'.$precio_especial->anho;
		}else{
			$precio_especial->fecha = '';
		}

		return View::make('precio_especiales.edit',compact('precio_especial','agendas','agendas_seleccionadas'));	
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /precioespecials/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$precio_especial = PrecioEspecial::findOrFail($id);

		$validator = Validator::make($data = Input::all(), PrecioEspecial::$rules);
		$data = array_merge($data, $this->modDate($data['fecha']));
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$precio_especial->update($data);

		DetallePrecioEspecial::where('id_especial','=', $id)->delete();

		if(is_array($data['sala'])){
			foreach ($data['sala'] as $key => $value) {
				$detalles[] = new DetallePrecioEspecial(array('id_plaza' => $value));		
			}
		}	
		$precio_especial->detallesPrecioEspecial()->saveMany($detalles);

		return Redirect::route('precio_especiales.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /precioespecials/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		PrecioEspecial::destroy($id);
		DetallePrecioEspecial::where('id_especial','=',$id)->delete();

		return Redirect::route('precio_especiales.index');
	}

	private function modDate($data){
			$date = strtotime(str_replace('/', '-', $data));
			
			$array['dia'] = date('j', $date);
			$array['mes'] = date('n', $date);
			$array['anho'] = date('Y', $date);
		return $array;
	}

}