$(document).ready(function() {                  
  $("#fecha").datepicker({
    format: 'dd/mm/yyyy',
  });    
   
   
 $("#dia_tipo").prop("disabled", true);
 $("#fecha").prop("disabled", true);
 $("#dia_semana").prop("disabled", true);
 $("#hora_inicio").prop("disabled", true);
 $("#hora_fin").prop("disabled", true);
     
 $("#valor").keypress(function (e) {
   //if the letter is not digit then display error and don't type anything
   if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
             return false;
  }
 });
   
   
$("#todos").on("change", function(){
   var all = $(this);
   $('input:checkbox').each(function() {
   	$(this).prop("checked", all.prop("checked"));
   });
});
   
   
$('#tipo_especial').on('change', function() {      
  var tipo_especial = $('select[name=tipo]').val();

  if(tipo_especial==1){
  	// dia_semana, fecha
  	$("#dia_tipo").prop("disabled", true).parent().hide();
  	$("#fecha").prop("disabled", false).parent().show();
  	$("#dia_semana").prop("disabled", true).parent().hide();
  	$("#hora_inicio").prop("disabled", false).parent().show();
    $("#hora_fin").prop("disabled", false).parent().show();
  }

  if(tipo_especial==2){
  	// dia_semana, fecha
  	$("#dia_tipo").prop("disabled", false).parent().show();
  	$("#fecha").prop("disabled", true).val('').parent().hide();
  	$("#dia_semana").prop("disabled", false).parent().show();
  	$("#hora_inicio").prop("disabled", false).parent().show();
    $("#hora_fin").prop("disabled", false).parent().show();;
  }

});    

if($tipo==1){
  $("#dia_tipo").prop("disabled", true).parent().hide();
  $("#fecha").prop("disabled", false).parent().show();
  $("#dia_semana").prop("disabled", true).parent().hide();
  $("#hora_inicio").prop("disabled", false).parent().show();
  $("#hora_fin").prop("disabled", false).parent().show();
}


if($tipo==2){
  $("#dia_tipo").prop("disabled", false).parent().show();
  $("#fecha").prop("disabled", true).parent().hide();
  $("#dia_semana").prop("disabled", false).parent().show();
  $("#hora_inicio").prop("disabled", false).parent().show();
  $("#hora_fin").prop("disabled", false).parent().show();
}        
   
   

   
});