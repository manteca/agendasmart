$(document).ready(function(){
  $('#btn-certificado').on('click',function(){
    // console.log('Comenzo');
    $.ajax({
      type: 'POST',
      url: '/clientes/flowCertificate',
      data: {client_id: $('#client_id').val()},
      beforeSend: function() {
        $("#certificado_status").html('Buscando Archivo...');
      },
      success: function(response) {
        if(response.status){
          $("#certificado_status").html('Archivo Descargado');
        }else{
          $("#certificado_status").html('Error al descargar el archivo <br><p>Recuerda que para descargar el archivo debes tener encuenta lo siguiente:</p><ul><li>Confirmar el correo que se envió al mail que ingresaste en este formulario</li><li>No haber cambiado la contraseña del sitio de flow</li></ul>');
        }
      }

    });
  });
});